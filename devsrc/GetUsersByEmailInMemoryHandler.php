<?php

namespace DRVBund\Plugins\CGAutomation\Development;

use DRVBund\Plugins\CGAutomation\Ilias\Query\GetUserByEmail;
use DRVBund\Plugins\CGAutomation\Ilias\Query\Handler\QueryHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Query\Query;
use ilObjUser;

/**
 * @psalm-api
 * @template-implements QueryHandler<GetUserByEmail, ilObjUser[]>
 */
class GetUsersByEmailInMemoryHandler implements QueryHandler
{
    /**
     * @var array<string, ilObjUser>
     */
    private array $emailUserMapping = [];

    public function __construct()
    {
    }

    public function handles(): string
    {
        return GetUserByEmail::getName();
    }

    public function handle(Query $query)
    {
        $email = $query->getEmail();

        return $this->emailUserMapping[$email] ?? null;
    }
}
