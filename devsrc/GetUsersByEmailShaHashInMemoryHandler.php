<?php

namespace DRVBund\Plugins\CGAutomation\Development;

use DRVBund\Plugins\CGAutomation\Ilias\Query\GetUserByEmailShaHash;
use DRVBund\Plugins\CGAutomation\Ilias\Query\Handler\QueryHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Query\Query;
use ilObjUser;

/**
 * @psalm-api
 * @template-implements QueryHandler<GetUserByEmailShaHash, ilObjUser[]>
 */
class GetUsersByEmailShaHashInMemoryHandler implements QueryHandler
{
    /**
     * @var array<string, ilObjUser>
     */
    private array $emailHashUserMapping = [];

    public function __construct()
    {
    }

    public function handles(): string
    {
        return GetUserByEmailShaHash::getName();
    }

    public function handle(Query $query)
    {
        $emailHash = $query->getEmailHash();

        return $this->emailHashUserMapping[$emailHash] ?? null;
    }
}
