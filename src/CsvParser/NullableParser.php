<?php

namespace DRVBund\Plugins\CGAutomation\CsvParser;

/**
 * @template T
 * @implements CellParser<T>
 */
class NullableParser implements CellParser
{
    private CellParser $parser;
    /**
     * @var string[]
     */
    private array $nullValues;

    /**
     * @param CellParser $parser
     * @param string[] $nullValues
     */
    public function __construct(CellParser $parser, array $nullValues = [''])
    {
        $this->parser = $parser;
        $this->nullValues = $nullValues;
    }

    /**
     * @param string $value
     * @return null|T
     */
    public function parse(string $value)
    {
        if (in_array($value, $this->nullValues, true)) {
            return null;
        }

        return $this->parser->parse($value);
    }
}
