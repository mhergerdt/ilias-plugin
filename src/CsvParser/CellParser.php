<?php

namespace DRVBund\Plugins\CGAutomation\CsvParser;

/**
 * @template T
 */
interface CellParser
{
    /**
     * @param string $value
     * @return T
     */
    public function parse(string $value);
}
