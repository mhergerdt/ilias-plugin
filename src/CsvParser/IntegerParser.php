<?php

namespace DRVBund\Plugins\CGAutomation\CsvParser;

/**
 * @implements CellParser<int>
 */
class IntegerParser implements CellParser
{
    public function parse(string $value): int
    {
        return (int)filter_var($value, FILTER_SANITIZE_NUMBER_INT);
    }
}
