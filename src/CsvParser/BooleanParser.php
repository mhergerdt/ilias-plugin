<?php

namespace DRVBund\Plugins\CGAutomation\CsvParser;

/**
 * @implements CellParser<bool>
 */
class BooleanParser implements CellParser
{
    private string $trueValue;

    public function __construct(string $trueValue = '1')
    {
        $this->trueValue = $trueValue;
    }

    public function parse(string $value): bool
    {
        return $value === $this->trueValue;
    }
}
