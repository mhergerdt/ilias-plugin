<?php

namespace DRVBund\Plugins\CGAutomation\CsvParser;

/**
 * @implements CellParser<string>
 */
class StringTimeParser implements CellParser
{
    public function parse(string $value): string
    {
        return str_replace(':', '', trim($value));
    }
}
