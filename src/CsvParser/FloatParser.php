<?php

namespace DRVBund\Plugins\CGAutomation\CsvParser;

/**
 * @implements CellParser<float>
 */
class FloatParser implements CellParser
{
    public function parse(string $value): float
    {
        $filtered = filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND);
        $pointPos = strrpos($filtered, '.');
        $commaPos = strrpos($filtered, ',');

        if ($commaPos > -1) {
            if ($commaPos > $pointPos) {
                $filtered = str_replace(',', '.', str_replace('.', '', $filtered));
            }

            if ($commaPos < $pointPos) {
                $filtered = str_replace(',', '', $filtered);
            }
        }

        return floatval($filtered);
    }
}
