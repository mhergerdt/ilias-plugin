<?php

namespace DRVBund\Plugins\CGAutomation\CsvParser\Exception;

use Exception;

class InvalidCsvLine extends Exception
{
    public function __construct(string $expected, string $actualLine)
    {
        parent::__construct(
            'Invalid CSV line!' . PHP_EOL .
            "Expected:" . $expected . PHP_EOL . PHP_EOL .
            "Actual:" . $actualLine . PHP_EOL
        );
    }
}
