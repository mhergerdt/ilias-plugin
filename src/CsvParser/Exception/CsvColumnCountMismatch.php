<?php

namespace DRVBund\Plugins\CGAutomation\CsvParser\Exception;

use Exception;

class CsvColumnCountMismatch extends Exception
{
    private string $csvFile;
    private int $expectedColumns;
    private int $actualColumns;
    private string $csvLine;
    /**
     * @var string[]
     */
    private array $columns;

    /**
     * @param string $csvFile
     * @param int $expectedColumns
     * @param int $actualColumns
     * @param string $csvLine
     * @param string[] $columns
     */
    public function __construct(string $csvFile, int $expectedColumns, int $actualColumns, string $csvLine, array $columns)
    {
        parent::__construct("Expected $expectedColumns columns, got $actualColumns in file \"$csvFile\"");

        $this->csvFile = $csvFile;
        $this->expectedColumns = $expectedColumns;
        $this->actualColumns = $actualColumns;
        $this->csvLine = $csvLine;
        $this->columns = $columns;
    }

    /**
     * @return string
     */
    public function getCsvFile(): string
    {
        return $this->csvFile;
    }

    /**
     * @return int
     */
    public function getExpectedColumns(): int
    {
        return $this->expectedColumns;
    }

    /**
     * @return int
     */
    public function getActualColumns(): int
    {
        return $this->actualColumns;
    }

    /**
     * @return string
     */
    public function getCsvLine(): string
    {
        return $this->csvLine;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }
}
