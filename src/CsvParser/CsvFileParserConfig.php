<?php

namespace DRVBund\Plugins\CGAutomation\CsvParser;

class CsvFileParserConfig {
    public string $delimiter = ',';
    public string $enclosure = '"';
    public string $escape = '\\';
    public bool $hasHeader = false;
    /**
     * @var string[]
     */
    public array $columnTypes = [];
    /**
     * @var string[]|null
     */
    public ?array $headers = null;
}
