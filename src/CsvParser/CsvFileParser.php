<?php

namespace DRVBund\Plugins\CGAutomation\CsvParser;

use DRVBund\Plugins\CGAutomation\CsvParser\Exception\CsvColumnCountMismatch;
use DRVBund\Plugins\CGAutomation\CsvParser\Exception\InvalidCsvLine;
use DRVBund\Plugins\CGAutomation\Ilias\Adapter\FilesystemAdapter;
use Exception;
use Generator;

class CsvFileParser
{
    /**
     * @var CellParser[]
     */
    private array $cellParser = [];
    private FilesystemAdapter $filesystem;
    private CsvFileParserConfig $config;

    public function __construct(FilesystemAdapter $filesystem, ?CsvFileParserConfig $config)
    {
        $this->filesystem = $filesystem;
        $this->config = $config ?? new CsvFileParserConfig();
    }

    /**
     * @return CsvFileParserConfig
     */
    public function getConfig(): CsvFileParserConfig
    {
        return $this->config;
    }

    /**
     * @param CsvFileParserConfig $config
     * @return CsvFileParser
     */
    public function setConfig(CsvFileParserConfig $config): CsvFileParser
    {
        $this->config = $config;
        return $this;
    }

    public function addCellParser(string $columnType, CellParser $cellParser): self
    {
        $this->cellParser[$columnType] = $cellParser;
        return $this;
    }

    /**
     * @param string $file
     * @param string[] $columnTypes
     * @return Generator<array>
     * @throws CsvColumnCountMismatch
     * @throws Exception
     */
    public function parseFile(string $file, array $columnTypes = null): Generator
    {
        $handle = $this->openFile($file);
        if (null === $handle) {
            throw new Exception(sprintf('Failed to open file "%s"', $file));
        }

        $columnNames = $this->headers ?? null;

        $columnTypes = $columnTypes ?? $this->columnTypes ?? [];

        $i = 0;
        while (($line = fgets($handle)) !== false) {
            $line = preg_replace('/"[\n\r]+/', '"', $line);
            /** @var string[] $columns */
            $columns = str_getcsv($line, $this->config->delimiter, $this->config->enclosure, $this->config->escape);

            if ($this->config->hasHeader && $i === 0 && $columnNames === null) {
                $columnNames = $columns;
                $i = 1;
                continue;
            }

            $lineTrimmed = trim($line, "\n\r");
            $testLine = trim($this->toCsvString($columns), "\n\r");
            if ($testLine !== $lineTrimmed) {
                throw new InvalidCsvLine($lineTrimmed, $testLine);
            }

            if ($columnNames !== null) {
                $columnCount = count($columns);
                $headerCount = count($columnNames);

                if ($columnCount !== $headerCount) {
                    throw new CsvColumnCountMismatch($file, $headerCount, $columnCount, $line, $columns);
                }
            }

            $row = $columnNames !== null ? array_combine($columnNames, $columns) : $columns;

            $j = -1;
            foreach ($row as $key => $cell) {
                $j++;
                $row[$key] = $this->parseCell($columnTypes[$j] ?? null, $cell);
            }

            yield $row;
        }
    }

    /**
     * @throws Exception
     * @param string $file
     * @return resource|null
     */
    private function openFile(string $file)
    {
        return $this->filesystem->readStream($file)->detach();
    }

    /**
     * @param string|null $columnType
     * @param string $value
     * @return mixed
     */
    private function parseCell(?string $columnType, string $value)
    {
        if ($columnType === null) {
            return $value;
        }

        $parser = $this->cellParser[$columnType] ?? null;
        if ($parser === null) {
            return $value;
        }

        return $parser->parse($value);
    }

    private function toCsvString(array $fields): string
    {
        return join($this->config->delimiter, array_map(function ($value) {
            $needsEnclosure = str_contains($value, $this->config->delimiter) || str_contains($value, $this->config->enclosure);
            $value = str_replace($this->config->enclosure, $this->config->enclosure.$this->config->enclosure, $value);

            if ($needsEnclosure) {
                $value = $this->config->enclosure . $value . $this->config->enclosure;
            }

            return $value;
        }, $fields));
    }
}
