<?php

namespace DRVBund\Plugins\CGAutomation\CsvParser;

use DateTime;
use DateTimeZone;

/**
 * @implements CellParser<DateTime>
 */
class DateTimeParser implements CellParser
{
    private string $format;
    private ?DateTimeZone $timezone;

    public function __construct(string $format, ?DateTimeZone $timezone = null)
    {
        $this->format = $format;
        $this->timezone = $timezone;
    }

    public function parse(string $value): DateTime
    {
        return DateTime::createFromFormat($this->format, $value, $this->timezone);
    }
}
