<?php

namespace DRVBund\Plugins\CGAutomation\Booking;

use DRVBund\Plugins\CGAutomation\Ilias\Adapter\FilesystemAdapter;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Participant\AddCourseParticipant;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Participant\AddGroupParticipant;
use DRVBund\Plugins\CGAutomation\Ilias\Dispatcher;
use DRVBund\Plugins\CGAutomation\Ilias\PluginSettings;
use DRVBund\Plugins\CGAutomation\Ilias\Service\TreeService;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\CourseParticipantRole;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\GroupParticipantRole;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\PersonalizedBookingCode;
use Exception;
use ilLink;
use ilObject;
use ilObjUser;
use Throwable;

class BookingService
{
    private FilesystemAdapter $filesystem;
    private PluginSettings $pluginSettings;
    private TreeService $treeService;
    private ilObjUser $user;
    private Dispatcher $dispatcher;

    public function __construct(
        FilesystemAdapter $filesystem,
        PluginSettings    $pluginSettings,
        TreeService       $treeService,
        ilObjUser         $user,
        Dispatcher        $dispatcher
    )
    {
        $this->filesystem = $filesystem;
        $this->pluginSettings = $pluginSettings;
        $this->treeService = $treeService;
        $this->user = $user;
        $this->dispatcher = $dispatcher;
    }

    public function redeem(string $value): array
    {
        $bookingCode = PersonalizedBookingCode::fromString($value);

        $bookingCodeMapping = $this->loadBookingCodeMapping();

        $groupId = $bookingCodeMapping['bookingCodeMapping'][(string)$bookingCode->getTrainingBookingCode()] ?? null;

        if (null === $groupId) {
            throw new Exception('Unknown booking code!');
        }

        $groupInfo = $bookingCodeMapping['groupInfo'][$groupId] ?? null;
        if (null === $groupInfo) {
            throw new Exception('Unknown booking code!');
        }

        if (!$bookingCode->isValidForEmail($this->user->getEmail())) {// || hash('sha256', $this->user->getEmail()) !== $groupInfo['allowedParticipantHashes']) {
            throw new Exception('Booking code invalid for current user!');
        }

        try {
            $groupRefId = $this->treeService->resolveObjRefIdUnderParentTreeItem($groupId, $this->pluginSettings->refIdTargetCategory()->getValueAsInt());
            $courseRefId = $this->treeService->getParentId($groupRefId);

            if (null === $courseRefId) {
                throw new Exception('Unable to redeem booking code');
            }

            $courseId = ilObject::_lookupObjId($courseRefId);

            if (0 === $courseId) {
                throw new Exception('Unable to redeem booking code');
            }

            $this->dispatcher->dispatchCommand(new AddCourseParticipant($courseId, $this->user->getId(), CourseParticipantRole::Member()));
            $this->dispatcher->dispatchCommand(new AddGroupParticipant($groupId, $this->user->getId(), GroupParticipantRole::Member()));

            return [
                'target' => ilLink::_getLink($groupRefId, (string)ObjectType::Group())
            ];
        } catch (Throwable $e) {
            throw new Exception('Unable to redeem booking code');
        }
    }

    public function saveBookingCodeMapping(array $bookingCodeMapping): void
    {
        $this->filesystem->write((string)$this->pluginSettings->getDataSrcDirectory(), json_encode($bookingCodeMapping));
    }

    private function loadBookingCodeMapping(): array
    {
        return json_decode(
            $this->filesystem->read((string)$this->pluginSettings->getBookingCodeMappingFilePath()), true
        ) ?? [];
    }
}
