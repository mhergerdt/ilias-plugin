<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Provider;

use DRVBund\Plugins\CGAutomation\Shared\Dto\LocationDto;

class LocationProvider
{
    public function provideUnknownLocation(): LocationDto
    {
        $location = new LocationDto();
        $location->id = 'UNKNOWN';
        $location->title = 'UNKNOWN';
        $location->streetLine1 = '';
        $location->streetLine2 = '';
        $location->streetNumber = '';
        $location->zipCode = '';
        $location->city = '';

        return $location;
    }

    public function provideBigBlueButtonLocation(): LocationDto
    {
        $location = new LocationDto();
        $location->id = '75625259';
        $location->title = 'DRV Bund Lernwelt';
        $location->streetLine1 = 'Lernplattform';
        $location->streetLine2 = '';
        $location->streetNumber = '';
        $location->zipCode = '';
        $location->city = 'BigBlueButton';

        return $location;
    }

    /**
     * @param LocationDto $location
     * @return array<array-key, string>
     */
    public function provideLocationPrefix(LocationDto $location): array
    {
        switch ($location->id) {
            case '75625259': return ['VL'];
            case '75240847': return ['H'];
            case '75000963': return ['G'];
            case '75001981': return ['R'];
            case '75000964': return ['St'];
            case '75000937': return ['EU'];
            case '75967776': return ['TQ'];
            case '75000371': return ['U-', 'Pa'];
            case '75000375': return ['Wü'];
            case '75011577': return ['K-'];
            default: return [];
        }
        /*
        "id": "75000371",
        "title": "Bildungszentrum Erkner e.V.",
        "streetLine1": "Seestr.",
        "streetLine2": "",
        "streetNumber": "39",
        "zipCode": "15537",
        "city": "Erkner"

        "id": "75625259",
        "title": "DRV Bund Lernwelt",
        "streetLine1": "Lernplattform",
        "streetLine2": "",
        "streetNumber": "",
        "zipCode": "",
        "city": "BigBlueButton"

        "id": "75000374",
        "title": "Tagungszentrum Winterberg",
        "streetLine1": "Schneilstr.",
        "streetLine2": "",
        "streetNumber": "11",
        "zipCode": "59955",
        "city": "Winterberg"

        "id": "75000372",
        "title": "Bildungszentrum Reinfeld e.V.",
        "streetLine1": "Ahrensböker Str.",
        "streetLine2": "",
        "streetNumber": "51",
        "zipCode": "23858",
        "city": "Reinfeld"

        "id": "75240847",
        "title": "Campus am Silberturm der DRV Bund",
        "streetLine1": "Hohenzollerndamm",
        "streetLine2": "",
        "streetNumber": "46-47",
        "zipCode": "10713",
        "city": "Berlin"

        "id": "75000928",
        "title": "DRV Knappschaft-Bahn-See",
        "streetLine1": "Pieperstr.",
        "streetLine2": "",
        "streetNumber": "14-28",
        "zipCode": "44789",
        "city": "Bochum"

        "id": "75000964",
        "title": "DRV Bund, Stralsund",
        "streetLine1": "Zur Schwedenschanze",
        "streetLine2": "",
        "streetNumber": "1",
        "zipCode": "18435",
        "city": "Stralsund"

        "id": "75000963",
        "title": "DRV Bund, Gera",
        "streetLine1": "Reichsstr.",
        "streetLine2": "",
        "streetNumber": "5",
        "zipCode": "07545",
        "city": "Gera"

        "id": "75785783",
        "title": "next campus (1. OG DG Ruhrstraße)",
        "streetLine1": "Konstanzer Straße",
        "streetLine2": "",
        "streetNumber": "42",
        "zipCode": "10709",
        "city": "Berlin"

        "id": "75001981",
        "title": "DRV-Bund, Berlin, HV West",
        "streetLine1": "",
        "streetLine2": "",
        "streetNumber": "",
        "zipCode": "10704",
        "city": "Berlin"

        "id": "75000937",
        "title": "DRV Schwaben",
        "streetLine1": "Dieselstr.",
        "streetLine2": "",
        "streetNumber": "9",
        "zipCode": "86154",
        "city": "Augsburg"

        "id": "75967776",
        "title": "DRV Bund Tegel Quartier Süd",
        "streetLine1": "Gorkistraße",
        "streetLine2": "",
        "streetNumber": "12",
        "zipCode": "13507",
        "city": "Berlin"

        "id": "75000375",
        "title": "DRV Bund - Würzburg",
        "streetLine1": "Berner Straße",
        "streetLine2": "",
        "streetNumber": "1",
        "zipCode": "97084",
        "city": "Würzburg"

        "id": "75011577",
        "title": "DRV Braunschweig-Hannover (Laatzen)",
        "streetLine1": "Lange Weihe",
        "streetLine2": "",
        "streetNumber": "4",
        "zipCode": "30880",
        "city": "Laatzen"
        */
    }
}
