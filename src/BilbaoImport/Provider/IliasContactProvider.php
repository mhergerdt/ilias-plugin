<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Provider;

use DRVBund\Plugins\CGAutomation\Shared\Dto\IliasContactDto;
use ilDBConstants;
use ilDBInterface;
use ilFileUtils;

class IliasContactProvider
{
    private ilDBInterface $database;

    public function __construct(ilDBInterface $database)
    {
        $this->database = $database;
    }

    /**
     * @param array<string> $employeeNumbers
     * @return array<string, IliasContactDto>
     */
    public function provide(array $employeeNumbers): array
    {
        $iliasContacts = [
            'rvEvolution' => IliasContactDto::fromValues(
                '',
                '',
                '',
                'rvEvolution Bildung',
                'rvEvolution Bildung',
                null,
                'rvEvolution-Bildung@drv-bund.de',
                null,
                'rvEvolution'
            ),
        ];

        $usersData = $this->searchUser($employeeNumbers);
        $userIds = array_map(fn($userData) => (int)$userData['usr_id'], $usersData);
        $userDefinedDataMap = $this->getUserDefinedData($userIds);

        foreach ($usersData as $userData) {
            $iliasContact = $this->mapUser($userData, $userDefinedDataMap);
            $iliasContacts[$iliasContact->iliasContactReference] = $iliasContact;
        }

        return $iliasContacts;
    }

    /**
     * @param array<string> $employeeNumbers
     * @return array
     */
    private function searchUser(array $employeeNumbers): array
    {
        if (empty($employeeNumbers)) {
            return [];
        }

        $employeeNumbers = array_map(fn($v): string => $this->database->quote(str_pad(ltrim($v, '0'), 6, '0', STR_PAD_LEFT), 'STRING'), $employeeNumbers);

        $query = "SELECT usr_data.*, usr_pref_language.value AS language, usr_pref_picture.value AS profile_image
		          FROM usr_data
		          LEFT JOIN usr_pref as usr_pref_language ON usr_pref_language.usr_id = usr_data.usr_id AND usr_pref_language.keyword = " . $this->database->quote("language", "text") . "
		          LEFT JOIN usr_pref as usr_pref_picture ON usr_pref_picture.usr_id = usr_data.usr_id AND usr_pref_picture.keyword = " . $this->database->quote("profile_image", "text") . "
		          WHERE ext_account IN (" . implode(',', $employeeNumbers) . ")";
        $query .= " ORDER BY usr_data.lastname, usr_data.firstname ";

        $r = $this->database->query($query);

        $data = array();

        while ($row = $this->database->fetchAssoc($r)) {
            $data[] = $row;
        }

        return $data;
    }

    /**
     * @param array $userData
     * @return IliasContactDto
     */
    private function mapUser(array $userData, array $userDefinedDataMap): IliasContactDto
    {
        $userId = (int)$userData['usr_id'];
        $userDefinedData = $userDefinedDataMap[$userId] ?? null;

        $profileImage = null;
        if (isset($userData['profile_image'])) {
            $personal_picture = $userData['profile_image'];
            $webspace_dir = ilFileUtils::getWebspaceDir();
            $image_file = $webspace_dir . "/usr_images/" . $personal_picture;
            if (is_file($image_file)) {
                $profileImage = $image_file;
            }
        }

        return IliasContactDto::fromValues(
            str_pad(ltrim($userData['ext_account'], '0'), 6, '0', STR_PAD_LEFT),
            $userData['title'],
            $userData['firstname'],
            $userData['lastname'],
            join(' ', [$userData['title'], $userData['firstname'], $userData['lastname']]),
            !empty($userData['phoneoffice']) ? $userData['phoneoffice'] : null,
            $userDefinedData !== null && isset($userDefinedData['f_2']) ? $userDefinedData['f_2'] : null,
            $profileImage,
            hash('sha512', (string)$userId),
        );
    }

    /**
     * @param array<int> $userIds
     * @return array<int, array>
     */
    private function getUserDefinedData(array $userIds): array
    {
        if (empty($userIds)) {
            return [];
        }

        $userIds = join(',', array_map(fn($v): string => $this->database->quote($v, 'integer'), $userIds));

        $data = [];

        $query = "SELECT * FROM udf_text WHERE usr_id IN (" . $userIds . ")";
        $res = $this->database->query($query);
        while ($row = $res->fetchRow(ilDBConstants::FETCHMODE_ASSOC)) {
            if (!isset($data[$row['usr_id']])) {
                $data[$row['usr_id']] = [];
            }
            $data[$row['usr_id']]["f_" . $row["field_id"]] = $row["value"];
        }

        $query = "SELECT * FROM udf_clob WHERE usr_id IN (" . $userIds . ")";
        $res = $this->database->query($query);
        while ($row = $res->fetchRow(ilDBConstants::FETCHMODE_ASSOC)) {
            if (!isset($data[$row['usr_id']])) {
                $data[$row['usr_id']] = [];
            }
            $data[$row['usr_id']]["f_" . $row["field_id"]] = $row["value"];
        }

        return $data;
    }
}
