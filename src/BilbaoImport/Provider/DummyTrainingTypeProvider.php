<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Provider;

use DateTime;
use DRVBund\Plugins\CGAutomation\Shared\Dto\CapacityDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\ContactDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\DummyTrainingTypeDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\DurationDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\MoneyDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\VerbalDescriptionsDto;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingForm;
use Exception;

class DummyTrainingTypeProvider
{
    /**
     * @return array<DummyTrainingTypeDto>
     * @throws Exception
     */
    public function provide(): array
    {
        return [
            $this->createDummyTrainingType(
                '10000001',
                '5520.1028',
                'Organisationslehre',
                '4650.00',
                true,
                'B',
                ['Beschäftigte'],
                CapacityDto::fromValues(8, 12, 14),
                DurationDto::fromValues(168.5, 25),
                [
                    '- Modul 1 (5520.1021) Grundlagen der Organisationslehre-Methodik, Projektmanagement, Start des Lernprojekts',
                    '- Modul 2 (5520.1022) Der Mensch in der Organisation, Change-Management und Lernprojekt',
                    '- Modul 3 (5520.1023) Prozessmanagement und Personalbedarfsermittlung',
                    '- Modul 4 (5520.1024) Aufbauorganisation und Personalbedarfsermittlung',
                    '- Modul 5 (5520.1025) Abschluss des begleitenden Lernprojekts, Vorbereitung und Abnahme der Prüfung',
                ],
                [
                    'Die Teilnehmenden erwerben Wissen sowie Fertigkeiten zur selbstständigen Planung, methodisch gestützten Durchführung und Umsetzung von Organisationsvorhaben bzw. zur effektiven Mitarbeit daran. Dazu kennen Sie Ansätze, Methoden und Techniken der Business-Analyse und Organisationsgestaltung. Sie können diese in spezifischen Projekten wie auch in ihren Linienaufgaben anwenden.',
                ],
                [
                    'Das Programm umfasst fünf verpflichtende Module, zum Teil online. Es sind Leistungsnachweise während und zum Abschluss zu erbringen - mit dem Ziel des Erwerbs eines allgemein gültigen Zertifikats.',
                    'Preis: 930 Euro pro Modul, Gesamtpreis 4650 Euro',
                ],
                [
                    'Führungskräfte und Beschäftigte mit Organisationsaufgaben',
                    'Grundlagenwissen im Bereich Kommunikation',
                    'Bereitschaft für zusätzlichen Zeitaufwand zur Bearbeitung des begleitenden Lernprojekts',
                    'außerhalb der Veranstaltungen (individuelle Beratungstermine)',
                ],
                [
                    ContactDto::fromNameAndIliasRef('Liane Schuh', '38c9a00c84bbc55c66d98b07a6b7f4b6b7dab75d0edf93b066d94dd0373c28cc337228a4652120a7fd0afcdfc8e1488b99c27896b3ba9fc08a44d9480e5bcecb'),
                ],
                [],
                [
                    '75010504',
                    '75011824',
                    '75012070',
                    '75012212',
                    '75012214',
                ],
                [
                    '5520.1021',
                    '5520.1022',
                    '5520.1023',
                    '5520.1024',
                    '5520.1025',
                ],
                [],
                [],
                true,
                true
            ),
            $this->createDummyTrainingType(
                '10000002',
                '4522.1080',
                'Fallmanagement in der Rehabilitation',
                '3940.00',
                true,
                'C',
                ['Fachberater*innen für Rehabilitation'],
                CapacityDto::fromValues(8, 12, 14),
                DurationDto::fromValues(140, 22),
                [
                    '- Modul 1 (4522.1081) Grundlagen',
                    '- Modul 2 (4522.1082) Praktische Rahmenbedingungen',
                    '- Modul 3 (4522.1083) Beratungsgrundlagen',
                    '- Modul 4 (4522.1084) Assessment und Teilhabeplan',
                    '- Modul 5 (4522.1085) Linking, Monitoring und Evaluation',
                    '- Modul 6 (4522.1086) Organisationsebene',
                    '- Modul 7 (4522.1087) Netzwerkebene',
                    '- Modul 8 (4522.1088) Begleitete Projektarbeit und Präsentationsvorbereitung',
                    '- Modul 9 (4522.1089) Tagung/ Präsentation und Abschluss mit Zertifikatsausgabe',
                ],
                [
                    'Die Fachberater*innen für Rehabilitation werden in diesem Weiterbildungsprogramm befähigt, Versicherte mit komplexen Reha-Bedarfen beratend zu begleiten sowie den Rehabilitationsprozess zu planen und zu steuern.'
                ],
                [
                    'Das Programm umfasst neun verpflichtende Module, zum Teil online.',
                    'Es kann zum Abschluss ein Leistungsnachweis erbracht werden - mit dem Ziel des Erwerbs eines allgemein gültigen Zertifikats als Fallmanager*in in der Deutschen Rentenversicherung mit gleichzeitiger Anerkennung als Case Manager*in (DGCC)',
                    'Gesamtpreis  3.940,00 Euro',
                ],
                [],
                [
                    ContactDto::fromNameAndIliasRef('Sabina Müller', '108a2b7ede2dec2ebe0a4d3315027b5b09d669c7b34e32af19fc87e79db5a1d82c134b7b83b43a8d7dba3fc5f185c56e8fb72c592817335fb2aa82e7950d109c'),
                ],
                [],
                [
                    // '75589425',
                    // '75591527',
                    // '75591528',
                    // '75591531',
                    // '75640526',
                    // '75641581',
                    // '75641582',
                    // '75685900',
                    // '75686351',
                ],
                [
//                     '4522.1081',
//                     '4522.1082',
//                     '4522.1083',
//                     '4522.1084',
//                     '4522.1085',
//                     '4522.1086',
//                     '4522.1087',
//                     '4522.1088',
//                     '4522.1089',
                ],
                [],
                [
                    ContactDto::fromNameAndIliasRef('Sabine Dalchow', '41516915f021c34ba9c0932dcddb6dd220ac904d8f22c34f62486836a3b6ff998aec9078ffbe56c2fc8e0febc3cff0a79a849cb9753c1350059a137ec68f90fa'),
                ],
                true,
                false,
            ),
            $this->createDummyTrainingType(
                '10000003',
                '5530.1035',
                'Qualitätsmanagement in den Reha-Zentren',
                '1460.00',
                true,
                'B',
                ['Beschäftigte'],
                CapacityDto::fromValues(8, 12, 14),
                DurationDto::fromValues(67, 12),
                [
                    '- Modul 1 (5530.1036): Einführung in das Qualitätsmanagement, Grundlagen interner Audits, Aufbau und Struktur von QMS Reha',
                    '- Modul 2 (5530.1037): Bewertung und Weiterentwicklung des Qualitätsmanagements, QM-bezogene Kosten',
                    '- Modul 3 (5530.1038): Kommunikationstechniken und Grundlagen des Projektmanagements',
                    '- Modul 4 (5530.1039): Prüfung für das Zertifikat "DGQ-Qualitätsbeauftragte*r im Gesundheits- und Sozialwesen" oder für das Zertifikat "DGQ-Qualitätsmanagementbeauftragte*r im Gesundheits- und Sozialwesen".',
                ],
                [
                    'Die Teilnehmenden erwerben Wissen und Fertigkeiten rund um das Qualitätsmanagmentsystem sowie um das System QMS REHA. Sie können diese Systeme in ihrer Organisation implementieren. Zudem haben sie die Möglichkeit, dieses Wissen zertifizieren zu lassen.'
                ],
                [
                    'Das Programm umfasst insgesamt drei Module und eine Prüfung. Die Module 1 und 2 werden nur im Zusammenhang angeboten. Das Modul 3 ist freiwillig. Die Anmeldung zur Prüfung ist ebefalls freiwillig, allerdings sollte eine erfolgreiche Teilnahme an den Modulen 1 bis 3 erfolgt sein. Ziel ist es, das Zertifikat DGQ Qualitätsbeauftragte*r im Geundheits- und Sozialwesen oder DGQ-Qualitätsmanagementbeauftragte*r zu erlangen.',
                    '- Preis Modul 1: 530 EUR (4 Tage)',
                    '- Preis Modul 2: 350 EUR (4 Tage mit zweimaligem Trainerwechsel)',
                    '- Preis Modul 3: 350 EUR (3 Tage)',
                    '- Preis Modul 4: 230 EUR (1 Tag)',
                    '- Gesamtpreis: 1460 EUR',
                ],
                [
                    'Für die abschließende Prüfung ist eine erfolgreiche Teilnahme an den Modulen 1 bis 3 erforderlich.',
                ],
                [],
                [],
                [],
                [
                    '5530.1036',
                    '5530.1037',
                    '5530.1038',
//                    '5530.1039',
                ],
                [],
                [],
                true,
                true
            ),
            $this->createDummyTrainingType(
                '10000004',
                '4130.1022',
                'Agile Coach Ausbildung',
                '2600.00',
                true,
                'C',
                ['Beschäftigte'],
                CapacityDto::fromValues(8, 16, 16),
                DurationDto::fromValues(116.5, 24),
                [
                    '* Kick-Off (4131.1010)',
                    '* WOL 1 (4131.1016)',
                    '* Modul 1 (4130.1011): Agiles Coaching. Fokus Person & Team',
                    '* WOL 2 (4131.1016)',
                    '* Modul 2 (4130.1012): Werkstatt #Scrum',
                    '* Modul 3 (4130.1013): Werkstatt #OKRs (Objectives and Key Results)',
                    '* Modul 4 (4130.1014): Werkstatt #Agile Moderation',
                    '* Modul 5 (4130.1015): MasterClass',
                    '* Retrospektive (4131.1017)',
                    '',
                    '',
                    'Termine 1. Reihe',
                    '* Kick-Off: 29.01.2024 (Online: ILIAS/BBB)',
                    '* Modul 1: 12.02. - 16.02.2024 (Präsenz: Winterberg)',
                    '* WOL 1: 11.03.2024 (Online: ILIAS/BBB)',
                    '* Modul 2: 18.03. - 19.03.2024 (Online: ILIAS/BBB)',
                    '* WOL 2: 15.04.2024 (Online: ILIAS/BBB)',
                    '* Modul 3: 27.05. - 28.05.2024 (Online: ILIAS/BBB)',
                    '* Modul 4: 17.06. - 19.06.2024 (Präsenz: Reinfeld)',
                    '* Modul 5: 02.09. - 06.09.2024 (Präsenz: Erkner)',
                    '* Retrospektive: 11.10.2024 (Online: ILIAS/BBB)',
                    '',
                    '',
                    'Termine 2. Reihe',
                    '* Kick-Off: 26.02.2024 (Online: ILIAS/BBB)',
                    '* Modul 1: 11.03. - 15.03.2024 (Präsenz: Winterberg)',
                    '* WOL 1: 08.04.2024 (Online: ILIAS/BBB)',
                    '* Modul 2: 15.04. - 16.04.2024 (Online: ILIAS/BBB)',
                    '* WOL 2: 13.05.2024 (Online: ILIAS/BBB)',
                    '* Modul 3: 17.06. - 18.06.2024 (Online: ILIAS/BBB)',
                    '* Modul 4: 08.07. - 10.07.2024 (Präsenz: Reinfeld)',
                    '* Modul 5: 23.09. - 27.09.2024 (Präsenz: Erkner)',
                    '* Retrospektive: 08.11.2024 (Online: ILIAS/BBB)',
                ],
                [
                    'Mit der Ausbildung zum internen Agile Coach der DRV können die Teilnehmenden - mittels erlernter Methoden und Konzepte - die Herausforderungen, die die agile Transformation unserer Organisation und auch die Entwicklung agiler Teams mit sich bringen, bewältigen.',
                ],
                [
                    'Die Agile Coach Ausbildung umfasst insgesamt fünf Module, fünf digitale Working-Out-Loud (WOL) Termine (à zwei Stunden) zwischen den Modulen, eine Kurzarbeitsprobe und endet mit einer Retrospektive. Erlerntes stellen Sie im geschützten Raum unter Beweis und erhalten ein konstruktives Experten-Feedback. Sechs bis acht Wochen nach der Ausbildung findet eine vierstündige Retrospektive statt. Nach erfolgreichem Absolvieren der Ausbildung können die Teilnehmenden ein Zertifikat mit dem dvct-Siegel erhalten. Die Anmeldung für die gesamte Agile Coach Ausbildung wird verbindlich je Reihe vorgenommen, da sich der Teilnehmerkreis pro Ausbildungsreihe nicht verändert.',
                    'Gesamtpreis: 2.600 €',
                ],
                [
                    'Die Teilnehmenden sind motiviert, bringen Berufserfahrung mit, haben Berührungspunkte zum agilen Arbeiten und sollen zukünftig als Agile Coaches eingesetzt werden.',
                    'Zur effektiven Gestaltung der Ausbildung werden Interessierte gebeten, bereits im Vorfeld zu reflektieren, welche Ziele sie verfolgen. Wo und wie soll die angestrebte Qualifizierung im eigenen bzw. übergeordneten Tätigkeitsfeld zur konkreten Anwendung kommen?',
                ],
                [
                    ContactDto::fromNameAndIliasRef('Juliane Mundt', 'd82a380beedd6fbe7442c4cbb794f67e93247783d0b9eff636cf757c2ce4eff26cf6f72b9fedc49a0dcd1e3a7d081773735c20cda705c928903d3089494f0ee7'),
                ],
                [
                    'Interaktive Theorieerarbeitung',
                    'Gruppenarbeit mit agilen Methoden',
                    'Diskussion im Plenum',
                    'erlebnisaktivierende Übungen',
                    'Agile Frameworks',
                    'Agile Moderation',
                    'Agile Games',
                    'Coaching',
                    'virtuelles Einzel-Coaching zur Haltungsreflexion bei Bedarf',
                    'Praxistransfer',
                ],
                [],
                [
//                    '4130.1011',
//                    '4130.1012',
//                    '4130.1013',
//                    '4130.1014',
//                    '4130.1015',
//                    '4130.1016',
//                    '4130.1017',
//
//                    '4131.1010',
//                    '4131.1016',
//                    '4131.1017',
//                    '4131.1018',
//                    '4131.1019',
//
//                    '5570.1010',
//                    '5570.1011',
//                    '5570.1012',
//                    '5570.1013',
//                    '5570.1014',
//                    '5570.1015',
//                    '5570.1016',
//                    '5570.1017',
//                    '5570.1018',
//                    '5570.1019',
                ],
                [],
                [
                    ContactDto::fromNameAndIliasRef('Alexandra Witt', '848b3330b80ba3329a7229017e9d87774cd04f927458ecdeb61cb61e767772426b0966623870b1bd11369410e24cb83459626a058ed656cda1d388b473d10edf'),
                ],
                true,
                false
            ),
            $this->createDummyTrainingType(
                '10000005',
                '5020.1060',
                'Basisprogramm Rehabilitationseinrichtung',
                'N/A',
                true,
                'B',
                ['Führungskräfte'],
                CapacityDto::fromValues(8, 12, 14),
                DurationDto::fromValues(78, 14),
                [
                    '- Modul 1 (5020.1050) - Kommunikation, Führung und Motivation als Teamleitung',
                    '- Modul 2 (5020.1051) - Teamsteuerung und Führung im Wandel',
                    '- Arbeitsrecht (5020.1054)',
                    '- Follow up (5021.1052)',
                ],
                [
                    // 'Die Teilnehmenden haben Klarheit über die Rolle und die Aufgaben der Führung. Sie besitzen Grundlagenwissen zur Führung, Kommunikation und Zusammenarbeit und haben Steuerungs- und Handlungskompetenz als Führungskraft. Die Teilnehmenden entwickeln Verständnis für die Begleitung von Veränderungsprozessen und haben die Fähigkeit erlernt, diese aktiv als Führungskraft zu steuern',
                    '- Klarheit über die Rolle und die Aufgaben der Führung erlangen',
                    '- Erwerb des Grundlagenwissens zu Führung, Kommunikation und Zusammenarbeit',
                    '- Steuerungs- und Handlungskompetenz als Führungskraft erwerben',
                    '- Verständnis für die Begleitung von Veränderungsprozessen entwickeln und die Fähigkeit erlernen, diese aktiv als Führungskraft zu steuern',
                ],
                [
                    'Die Führungskräfteentwicklung erfolgt im Rahmen von 3 Veranstaltungen und einem anschließenden Follow up. Eine Anmeldung wird vor Programmstart verbindlich für alle 4 Termine vorgenommen.',
                ],
                [
                    'Das Basisprogramm richtet sich an Bereichsleitungen, die erstmals Führungsverantwortung übernommen haben (maximal 1 bis 2 Jahre).',
                    'Zum Teilnehmendenkreis der Bereichsleitungen gehören auch die Personalsachbearbeitung und die Oberärzt*innen ohne Vertretung der Klinikleitung.',
                ],
                [
                    ContactDto::fromNameAndIliasRef('Grit Wannemacher', '5ed8b9d3ebd299cae5d65d3164fbb229bdd063d801f71567b46f7482502a695abfbecd644cdc0960651dbd1d8c1ec242c8ffd1c8203a24b725af434f51ffeb51'),
                ],
                [
                    'Lehrgespräch',
                    'Diskussion',
                    'Einzel- und Gruppenarbeit',
                    'Simulation praxisrelevanter Fallbeispiele',
                    'Kollegiale Beratung',
                    'Erfahrungsaustausch',
                ],
                [
                    '75728920',
                    '75728921',
                    '75728922',
                    '75728923',
                ],
                [
                    '5020.1050',
                    '5020.1051',
                    '5020.1054',
                    '5021.1052',
                ],
                [],
                [
                    ContactDto::fromNameAndIliasRef('Christine Viol', 'c5b9a0d1882d2d9fe4c2504d5447155e175db3569bab53818dec67e09f14c4fd497c73406a52cf738b4f920fc30883656c7e06762461e506d23d4b79dc7a4b86'),
                ],
                true,
                true
            ),
            $this->createDummyTrainingType(
                '10000006',
                '7500.1000',
                'Bildungsangebot des Multiprojekts rvEvolution',
                'indiv.',
                true,
                'P',
                ['Beschäftigte'],
                CapacityDto::fromValues(8, 12, 14),
                DurationDto::fromValues(16, 2),
                [
                    '- Agile Methoden',
                    '- Agiles Management',
                    '- Kollaboration',
                    '- Fachliche Qualifizierungen',
                ],
                [
                    '- Die Teilnehmenden kennen die Bedeutung von Agilität für die digitale Transformation. Sie finden sich im agilen Arbeitsumfeld zurecht, kennen das agile Mindset, agile Prinzipien sowie die Besonderheiten von Zusammenarbeit, Kommunikation und Führung im Multiprojekt.',
                    '- Sie können die im Multiprojekt eingesetzten (trägerübergreifenden) Kollaborationstools sicher nutzen.',
                    '- Sie sind entsprechend ihrer jeweiligen Rolle und Aufgaben im Projekt sowohl fachlich als auch methodisch qualifiziert.',
                ],
                [
                    'Die Bereitstellung des Bildungsangebots erfolgt ausschließlich über die Stabsstelle "Bildung und Wissensmanagement" im Multiprojekt rvEvolution.',
                ],
                [
                    'Das Bildungsangebot richtet sich ausschließlich an Beschäftigte aus oder in Zusammenhang mit dem Multiprojekt rvEvolution.',
                ],
                [
                    ContactDto::fromNameAndIliasRef('rvEvolution Bildung', 'rvEvolution'),
                ],
                null,
                [],
                [
//                     '7510.1010',
//                     '7510.1020',
//                     '7510.1050',
//
//                     '7520.1020',
//                     '7520.1030',
//                     '7520.1040',
//
//                     '7540.1010',
//
//                     '7550.1010',
//                     '7550.1020',
                ],
                [],
                [],
                false,
                false,
            ),
            $this->createDummyTrainingType(
                'dummy-5030.1065',
                '5030.1065',
                'Resilienz für Frauen in Führung-online (geh. Dienst)',
                '270.00',
                true,
                'B',
                ['Weibliche Führungskräfte'],
                CapacityDto::fromValues(8, 12, 12),
                DurationDto::fromValues(12, 2),
                [
                    'Modul 1 (5030.1061) - 07.09.2023',
                    '* Einführung und individuelle Resilienz als Kernkompetenz',
                    '* Belastung und Stress ím weiblichen Führungsalltag',
                    '* Selbsteinschätzung und Hintergründe ',
                    '* Eigenes Resilienzprofil mit persönlicher Zielsetzung',
                    '',
                    '',
                    'Modul 2 (5030.1062) - 21.09.2023',
                    '* Resilienzfaktor 1-4: Input, Best-Practices',
                    '* Persönliche Zielsetzung: Maßnahmenplanung zur nachhaltigen Umsetzung',
                    '',
                    '',
                    'Modul 3 (5030.1063) - 12.10.2023',
                    '* Maßnahmenumsetzung: Erfahrungsaustausch, Reflektion',
                    '* Evaluation des persönlichen Zielfortschritts',
                    '* Resilienzfaktor 5-7: Input, Best-Practices',
                    '* Persönliche Zielsetzung: Maßnahmenplanung für den eigenen Führungsalltag',
                    '',
                    '',
                    'Modul 4 (5030.1064) - 26.10.2023',
                    '* Maßnahmenumsetzung: Erfahrungsaustausch, Reflektion',
                    '* Evaluation des persönlichen Zielfortschritts',
                    '* Resilienzfaktor 8-10: Input, Best-Practices',
                    '* Persönliche Zielsetzung: Maßnahmen zur nachhaltigen Umsetzung',
                ],
                ['Die Teilnehmerinnen können aufgrund des Konzepts zur Resilienz Ihre eigene Resilienzkapazität einschätzen. Sie verstehen ihre persönlichen Muster, die zu einem erhöhten Stresserleben beitragen, sind sich den Zusammenhängen zwischen ihrer individuellen Resilienz und ihrem Führungsverhalten bewusst. Sie verfügen über Strategien, ihre eigene Resilienz zu stärken und können sich auf Krisensituationen vorbereiten. Sie können auf anspruchsvolle Situationen in ihrem Führungsalltag flexibel reagieren, sich gegen Stress wappnen und aus kräftezehrenden Situationen gestärkt hervorgehen.'],
                ['Das Thema umfasst 4 Module, die inhaltlich aufeinander aufbauen. Die Teilnahme an allen 4 Terminen ist verpflichtend. Keine Einzelbuchung möglich.'],
                [
                    '* Weibliche Führungskräfte mit Fokus Teamsteuerung und Führung von Mitarbeitenden, Teamleitende/gehobener Dienst.',
                    '* Bereitschaft an allen 4 online-Terminen teilzunehmen.',
                ],
                [
                    ContactDto::fromNameAndIliasRef(
                        'Jessica Michaela Tollkühn',
                        '54a3aee0261b26f8a2464ffb7773bc6a7ca085df75a0a7233d589eb8ea4ed3ae2e3ecbdb1985cc3d901c86642e8906fdfe6fdfcee214c97b39db9059f640b3b0',
                    ),
                    ContactDto::fromNameAndIliasRef(
                        'Claudia Schütze-Liß',
                        '8a1cbe4ba43f4287cd56f566f95cec9677e31fc58e638d36aa6d990aacb6d041d6f94cec9b31c0dfd5fa5b593c7a4caca2e26d33e5c618387ef338a5b3e191b5',
                    ),
                ],
                [
                    'Input',
                    'Gruppenarbeit',
                    'Einzelarbeit',
                    'Best-Practices',
                ],
                [],
                [
//                    '5030.1061',
//                    '5030.1062',
//                    '5030.1063',
//                    '5030.1064',
                ],
                [],
                [],
                true,
                false
            ),
            $this->createDummyTrainingType(
                'dummy-5030.1055',
                '5030.1055',
                'Resilienz für Frauen in Führung-online',
                '270.00',
                true,
                'B',
                ['Weibliche Führungskräfte'],
                CapacityDto::fromValues(8, 12, 12),
                DurationDto::fromValues(12, 2),
                [
                    'Modul 1 (5030.1051) - 07.09.2023',
                    '* Einführung und individuelle Resilienz als Kernkompetenz',
                    '* Belastung und Stress im weiblichen Führungsalltag',
                    '* Selbsteinschätzung, Hintergründe',
                    '* Eigenes Resilienzprofil mit persönlicher Zielsetzung',
                    '',
                    '',
                    'Modul 2 (5030.1052) - 21.09.2023',
                    '* Resilienzfaktor 1-4: Input, Best-Practices',
                    '* Persönliche Zielsetzung: Maßnahmen zur nachhaltigen Umsetzung',
                    '',
                    '',
                    'Modul 3 (5030.1053) - 12.10.2023',
                    '* Maßnahmenumsetzung: Erfahrungsaustausch, Reflektion',
                    '* Evaluation des persönliche Zielfortschritts',
                    '* Resilienzfaktoren 5-7: Input, Best-Practices',
                    '* Persönliche Zielsetzung: Maßnahmen zur nachhaltigen Umsetzung',
                    '',
                    '',
                    'Modul 4 (5030.1054) - 26.10.2023',
                    '* Maßnahmenumsetzung: Erfahrungsaustausch, Reflektion',
                    '* Evaluation des persönlichen Zielfortschritts',
                    '* Resilienzfaktoren 8-10: Input, Best-Practices',
                    '* Persönliche Zielsetzung: Maßnahmen zur nachhaltigen Umsetzung',
                ],
                ['Die Teilnehmerinnen können auf der Grundlage des Konzepts zur Resilienz Ihre eigene Resilienzkapazität einschätzen. Sie verstehen ihre persönlichen Muster, die zu einem erhöhten Stresserleben beitragen, sind sich den Zusammenhängen zwischen ihrer individuellen Resilienz und ihrem Führungsverhalten bewusst. Sie verfügen über Strategien, ihre eigene Resilienz zu stärken und können sich auf Krisensituationen vorbereiten. Sie können auf anspruchsvolle Situationen in ihrem Führungsalltag flexibel reagieren, sich gegen Stress wappnen und aus kräftezehrenden Situationen gestärkt hervorgehen.'],
                ['Das Thema umfasst 4 Module, die inhaltlich aufeinander aufbauen. Die Teilnahme an allen 4 Terminen ist verpflichtend. Keine Einzelbuchung möglich.'],
                ['* Weibliche Führungskräfte aller Führungsebenen', '* Bereitschaft an allen 4 online-Terminen teilzunehmen.'],
                [
                    ContactDto::fromNameAndIliasRef(
                        'Jessica Michaela Tollkühn',
                        '54a3aee0261b26f8a2464ffb7773bc6a7ca085df75a0a7233d589eb8ea4ed3ae2e3ecbdb1985cc3d901c86642e8906fdfe6fdfcee214c97b39db9059f640b3b0',
                    ),
                    ContactDto::fromNameAndIliasRef(
                        'Claudia Schütze-Liß',
                        '8a1cbe4ba43f4287cd56f566f95cec9677e31fc58e638d36aa6d990aacb6d041d6f94cec9b31c0dfd5fa5b593c7a4caca2e26d33e5c618387ef338a5b3e191b5',
                    ),
                ],
                [
                    'Input',
                    'Gruppenarbeit',
                    'Einzelarbeit',
                    'Best-Practices',
                ],
                [],
                [
//                    '5030.1051',
//                    '5030.1052',
//                    '5030.1053',
//                    '5030.1054',
                ],
                [],
                [],
                true,
                false
            ),
            $this->createDummyTrainingType(
                'dummy-5030.1050',
                '5030.1050',
                'Resilienz für Frauen in Führung (präsenz)',
                '360.00',
                true,
                'B',
                ['Weibliche Führungskräfte'],
                CapacityDto::fromValues(8, 12, 12),
                DurationDto::fromValues(16, 3),
                [
                    '* Einführung in die Grundlagen der Resilienz',
                    '* Individuelle Resilienz als Kernkompetenz von Führung',
                    '* Belastung und Stress im weiblichen Führungsalltag',
                    '* Selbsteinschätzung und Hintergründe',
                    '* Eigenes Resilienzprofil mit persönlicher Zielsetzung',
                    '* Individuellen Strategien zur Prävention',
                    '* Krisenbewältigung auf Basis von 10 Resilienzfaktoren',
                    '* Best Practices',
                ],
                ['Die Teilnehmerinnen können aufgrund des Konzepts zur Resilienz Ihre eigene Resilienzkapazität einschätzen. Sie verstehen ihre persönlichen Muster, die zu einem erhöhten Stresserleben beitragen, sind sich den Zusammenhängen zwischen ihrer individuellen Resilienz und ihrem Führungsverhalten bewusst. Sie verfügen über Strategien, ihre eigene Resilienz zu stärken und können sich auf Krisensituationen vorbereiten. Sie können auf anspruchsvolle Situationen in ihrem Führungsalltag flexibel reagieren, sich gegen Stress wappnen und aus kräftezehrenden Situationen gestärkt hervorgehen.'],
                ['Diese Veranstaltung wird auch online in 4 Modulen à 3 Zeitstunden unter VA-Nr. 5030.1051-1054 angeboten.'],
                ['Weibliche Führungskräfte aller Führungsebenen'],
                [
                    ContactDto::fromNameAndIliasRef(
                        'Jessica Michaela Tollkühn',
                        '54a3aee0261b26f8a2464ffb7773bc6a7ca085df75a0a7233d589eb8ea4ed3ae2e3ecbdb1985cc3d901c86642e8906fdfe6fdfcee214c97b39db9059f640b3b0',
                    ),
                    ContactDto::fromNameAndIliasRef(
                        'Claudia Schütze-Liß',
                        '8a1cbe4ba43f4287cd56f566f95cec9677e31fc58e638d36aa6d990aacb6d041d6f94cec9b31c0dfd5fa5b593c7a4caca2e26d33e5c618387ef338a5b3e191b5',
                    ),
                ],
                [
                    'Input',
                    'Gruppenarbeit',
                    'Einzelarbeit',
                    'Best-Practices',
                ],
                [],
                [
                    '5030.1050',
//                    '5030.1051',
//                    '5030.1052',
//                    '5030.1053',
//                    '5030.1054',
                ],
                [],
                [],
                true,
                true
            ),
            $this->createDummyTrainingType(
                'dummy-5020.1500',
                '5020.1500',
                'Führung - Vertiefungsprogramme',
                'N/A',
                true,
                'B',
                ['Führungskräfte'],
                CapacityDto::fromValues(8, 12, 12),
                DurationDto::fromValues(0, 15),
                [
                    '* Kick-off (5021.1500 / 5021.1600) : Start',
                    '* Modul 1 (5020.1501 / 5020.1601): Zukunftsgestalter*in und Coach im Change',
                    '* Modul 2 (5020.1502 / 5020.1602): Professionelles Kommunikationstraining',
                    '* Modul 3 (5020.1503 / 5020.1603): Beziehungsmanagement und Netzwerken',
                    '* Modul 4 (5020.1504 / 5020.1604): Agiles Denken und Handeln',
                    '* Modul 5 (5020.1505 / 5020.1605): Achtsam zu Lösungen finden',
                    '* Working Out Loud (5021.1506 / 5021.1606)',
                    '* Final Event (5021.1507 / 5021.1607): Abschluss',
                    '',
                    '',
                    'Vertiefungsprogramm Zielgruppe 1 - Starttermin am 12.01.2024',
                    'Vertiefungsprogramm Zielgruppe 1 - Starttermin am 14.06.2024',
                    'Vertiefungsprogramm Zielgruppe 2 - Starttermin am 07.06.2024',
                ],
                [
                    '* Sie reflektieren und stärken Ihre Führungsrolle mit Blick auf Vorbildfunktion, Verantwortungsübernahme und positive Außendarstellung sowie die zukunftsgerichtete Haltung als Coach in Führungssituationen.',
                    '* Sie wissen, wie Sie die Selbstorganisationsfähigkeit der Mitarbeitenden fördern.',
                    '* Sie schärfen Ihre Methoden- und Netzwerkkompetenz und das Bewusstsein für eine aktive Steuerung der Veränderungsprozesse.',
                    '* Sie kennen Strategie- und Management-Tools, um Prozesse zielorientiert zu steuern und Ihre Mitarbeitenden empathisch zu führen.',
                    '* Sie verstehen das Prinzip der Achtsamkeit und den Einfluss auf den eigenen Stresslevel, zeigen eine lösungsorientierte Haltung im Umgang mit Belastungssituationen im Team und können Ressourcen und Einergiequellen erkennen.',
                ],
                ['Die Führungskräfteentwicklung erfolgt im Blended-Learning-Format mit einem Kick-off, 5 Modulen sowie Working Out Loud und einem Final Event als Abschlussveranstaltung. Da sich der Teilnehmerkreis nicht ändert, wird die Anmeldung verbindlich für alle 9 Veranstaltungen vorgenommen.'],
                [
                    'Das Vertiefungsprogramm richtet sich an Führungskräfte mit mindestens fünf Jahren Führungserfahrung und Grundlagenwissen zu den Themen Führung, Kommunikation und Veränderungsmanagement. Differenziert wird in zwei Zielgruppen:',
                    '* Zielgruppe 1: Fokus auf Teamsteuerung und Mitarbeiterführung, beispielsweise Team- und Sachgebietsleitende',
                    '* Zielgruppe 2: Fokus auf strategische Leitung und Führen von Führungskräften, beispielsweise Dezernatsleitende',
                ],
                [
                    ContactDto::fromNameAndIliasRef('Cornelia Lier', '6c86841c0aee5e49cf7b1749801222a4b822393b1b4e3268d2018361c82a01442f865b5ec8013733240654fa7c6bd769c6ff4e74d96687ac83e757742a587f9d'),
                ],
                [
                    'Präsentation',
                    'Diskussion',
                    'Einzel- und Gruppenarbeit',
                    'Simulation von Fallbeispielen',
                    'Einzelcoaching und Rollenspiel mit Trainingsschauspieler',
                    'Erfahrungsaustausch',
                ],
                [],
                [
//                    '5020.1501',
//                    '5020.1502',
//                    '5020.1503',
//                    '5020.1504',
//                    '5020.1505',
//
//                    '5020.1601',
//                    '5020.1602',
//                    '5020.1603',
//                    '5020.1604',
//                    '5020.1605',
//
//                    '5021.1500',
//                    '5021.1506',
//                    '5021.1507',
//
//                    '5021.1600',
//                    '5021.1606',
//                    '5021.1607',
                ],
                [],
                [],
                true,
                false
            ),
            $this->createDummyTrainingType(
                'dummy-5020.1300',
                '5020.1300',
                'Führung - Einstiegsprogramme',
                'N/A',
                true,
                'B',
                ['Führungskräfte'],
                CapacityDto::fromValues(8, 12, 12),
                DurationDto::fromValues(0, 14),
                [
                    '* Kick-off (5021.1300 / 5021.1400): Start',
                    '* Modul 1 (5020.1301 / 5020.1401): Rollenverständnis und wirksame Führungskommunikation',
                    '* Modul 2 (5020.1302 / 5020.1402): Teams in Veränderung begleiten',
                    '* Modul 3 (5020.1303 / 5020.1403): Führen auf Distanz',
                    '* Modul 4 (5020.1304 / 5020.1404): Agiles Mindset und agile Methoden',
                    '* Modul 5 (5020.1305 / 5020.1405): Gesunde Führung und Resilienz',
                    '* Kollegiale Beratung ( 5021.1306 / 5021.1406)',
                    '* Final Event (5021.1307 / 5021.1407): Abschluss',
                    '',
                    '',
                    'Einstiegsprogramm Zielgruppe 1 - Starttermin am 19.01.2024',
                    'Einstiegsprogramm Zielgruppe 2 - Starttermin am 07.06.2024',
                ],
                [
                    '* Sie erlangen Klarheit über die eigene Führungsrolle.',
                    '* Sie erwerben Grundlagenwissen zu Führung, wertschätzender Kommunikation und Zusammenarbeit und kennen die Herausforderungen in der Führung auf Distanz.',
                    '* Sie erlernen Tools zur Entwicklung der Steuerungs- und Handlungskompetenz als Führungskraft, insbesondere in der Begleitung von Veränderungsprozessen.',
                    '* Sie führen Mitarbeitende und steuern Teams empathisch und zielführend.',
                    '* Sie sind sich bewusst, dass eine gesunde Führung Achtsamkeit und Förderung der Resilienz im Berufsalltag erfordert.',
                ],
                ['Die Führungskräfteentwicklung erfolgt im Blended-Learning-Format mit einem Kick-off, 5 Modulen sowie der kollegialen Beratung und einem Final Event als Abschlussveranstaltung. Da sich der Teilnehmerkreis nicht ändert, wird die Anmeldung verbindlich für alle 9 Veranstaltungen vorgenommen.'],
                [
                    'Das Einstiegsprogramm richtet sich an Führungskräfte, die erstmalig Führungsverantwortung übernommen haben (maximal 24 Monate Führungserfahrung).',
                    'Differenziert wird in zwei Zielgruppen:',
                    '* Zielgruppe 1: Fokus auf Teamsteuerung und Mitarbeiterführung, beispielsweise Team- und Sachgebietsleitende',
                    '* Zielgruppe 2: Fokus auf strategische Leitung und Führen von Führungskräften, beispielsweise Dezernatsleitende',
                ],
                [
                    ContactDto::fromNameAndIliasRef('Cornelia Lier', '6c86841c0aee5e49cf7b1749801222a4b822393b1b4e3268d2018361c82a01442f865b5ec8013733240654fa7c6bd769c6ff4e74d96687ac83e757742a587f9d'),
                ],
                [
                    'Präsentation',
                    'Diskussion',
                    'Einzel- und Gruppenarbeit',
                    'Simulation von Fallbeispielen',
                    'Erfahrungsaustausch'
                ],
                [],
                [
//                    '5020.1301',
//                    '5020.1302',
//                    '5020.1303',
//                    '5020.1304',
//                    '5020.1305',
//
//                    '5020.1401',
//                    '5020.1402',
//                    '5020.1403',
//                    '5020.1404',
//                    '5020.1405',
//
//                    '5021.1300',
//                    '5021.1306',
//                    '5021.1307',
//
//                    '5021.1400',
//                    '5021.1406',
//                    '5021.1407',
                ],
                [],
                [],
                true,
                false,
            ),
        ];
    }

    /**
     * @param string $dummyId
     * @param string $short
     * @param string $title
     * @param string $price
     * @param bool $vatExempt
     * @param string $accountingCategoryChar
     * @param array<string> $targetGroup
     * @param CapacityDto $capacity
     * @param DurationDto $duration
     * @param array<string> $contentShort
     * @param array<string> $goals
     * @param array<string> $notes
     * @param array<string> $preconditions
     * @param array<ContactDto> $contentContacts
     * @param array<string>|null $methods
     * @param array<string> $containsTrainingIds
     * @param array<string> $replacedTrainingTypeNumbers
     * @param array<string> $contentLong
     * @param array<ContactDto> $eventContacts
     * @param bool $determinationOfDemand
     * @return DummyTrainingTypeDto
     * @throws Exception
     */
    private function createDummyTrainingType(
        string      $dummyId,
        string      $short,
        string      $title,
        string      $price,
        bool        $vatExempt,
        string      $accountingCategoryChar,
        array       $targetGroup,
        CapacityDto $capacity,
        DurationDto $duration,
        array       $contentShort,
        array       $goals = [],
        array       $notes = [],
        array       $preconditions = [],
        array       $contentContacts = [],
        array       $methods = null,
        array       $containsTrainingIds = [],
        array       $replacedTrainingTypeNumbers = [],
        array       $contentLong = [],
        array       $eventContacts = [],
        bool        $determinationOfDemand = true,
        bool        $includeReplacedTrainingTypeTrainings = true
    ): DummyTrainingTypeDto
    {
        $accountingCategories = [
            'A' => 'kostenfrei (Grundsatz und Querschnitt)',
            'B' => 'kostenpflichtig (virtuelles Budget)',
            'C' => 'kostenpflichtig',
            'P' => 'kostenfrei (trägerüb. Projekt/WS)',
        ];
        $accountingCategory = $accountingCategories[$accountingCategoryChar] ?? null;
        if (!$accountingCategory) {
            throw new Exception('Unknown accounting category: ' . $accountingCategoryChar);
        }

        $trainingType = new TrainingTypeDto();
        $trainingType->id = $short;
        $trainingType->short = $short;
        $trainingType->planVariant = '01';
        $trainingType->objectType = 'D';
        $trainingType->trainingGroupId = '';
        $trainingType->title = $title;
        $trainingType->validFrom = new DateTime('today');
        $trainingType->validFrom->setTime(0, 0, 0);
        $trainingType->validFrom->setDate(intval($trainingType->validFrom->format('Y')), 1, 1);
        $trainingType->validUntil = DateTime::createFromFormat('!Y-m-d', '9999-12-31');
        $trainingType->trainingForm = TrainingForm::Presence();
        $trainingType->verbalDescriptions = new VerbalDescriptionsDto();
        $trainingType->verbalDescriptions->contentShort = $contentShort;
        $trainingType->verbalDescriptions->contentLong = $contentLong;
        $trainingType->verbalDescriptions->goals = $goals;
        $trainingType->verbalDescriptions->methods = $methods ?? [];
        $trainingType->verbalDescriptions->notes = $notes;
        $trainingType->verbalDescriptions->preconditions = $preconditions;
        $trainingType->verbalDescriptions->targetGroup = $targetGroup;
        $trainingType->showInBrochure = true;
        $trainingType->showInIntranet = true;
        $trainingType->showInInternet = false;
        $trainingType->priceVatExempt = $vatExempt;
        $trainingType->priceInternal = new MoneyDto();
        $trainingType->priceInternal->amount = $price;
        $trainingType->priceInternal->currency = 'EUR';
        $trainingType->priceExternal = new MoneyDto();
        $trainingType->priceExternal->amount = $price;
        $trainingType->priceExternal->currency = 'EUR';
        $trainingType->capacity = $capacity;
        $trainingType->targetGroup = 'trägerüb.';
        $trainingType->accountingChar = strtoupper($accountingCategoryChar);
        $trainingType->accountingCategory = $accountingCategory;
        $trainingType->duration = $duration;
        $trainingType->contentResponsible = $contentContacts;
        $trainingType->eventResponsible = $eventContacts;
        $trainingType->determinationOfDemand = $determinationOfDemand;

        return DummyTrainingTypeDto::fromValues(
            $dummyId,
            $trainingType,
            $containsTrainingIds,
            $replacedTrainingTypeNumbers,
            $includeReplacedTrainingTypeTrainings
        );
    }
}
