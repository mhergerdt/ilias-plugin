<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Repository;

use DRVBund\Plugins\CGAutomation\Shared\Dto\BilbaoDataDto;

interface BilbaoDataRepository
{
    public function save(BilbaoDataDto $data): void;

    public function load(): BilbaoDataDto;
}

