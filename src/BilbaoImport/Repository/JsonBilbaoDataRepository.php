<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Repository;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use DRVBund\Plugins\CGAutomation\Ilias\Adapter\FilesystemAdapter;
use DRVBund\Plugins\CGAutomation\Shared\Dto\BilbaoDataDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\CapacityDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\ContactDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\DurationDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\IliasContactDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\LocationDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\MoneyDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\ObjectLinkDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\OccupancyDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingGroupDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingScheduleEntryDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\VerbalDescriptionsDto;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\FilePath;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingForm;

class JsonBilbaoDataRepository implements BilbaoDataRepository
{
    private ?FilePath $filePath;
    private FilesystemAdapter $filesystem;

    public function __construct(FilesystemAdapter $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function setFilePath(FilePath $filePath): void
    {
        $this->filePath = $filePath;
    }

    public function getFilePath(): FilePath
    {
        if (!isset($this->filePath)) {
            throw new \Exception('File path not defined');
        }

        return $this->filePath;
    }

    public function save(BilbaoDataDto $data): void
    {
        $jsonString = json_encode(
            [
                'exportedAt' => $this->formatDateTime($data->exportedAt, new DateTimeZone('UTC')),
                'trainingGroups' => array_map(function (TrainingGroupDto $trainingGroupDto) {
                    return [
                        'id' => $trainingGroupDto->id,
                        'short' => $trainingGroupDto->short,
                        'title' => $trainingGroupDto->title,
                        'parent' => $trainingGroupDto->parent,
                    ];
                }, $data->trainingGroups),
                'trainingTypes' => array_map(
                    function (TrainingTypeDto $trainingType) {
                        $accountingCategory = strtolower($trainingType->accountingChar) === 'a' ? 'kostenfrei' : $trainingType->accountingCategory;

                        return [
                            'id' => $trainingType->id,
                            'short' => $trainingType->short,
                            'planVariant' => $trainingType->planVariant,
                            'objectType' => $trainingType->objectType,
                            'title' => $trainingType->title,
                            'validFrom' => $this->formatDateTime($trainingType->validFrom),
                            'validUntil' => $this->formatDateTime($trainingType->validUntil),
                            'trainingGroupId' => $trainingType->trainingGroupId,
                            'trainingForm' => $trainingType->trainingForm,
                            'verbalDescription' => [
                                'content' => [
                                    'short' => $trainingType->verbalDescriptions->contentShort,
                                    'long' => $trainingType->verbalDescriptions->contentLong,
                                ],
                                'notes' => $trainingType->verbalDescriptions->notes,
                                'targetGroup' => $trainingType->verbalDescriptions->targetGroup,
                                'goals' => $trainingType->verbalDescriptions->goals,
                                'preconditions' => $trainingType->verbalDescriptions->preconditions,
                                'methods' => $trainingType->verbalDescriptions->methods,
                            ],
                            'price' => [
                                'vatExempt' => $trainingType->priceVatExempt,
                                'internal' => $trainingType->priceInternal,
                                'external' => $trainingType->priceExternal,
                            ],
                            'capacity' => $trainingType->capacity,
                            'publication' => [
                                'brochure' => $trainingType->showInBrochure,
                                'intranet' => $trainingType->showInIntranet,
                                'internet' => $trainingType->showInInternet,
                            ],
                            'determinationOfDemand' => $trainingType->determinationOfDemand,
                            'targetGroup' => $trainingType->targetGroup,
                            'accountingCategory' => $trainingType->accountingChar ? "{$trainingType->accountingChar} - {$accountingCategory}" : '',
                            'duration' => [
                                'hours' => $trainingType->duration->hours,
                                'days' => $trainingType->duration->days,
                            ],
                            'contact' => [
                                'content' => array_map([$this, 'mapContact'], $trainingType->contentResponsible),
                                'event' => array_map([$this, 'mapContact'], $trainingType->eventResponsible),
                                'speaker' => array_map([$this, 'mapContact'], $trainingType->speaker),
                            ],
                            'belongsTo' => $trainingType->belongsTo,
                            'requires' => $trainingType->requires,
                            'intendedFor' => $trainingType->intendedFor,
                            'hostedBy' => $trainingType->hostedBy,
                            'mandatoryFor' => $trainingType->mandatoryFor,
                            'needs' => $trainingType->needs,
                            'mediates' => $trainingType->mediates,
                            'trainings' => array_map(
                                function (TrainingDto $training) {
                                    if ($training->location->streetLine2 === 'https://lernwelt.drv-bund.de') {
                                        $training->location->streetLine2 = '';
                                    }

                                    return [
                                        'id' => $training->id,
                                        'short' => $training->short,
                                        'planVariant' => $training->planVariant,
                                        'objectType' => $training->objectType,
                                        'trainingForm' => $training->trainingForm,
                                        'title' => $training->title,
                                        'validFrom' => $this->formatDateTime($training->validFrom),
                                        'validUntil' => $this->formatDateTime($training->validUntil),
                                        'location' => $training->location,
                                        'registrationDeadline' => $this->formatDateTime($training->registrationDeadline),
                                        'capacity' => [
                                            'minimum' => $training->capacity->minimum,
                                            'optimum' => $training->capacity->optimum,
                                            'maximum' => $training->capacity->maximum,
                                            'free' => $training->occupancy->free,
                                            'waiting' => $training->occupancy->waiting,
                                        ],
                                        'duration' => [
                                            'days' => $training->duration->days,
                                            'hours' => $training->duration->hours,
                                        ],
                                        'price' => [
                                            'internal' => $training->priceInternal,
                                            'external' => $training->priceExternal,
                                        ],
                                        'contact' => [
                                            'content' => array_map([$this, 'mapContact'], $training->contentResponsible),
                                            'event' => array_map([$this, 'mapContact'], $training->eventResponsible),
                                            'speaker' => array_map([$this, 'mapContact'], $training->speaker),
                                        ],
                                        'rooms' => $training->rooms,
                                        'iliasBookingCode' => $training->iliasBookingCode,
                                        'status' => $training->status,
                                        'allowedParticipantHashes' => $training->allowedParticipantMailHashes,
                                        'schedule' => array_map(
                                            function (TrainingScheduleEntryDto $schedule) {
                                                return [
                                                    'date' => $this->formatDateTime($schedule->date),
                                                    'begin' => $schedule->startTime,
                                                    'end' => $schedule->endTime,
                                                ];
                                            },
                                            $training->schedule,
                                        ),
                                    ];
                                },
                                $trainingType->trainings,
                            ),
                        ];
                    },
                    array_values($data->trainingTypes)
                ),
                'iliasContacts' => array_map(
                    function (IliasContactDto $iliasContact) {
                        return [
                            'title' => $iliasContact->title,
                            'firstName' => $iliasContact->firstName,
                            'lastName' => $iliasContact->lastName,
                            'fullName' => $iliasContact->fullName,
                            'phoneNumber' => $iliasContact->phoneNumber,
                            'email' => $iliasContact->email,
                            'image' => $iliasContact->image,
                            'iliasContactReference' => $iliasContact->iliasContactReference,
                        ];
                    },
                    $data->iliasContacts
                ),
            ],
            JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
        );

        $this->filesystem->put((string)$this->getFilePath(), $jsonString);
    }

    private function mapContact(ContactDto $contactDto, bool $skipEmail = false): array
    {
        $data = [
            'personalNumber' => $contactDto->employeeNumber,
            'fullName' => $contactDto->fullName,
        ];

        if ($skipEmail === false && isset($contactDto->email)) {
            $data['email'] = $contactDto->email;
        }

        $data['iliasContactReference'] = $contactDto->iliasReference ?? null;

        return $data;
    }

    private function formatDateTime(?DateTimeInterface $dateTime, DateTimeZone $timeZone = null): ?string
    {
        if ($dateTime === null) {
            return null;
        }

        if ($timeZone) {
            $dateTime = new DateTime($dateTime->format(DateTimeInterface::ATOM));
            $dateTime->setTimezone($timeZone);
        }

        return $dateTime->format(DateTimeInterface::ATOM);
    }

    public function load(): BilbaoDataDto
    {
        $data = json_decode($this->filesystem->read((string)$this->getFilePath()), true);

        $bilbaoData = new BilbaoDataDto();
        $bilbaoData->exportedAt = new DateTimeImmutable($data['exportedAt']);
        $bilbaoData->trainingGroups = array_map(function (array $trainingGroupData) {
            $trainingGroupDto = TrainingGroupDto::fromValues(
                $trainingGroupData['id'],
                $trainingGroupData['short'],
                $trainingGroupData['title'],
                $trainingGroupData['parent'],
            );
            return $trainingGroupDto;
        }, $data['trainingGroups']);
        $bilbaoData->trainingTypes = array_map(function (array $trainingTypeData) {
            $trainingTypeDto = new TrainingTypeDto();
            $trainingTypeDto->id = $trainingTypeData['id'];
            $trainingTypeDto->short = $trainingTypeData['short'];
            $trainingTypeDto->planVariant = $trainingTypeData['planVariant'];
            $trainingTypeDto->objectType = $trainingTypeData['objectType'];
            $trainingTypeDto->title = $trainingTypeData['title'];
            if (!empty($trainingTypeData['validFrom'])) {
                $trainingTypeDto->validFrom = new \DateTime($trainingTypeData['validFrom']);
            }
            if (!empty($trainingTypeData['validUntil'])) {
                $trainingTypeDto->validUntil = new \DateTime($trainingTypeData['validUntil']);
            }
            $trainingTypeDto->trainingGroupId = $trainingTypeData['trainingGroupId'];
            $trainingTypeDto->trainingForm = new TrainingForm($trainingTypeData['trainingForm']);
            $trainingTypeDto->verbalDescriptions = new VerbalDescriptionsDto();
            $trainingTypeDto->verbalDescriptions->contentShort = $trainingTypeData['verbalDescription']['content']['short'];
            $trainingTypeDto->verbalDescriptions->contentLong = $trainingTypeData['verbalDescription']['content']['long'];
            $trainingTypeDto->verbalDescriptions->notes = $trainingTypeData['verbalDescription']['notes'];
            $trainingTypeDto->verbalDescriptions->targetGroup = $trainingTypeData['verbalDescription']['targetGroup'];
            $trainingTypeDto->verbalDescriptions->goals = $trainingTypeData['verbalDescription']['goals'];
            $trainingTypeDto->verbalDescriptions->preconditions = $trainingTypeData['verbalDescription']['preconditions'];
            $trainingTypeDto->verbalDescriptions->methods = $trainingTypeData['verbalDescription']['methods'];
            $trainingTypeDto->priceVatExempt = $trainingTypeData['price']['vatExempt'];
            $trainingTypeDto->priceInternal = MoneyDto::fromValues(
                $trainingTypeData['price']['internal']['amount'],
                $trainingTypeData['price']['internal']['current'] ?? 'EUR',
            );
            $trainingTypeDto->priceExternal = MoneyDto::fromValues(
                $trainingTypeData['price']['external']['amount'],
                $trainingTypeData['price']['external']['current'] ?? 'EUR',
            );
            $trainingTypeDto->capacity = CapacityDto::fromValues(
                $trainingTypeData['capacity']['minimum'],
                $trainingTypeData['capacity']['optimum'],
                $trainingTypeData['capacity']['maximum'],
            );
            $trainingTypeDto->showInBrochure = (bool)$trainingTypeData['publication']['brochure'];
            $trainingTypeDto->showInIntranet = (bool)$trainingTypeData['publication']['intranet'];
            $trainingTypeDto->showInInternet = (bool)$trainingTypeData['publication']['internet'];
            $trainingTypeDto->determinationOfDemand = $trainingTypeData['determinationOfDemand'];
            $trainingTypeDto->targetGroup = $trainingTypeData['targetGroup'];

            $accounting = explode(' - ', $trainingTypeData['accountingCategory']);
            $trainingTypeDto->accountingCategory = $accounting[1] ?? '';
            $trainingTypeDto->accountingChar = $accounting[0] ?? '';

            $trainingTypeDto->duration = DurationDto::fromValues(
                (float)($trainingTypeData['duration']['hours'] ?? 0),
                (float)($trainingTypeData['duration']['days'] ?? 0),
            );

            $trainingTypeDto->contentResponsible = array_map([$this, 'mapToContact'], $trainingTypeData['contact']['content']);
            $trainingTypeDto->eventResponsible = array_map([$this, 'mapToContact'], $trainingTypeData['contact']['event']);
            $trainingTypeDto->speaker = array_map([$this, 'mapToContact'], $trainingTypeData['contact']['speaker']);

            $trainingTypeDto->belongsTo = array_map([$this, 'mapToObjectLink'], $trainingTypeData['belongsTo']);
            $trainingTypeDto->requires = array_map([$this, 'mapToObjectLink'], $trainingTypeData['requires']);
            $trainingTypeDto->intendedFor = array_map([$this, 'mapToObjectLink'], $trainingTypeData['intendedFor']);
            $trainingTypeDto->hostedBy = array_map([$this, 'mapToObjectLink'], $trainingTypeData['hostedBy']);
            $trainingTypeDto->mandatoryFor = array_map([$this, 'mapToObjectLink'], $trainingTypeData['mandatoryFor']);
            $trainingTypeDto->needs = array_map([$this, 'mapToObjectLink'], $trainingTypeData['needs']);
            $trainingTypeDto->mediates = array_map([$this, 'mapToObjectLink'], $trainingTypeData['mediates']);

            foreach ($trainingTypeData['trainings'] as $trainingData) {
                $trainingDto = new TrainingDto();
                $trainingDto->id = $trainingData['id'];
                $trainingDto->short = $trainingData['short'];
                $trainingDto->planVariant = $trainingData['planVariant'];
                $trainingDto->objectType = $trainingData['objectType'];
                $trainingDto->trainingForm = array_key_exists('trainingForm', $trainingData) ? new TrainingForm($trainingData['trainingForm']) : $trainingTypeDto->trainingForm;
                $trainingDto->title = $trainingData['title'];
                if (!empty($trainingData['validFrom'])) {
                    $trainingDto->validFrom = new \DateTime($trainingData['validFrom']);
                }
                if (!empty($trainingData['validUntil'])) {
                    $trainingDto->validUntil = new \DateTime($trainingData['validUntil']);
                }
                $trainingDto->location = LocationDto::fromValues(
                    $trainingData['location']['id'] ?? '',
                    $trainingData['location']['title'] ?? '',
                    $trainingData['location']['streetLine1'] ?? '',
                    $trainingData['location']['streetLine2'] ?? '',
                    $trainingData['location']['streetNumber'] ?? '',
                    $trainingData['location']['zipCode'] ?? '',
                    $trainingData['location']['city'] ?? '',
                );
                $trainingDto->registrationDeadline = null;
                if (!empty($trainingData['registrationDeadline'])) {
                    $trainingDto->registrationDeadline = new \DateTime($trainingData['registrationDeadline']);
                }
                $trainingDto->capacity = CapacityDto::fromValues(
                    $trainingData['capacity']['minimum'],
                    $trainingData['capacity']['optimum'],
                    $trainingData['capacity']['maximum'],
                );
                $trainingDto->occupancy = OccupancyDto::fromValues(
                    $trainingData['capacity']['free'],
                    $trainingData['capacity']['waiting'],
                );
                $trainingDto->duration = DurationDto::fromValues(
                    $trainingData['duration']['hours'],
                    $trainingData['duration']['days'],
                );
                $trainingDto->priceInternal = MoneyDto::fromValues(
                    $trainingData['price']['internal']['amount'],
                    $trainingData['price']['internal']['current'] ?? 'EUR',
                );
                $trainingDto->priceExternal = MoneyDto::fromValues(
                    $trainingData['price']['external']['amount'],
                    $trainingData['price']['external']['current'] ?? 'EUR',
                );

                $trainingDto->contentResponsible = array_map([$this, 'mapToContact'], $trainingData['contact']['content']);
                $trainingDto->eventResponsible = array_map([$this, 'mapToContact'], $trainingData['contact']['event']);
                $trainingDto->speaker = array_map([$this, 'mapToContact'], $trainingData['contact']['speaker']);

                $trainingDto->rooms = array_map([$this, 'mapToObjectLink'], $trainingData['rooms']);
                $trainingDto->iliasBookingCode = $trainingData['iliasBookingCode'];
                $trainingDto->status = $trainingData['status'];
                $trainingDto->allowedParticipantMailHashes = $trainingData['allowedParticipantHashes'] ?? [];
                $trainingDto->schedule = array_map(function (array $entryData) {
                    $trainingScheduleEntryDto = new TrainingScheduleEntryDto();
                    $trainingScheduleEntryDto->date = new \DateTime($entryData['date']);
                    $trainingScheduleEntryDto->startTime = $entryData['begin'];
                    $trainingScheduleEntryDto->endTime = $entryData['end'];

                    return $trainingScheduleEntryDto;
                }, $trainingData['schedule']);

                $trainingTypeDto->trainings[] = $trainingDto;
            }

            return $trainingTypeDto;
        }, $data['trainingTypes']);

        return $bilbaoData;
    }

    private function mapToContact(array $contactData): ContactDto
    {
        return ContactDto::fromValues(
            $contactData['personalNumber'] ?? '',
            $contactData['fullName'] ?? '',
            $contactData['email'] ?? null,
            $contactData['iliasContactReference'] ?? null,
        );
    }

    private function mapToObjectLink(array $objectLinkData): ObjectLinkDto
    {
        return ObjectLinkDto::fromValues(
            $objectLinkData['id'],
            $objectLinkData['type'],
            $objectLinkData['short'],
            $objectLinkData['label'],
        );
    }
}
