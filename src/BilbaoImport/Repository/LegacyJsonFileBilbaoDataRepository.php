<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Repository;

use DateTime;
use DateTimeInterface;
use DateTimeZone;
use DRVBund\Plugins\CGAutomation\Ilias\Adapter\FilesystemAdapter;
use DRVBund\Plugins\CGAutomation\Shared\Dto\BilbaoDataDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\ContactDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\IliasContactDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingGroupDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingScheduleEntryDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\FilePath;

class LegacyJsonFileBilbaoDataRepository implements BilbaoDataRepository
{
    private ?FilePath $filePath;
    private BilbaoDataRepository $bilbaoDataRepository;
    private FilesystemAdapter $filesystem;

    public function __construct(BilbaoDataRepository $bilbaoDataRepository, FilesystemAdapter $filesystem)
    {
        $this->bilbaoDataRepository = $bilbaoDataRepository;
        $this->filesystem = $filesystem;
    }

    public function setFilePath(FilePath $filePath): void
    {
        $this->filePath = $filePath;
    }

    public function getFilePath(): FilePath
    {
        if (!isset($this->filePath)) {
            throw new \Exception('File path not defined');
        }

        return $this->filePath;
    }

    public function save(BilbaoDataDto $data): void
    {
        $this->bilbaoDataRepository->save($data);

        $jsonString = json_encode(
            [
                'exportedAt' => $this->formatDateTime($data->exportedAt, new DateTimeZone('UTC')),
                'trainingGroups' => array_map(function (TrainingGroupDto $trainingGroup) {
                    return [
                        'id' => $trainingGroup->id,
                        'short' => $trainingGroup->short,
                        'title' => $trainingGroup->title,
                        'parent' => $trainingGroup->parent,
                    ];
                }, $data->trainingGroups),
                'trainingTypes' => array_map(
                    function (TrainingTypeDto $trainingType) {
                        $accountingCategory = strtolower($trainingType->accountingChar) === 'a' ? 'kostenfrei' : $trainingType->accountingCategory;

                        return [
                            'id' => $trainingType->id,
                            'short' => $trainingType->short,
                            'planVariant' => $trainingType->planVariant,
                            'objectType' => $trainingType->objectType,
                            'title' => $trainingType->title,
                            'validFrom' => $this->formatDateTime($trainingType->validFrom),
                            'validUntil' => $this->formatDateTime($trainingType->validUntil),
                            'trainingGroupId' => $trainingType->trainingGroupId,
                            'trainingForm' => $trainingType->trainingForm,
                            'verbalDescription' => [
                                'content' => [
                                    'short' => $trainingType->verbalDescriptions->contentShort,
                                    'long' => $trainingType->verbalDescriptions->contentLong,
                                ],
                                'notes' => $trainingType->verbalDescriptions->notes,
                                'targetGroup' => $trainingType->verbalDescriptions->targetGroup,
                                'goals' => $trainingType->verbalDescriptions->goals,
                                'preconditions' => $trainingType->verbalDescriptions->preconditions,
                                'methods' => $trainingType->verbalDescriptions->methods,
                            ],
                            'price' => [
                                'vatExempt' => $trainingType->priceVatExempt,
                                'internal' => $trainingType->priceInternal,
                                'external' => $trainingType->priceExternal,
                            ],
                            'capacity' => $trainingType->capacity,
                            'publication' => [
                                'brochure' => $trainingType->showInBrochure,
                                'intranet' => $trainingType->showInIntranet,
                                'internet' => $trainingType->showInInternet,
                            ],
                            'determinationOfDemand' => $trainingType->determinationOfDemand,
                            'targetGroup' => $trainingType->targetGroup,
                            'accountingCategory' => $trainingType->accountingChar ? "{$trainingType->accountingChar} - {$accountingCategory}" : '',
                            'duration' => [
                                'hours' => $trainingType->duration->hours,
                                'days' => $trainingType->duration->days,
                            ],
                            'contact' => [
                                'content' => array_map([$this, 'mapContact'], $trainingType->contentResponsible),
                                'event' => array_map([$this, 'mapContact'], $trainingType->eventResponsible),
                                'speaker' => array_map([$this, 'mapContact'], $trainingType->speaker),
                            ],
                            'belongsTo' => $trainingType->belongsTo,
                            'requires' => $trainingType->requires,
                            'intendedFor' => $trainingType->intendedFor,
                            'hostedBy' => $trainingType->hostedBy,
                            'mandatoryFor' => $trainingType->mandatoryFor,
                            'needs' => $trainingType->needs,
                            'mediates' => $trainingType->mediates,
                            'trainings' => array_map(
                                function (TrainingDto $training) {
                                    if ($training->location->streetLine2 === 'https://lernwelt.drv-bund.de') {
                                        $training->location->streetLine2 = '';
                                    }

                                    return [
                                        'id' => $training->id,
                                        'short' => $training->short,
                                        'planVariant' => $training->planVariant,
                                        'objectType' => $training->objectType,
                                        'title' => $training->title,
                                        'validFrom' => $this->formatDateTime($training->validFrom),
                                        'validUntil' => $this->formatDateTime($training->validUntil),
                                        'location' => $training->location,
                                        'registrationDeadline' => $this->formatDateTime($training->registrationDeadline),
                                        'capacity' => [
                                            'minimum' => $training->capacity->minimum,
                                            'optimum' => $training->capacity->optimum,
                                            'maximum' => $training->capacity->maximum,
                                            'free' => $training->occupancy->free,
                                            'waiting' => $training->occupancy->waiting,
                                        ],
                                        'duration' => [
                                            'days' => $training->duration->days,
                                            'hours' => $training->duration->hours,
                                        ],
                                        'price' => [
                                            'internal' => $training->priceInternal,
                                            'external' => $training->priceExternal,
                                        ],
                                        'contact' => [
                                            'content' => array_map([$this, 'mapContact'], $training->contentResponsible),
                                            'event' => array_map([$this, 'mapContact'], $training->eventResponsible),
                                            'speaker' => array_map([$this, 'mapContact'], $training->speaker),
                                        ],
                                        'rooms' => $training->rooms,
                                        'iliasBookingCode' => $training->iliasBookingCode,
                                        'status' => $training->status,
                                        'schedule' => array_map(
                                            function (TrainingScheduleEntryDto $schedule) {
                                                return [
                                                    'date' => $this->formatDateTime($schedule->date),
                                                    'begin' => $schedule->startTime,
                                                    'end' => $schedule->endTime,
                                                ];
                                            },
                                            $training->schedule,
                                        ),
                                    ];
                                },
                                $trainingType->trainings,
                            ),
                        ];
                    },
                    array_values($data->trainingTypes)
                ),
                'iliasContacts' => array_map(
                    function (IliasContactDto $iliasContact) {
                        return [
                            'title' => $iliasContact->title,
                            'firstName' => $iliasContact->firstName,
                            'lastName' => $iliasContact->lastName,
                            'fullName' => $iliasContact->fullName,
                            'phoneNumber' => $iliasContact->phoneNumber,
                            'email' => $iliasContact->email,
                            'image' => $iliasContact->image,
                            'iliasContactReference' => $iliasContact->iliasContactReference,
                        ];
                    },
                    $data->iliasContacts
                ),
            ],
            JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
        );

        $this->filesystem->put((string)$this->getFilePath(), $jsonString);
    }

    private function mapContact(ContactDto $contactDto, bool $skipEmail = false): array
    {
        $data = [
            'personalNumber' => $contactDto->employeeNumber,
            'fullName' => $contactDto->fullName,
        ];

        if ($skipEmail === false && isset($contactDto->email)) {
            $data['email'] = $contactDto->email;
        }

        $data['iliasContactReference'] = $contactDto->iliasReference ?? null;

        return $data;
    }

    private function formatDateTime(?DateTimeInterface $dateTime, DateTimeZone $timeZone = null): ?string
    {
        if ($dateTime === null) {
            return null;
        }

        if ($timeZone) {
            $dateTime = new DateTime($dateTime->format(DateTimeInterface::ATOM));
            $dateTime->setTimezone($timeZone);
            return preg_replace('/\+\d{2}:\d{2}$/', '.000Z', $dateTime->format(DateTimeInterface::ATOM));
        }

        return preg_replace('/(\+\d{2}:\d{2})$/', '.000$1', $dateTime->format(DateTimeInterface::ATOM));
    }

    public function load(): BilbaoDataDto
    {
        return $this->bilbaoDataRepository->load();
    }
}
