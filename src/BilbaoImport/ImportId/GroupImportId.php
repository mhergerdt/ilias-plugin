<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\ImportId;

class GroupImportId extends BilbaoImportId
{
    protected function getTypeCode(): string
    {
        return 't'; // Code for Training
    }
}
