<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\ImportId;

class CategoryImportId extends BilbaoImportId
{
    protected function getTypeCode(): string
    {
        return 'tg'; // Code for TrainingGroup
    }
}
