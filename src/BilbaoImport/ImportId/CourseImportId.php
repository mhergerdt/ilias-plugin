<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\ImportId;

class CourseImportId extends BilbaoImportId
{
    protected function getTypeCode(): string
    {
        return 'tt'; // Code for TrainingType
    }
}
