<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\ImportId;

use Exception;

abstract class BilbaoImportId
{
    public const MAX_LENGTH = 50;

    private string $id;

    public final function __construct(string $id)
    {
        $this->id = sprintf('bilbao_%s_%s', $this->getTypeCode(), $id);
        if (strlen($this->id) > self::MAX_LENGTH) {
            throw new Exception(sprintf('ImportId "%s" is too long', $this->id));
        }
    }

    public static function asString(string $id): string
    {
        return (new static($id))->getValue();
    }

    public function getValue(): string
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return $this->id;
    }

    /**
     * @return string A short code of the type e.g. "tg" for TrainingGroup
     */
    protected abstract function getTypeCode(): string;
}
