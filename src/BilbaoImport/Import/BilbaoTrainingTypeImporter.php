<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Import;

use DateTimeImmutable;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Import\Dto\TrainingTypeImportFilesDto;
use DRVBund\Plugins\CGAutomation\CsvParser\BooleanParser;
use DRVBund\Plugins\CGAutomation\CsvParser\CsvFileParser;
use DRVBund\Plugins\CGAutomation\CsvParser\CsvFileParserConfig;
use DRVBund\Plugins\CGAutomation\CsvParser\DateTimeParser;
use DRVBund\Plugins\CGAutomation\CsvParser\Exception\CsvColumnCountMismatch;
use DRVBund\Plugins\CGAutomation\CsvParser\FloatParser;
use DRVBund\Plugins\CGAutomation\CsvParser\IntegerParser;
use DRVBund\Plugins\CGAutomation\CsvParser\StringMoneyAmountParser;
use DRVBund\Plugins\CGAutomation\CsvParser\StringTimeParser;
use DRVBund\Plugins\CGAutomation\Shared\Dto\CapacityDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\ContactDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\DurationDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\MoneyDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\VerbalDescriptionsDto;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingForm;
use Exception;

class BilbaoTrainingTypeImporter
{
    private CsvFileParser $csvFileParser;

    public function __construct(CsvFileParser $csvFileParser)
    {
        $config = new CsvFileParserConfig();
        $config->delimiter = ';';
        $config->escape = '"';
        $config->hasHeader = true;

        $this->csvFileParser = $csvFileParser->setConfig($config);
        $dateTimeParser = new DateTimeParser('!Ymd');
        $this->csvFileParser->addCellParser('date', $dateTimeParser);
        $this->csvFileParser->addCellParser('int', new IntegerParser());
        $this->csvFileParser->addCellParser('float', new FloatParser());
        $this->csvFileParser->addCellParser('bool', new BooleanParser());
        $this->csvFileParser->addCellParser('stringMoneyAmount', new StringMoneyAmountParser());
        $this->csvFileParser->addCellParser('stringTime', new StringTimeParser());
    }

    /**
     * @return array<string, TrainingTypeDto>
     * @throws CsvColumnCountMismatch
     * @throws Exception
     */
    public function import(TrainingTypeImportFilesDto $files): array
    {
        $trainingTypes = $this->importTrainingTypes((string)$files->trainingTypes);
        $trainingTypes = $this->importTrainingTypeVerbalDescriptions((string)$files->trainingTypesVerbalDescriptions, $trainingTypes);
        $trainingTypes = $this->importTrainingTypeReferences((string)$files->trainingTypesReferences, $trainingTypes);

        return $trainingTypes;
    }

    /**
     * @param string $file
     * @return array<string, TrainingTypeDto>
     * @throws CsvColumnCountMismatch
     * @throws Exception
     */
    private function importTrainingTypes(string $file): array
    {
        $columnTypes = [
            'string',
            'string',
            'string',
            'string',
            'string',
            'date',
            'date',
            'string',
            'string',
            'bool',
            'stringMoneyAmount',
            'string',
            'stringMoneyAmount',
            'string',
            'int',
            'int',
            'int',
            'bool',
            'bool',
            'bool',
            'bool',
            'string',
            'string',
            'string',
            'float',
            'float',
        ];

        $trainingTypes = [];
        $today = new DateTimeImmutable();
        foreach ($this->csvFileParser->parseFile($file, $columnTypes) as $row) {
            if ($row['ENDDA'] < $today) {
                continue;
            }

            $trainingTypeDto = new TrainingTypeDto();
            $trainingTypeDto->planVariant = trim($row["PLVAR"]);
            $trainingTypeDto->objectType = trim($row["OTYPE"]);
            $trainingTypeDto->id = trim($row["OBJID"]);
            $trainingTypeDto->short = trim($row["SHORT"]);
            $trainingTypeDto->title = trim(preg_replace('/^"(.*)"$/u', '$1', preg_replace('/\\\"/u', '"', (string)$row["STEXT"])));
            $trainingTypeDto->validFrom = $row["BEGDA"];
            $trainingTypeDto->validUntil = $row["ENDDA"];
            $trainingTypeDto->trainingGroupId = trim($row["TGROUP_ID"]);
            $trainingTypeDto->trainingForm = new TrainingForm(trim($row["TFORM"]));
            $trainingTypeDto->showInBrochure = $row["BBROSCH"];
            $trainingTypeDto->priceInternal = new MoneyDto();
            $trainingTypeDto->priceInternal->amount = trim($row["PREIS_INTERN"]);
            $trainingTypeDto->priceInternal->currency = !empty($row['IWAER']) ? trim($row['IWAER']) : 'EUR';
            $trainingTypeDto->priceExternal = new MoneyDto();
            $trainingTypeDto->priceExternal->amount = trim($row["PREIS_EXTERN"]);
            $trainingTypeDto->priceExternal->currency = !empty($row['EWAER']) ? trim($row['EWAER']) : 'EUR';
            $trainingTypeDto->capacity = new CapacityDto();
            $trainingTypeDto->capacity->minimum = $row["KAP_MIN"];
            $trainingTypeDto->capacity->optimum = $row["KAP_OPT"];
            $trainingTypeDto->capacity->maximum = $row["KAP_MAX"];
            $trainingTypeDto->showInIntranet = $row["ANZ_INTRANET"];
            $trainingTypeDto->showInInternet = $row["ANZ_INTERNET"];
            $trainingTypeDto->determinationOfDemand = $row["ANZ_BEDARF"];
            $trainingTypeDto->priceVatExempt = $row["UMSATZ_FREI"];
            $trainingTypeDto->targetGroup = trim($row["ZIELGRUPPE"]);
            $trainingTypeDto->accountingChar = trim($row["ABRECH_ID"]);
            $trainingTypeDto->accountingCategory = trim($row["ABRECH_KAT"]);
            $trainingTypeDto->duration = new DurationDto();
            $trainingTypeDto->duration->days = $row["NDAYS"];
            $trainingTypeDto->duration->hours = $row["NHOURS"];
            $trainingTypeDto->verbalDescriptions = new VerbalDescriptionsDto();

            $trainingTypes[$trainingTypeDto->id] = $trainingTypeDto;
        }

        return $trainingTypes;
    }

    /**
     * @param string $file
     * @param array<string, TrainingTypeDto> $trainingTypes
     * @return array<string, TrainingTypeDto>
     * @throws CsvColumnCountMismatch
     * @throws Exception
     */
    private function importTrainingTypeVerbalDescriptions(string $file, array $trainingTypes): array
    {
        $columnTypes = [
            'string',
            'string',
            'string',
        ];

        $verbalDescriptionsMapping = [
            'Veranstaltungsinhalt' => 'contentShort',
            'Inhalt (lang)' => 'contentLong',
            'Hinweise' => 'notes',
            'Zielgruppe' => 'targetGroup',
            'Ziele' => 'goals',
            'Voraussetzung' => 'preconditions',
            'Methoden' => 'methods',
            'Titel' => 'title',
        ];

        foreach ($this->csvFileParser->parseFile($file, $columnTypes) as $row) {
            $trainingTypeId = trim($row["TTYP_ID"]);
            $verbalDescriptionName = trim($row["SUTXT"]);
            /** @var string $verbalDescriptionContent */
            $verbalDescriptionContent = $row["TLINE"];

            $verbalDescriptionContent = preg_replace('/\t/u', '', $verbalDescriptionContent); // Remove tabs
            $verbalDescriptionContent = preg_replace('/\x{0096}/u', '-', $verbalDescriptionContent); // Replace em dash
            $verbalDescriptionContent = preg_replace('/•/u', '-', $verbalDescriptionContent); // Replace bullet
            $verbalDescriptionContent = preg_replace('/^ +/u', '', $verbalDescriptionContent); // Remove leading spaces
            $verbalDescriptionContent = preg_replace('/\xc2\xa0/', ' ', $verbalDescriptionContent); // Replace non-breaking space
            $verbalDescriptionContent = preg_replace('/ $/u', '', $verbalDescriptionContent); // Remove trailing space
            $verbalDescriptionContent = preg_replace('/\xe2\x80\x83/', '', $verbalDescriptionContent); // Remove em space

            if (!isset($trainingTypes[$trainingTypeId])) {
                continue;
            }

            $field = $verbalDescriptionsMapping[$verbalDescriptionName] ?? null;

            if ($field !== null) {
                $trainingTypes[$trainingTypeId]->verbalDescriptions->map($field, $verbalDescriptionContent);
            }
        }

        $verbalDescriptionFields = VerbalDescriptionsDto::getFields();
        foreach ($trainingTypes as $trainingType) {
            foreach ($verbalDescriptionFields as $field) {
                $value = $trainingType->verbalDescriptions->$field;

                if (is_array($value)) {
                    $joinedValue = trim(join(' ', $value));
                    if (preg_match('/^(keine?|entf.llt)[".]?$/iu', $joinedValue)) {
                        $trainingType->verbalDescriptions->$field = [];
                    }

                    if ($field === 'title') {
                        $trainingType->title = preg_replace('/^"(.*)"$/u', '$1', $joinedValue);
                        $trainingType->verbalDescriptions->title = null;
                    }

                    if ($field === 'targetGroup' && count($value) > 0) {
                        $content = trim(trim(join(' ', $value)), ',');
                        $lines = array_map(fn($v) => trim($v), explode(',', $content));
                        $linesFiltered = array_values(array_filter($lines, fn($v) => substr($v, 0, 1) === strtoupper(substr($v, 0, 1))));
                        $trainingType->verbalDescriptions->targetGroup = count($lines) === count($linesFiltered) ? $linesFiltered : [$content];
                    }

                    if ($field === 'methods' && count($value) > 0) {
                        $trainingType->verbalDescriptions->methods = array_map(
                            fn($v) => trim($v),
                            explode(
                                ',',
                                preg_replace(
                                    '/,+/',
                                    ',',
                                    join(',', $value)
                                )
                            )
                        );
                    }

                    if (is_array($trainingType->verbalDescriptions->$field)) {
                        $trainingType->verbalDescriptions->$field = array_values(array_filter($trainingType->verbalDescriptions->$field, fn(string $v): bool => !empty(trim($v))));
                    }
                }
            }

            $trainingType->title = preg_replace('/ +/', ' ', $trainingType->title);
        }

        return $trainingTypes;
    }

    /**
     * @param string $file
     * @param array<string, TrainingTypeDto> $trainingTypes
     * @return array<string, TrainingTypeDto>
     * @throws CsvColumnCountMismatch
     * @throws Exception
     */
    private function importTrainingTypeReferences(string $file, array $trainingTypes): array
    {
        $columnTypes = [
            'string',
            'string',
            'string',
            'string',
            'string',
            'string',
        ];

        $mapping = [
            'A003' => 'belongsTo',
            'A029' => 'requires',
            'A033' => 'intendedFor',
            'A036' => 'hostedBy',
            'A615' => 'mandatoryFor',
            'A022' => 'needs',
            'AZ30' => 'mediates',
        ];

        $contactReferences = [
            'A026' => 'speaker',
            'A090' => 'eventResponsible',
            'AZ01' => 'contentResponsible',
        ];

        foreach ($this->csvFileParser->parseFile($file, $columnTypes) as $row) {
            $trainingTypeId = trim($row['OBJID']);
            $objId = trim($row['V_OBJID']);
            $objType = trim($row['V_OTYPE']);
            $objShort = trim($row['V_SHORT']);
            $objName = trim($row['V_STEXT']);
            $objRelation = trim($row['RELAT']);

            $trainingTypeDto = $trainingTypes[$trainingTypeId] ?? null;

            if ($trainingTypeDto === null) {
                continue;
            }

            $field = $contactReferences[$objRelation] ?? null;
            if ($field !== null) {
                if (!isset($trainingTypeDto->$field)) {
                    $trainingTypeDto->$field = [];
                }
                $contactDto = new ContactDto();
                $contactDto->employeeNumber = $objId;
                $contactDto->fullName = trim($objName);

                $trainingTypeDto->$field[] = $contactDto;
                continue;
            }

            $ref = [
                'id' => $objId,
                'type' => $objType,
                'short' => $objShort,
                'label' => $objName,
            ];
            $field = $mapping[$objRelation] ?? null;
            if ($field !== null) {
                if (!isset($trainingTypeDto->$field)) {
                    $trainingTypeDto->$field = [];
                }
                $trainingTypeDto->$field[] = $ref;
            }
        }

        return $trainingTypes;
    }
}
