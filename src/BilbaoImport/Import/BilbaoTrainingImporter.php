<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Import;

use DRVBund\Plugins\CGAutomation\BilbaoImport\Import\Dto\TrainingImportFilesDto;
use DRVBund\Plugins\CGAutomation\CsvParser\BooleanParser;
use DRVBund\Plugins\CGAutomation\CsvParser\CsvFileParser;
use DRVBund\Plugins\CGAutomation\CsvParser\CsvFileParserConfig;
use DRVBund\Plugins\CGAutomation\CsvParser\DateTimeParser;
use DRVBund\Plugins\CGAutomation\CsvParser\Exception\CsvColumnCountMismatch;
use DRVBund\Plugins\CGAutomation\CsvParser\FloatParser;
use DRVBund\Plugins\CGAutomation\CsvParser\IntegerParser;
use DRVBund\Plugins\CGAutomation\CsvParser\NullableParser;
use DRVBund\Plugins\CGAutomation\CsvParser\StringMoneyAmountParser;
use DRVBund\Plugins\CGAutomation\CsvParser\StringTimeParser;
use DRVBund\Plugins\CGAutomation\Shared\Dto\CapacityDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\ContactDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\DurationDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\LocationDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\MoneyDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\ObjectLinkDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\OccupancyDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingScheduleEntryDto;

class BilbaoTrainingImporter
{
    private CsvFileParser $csvFileParser;

    public function __construct(CsvFileParser $csvFileParser)
    {
        $config = new CsvFileParserConfig();
        $config->delimiter = ';';
        $config->escape = '"';
        $config->hasHeader = true;

        $this->csvFileParser = $csvFileParser->setConfig($config);

        $dateTimeParser = new DateTimeParser('!Ymd');
        $this->csvFileParser->addCellParser('date', $dateTimeParser);
        $this->csvFileParser->addCellParser('dateNullable', new NullableParser($dateTimeParser, ['', '00.00.0000', '00000000']));
        $this->csvFileParser->addCellParser('int', new IntegerParser());
        $this->csvFileParser->addCellParser('float', new FloatParser());
        $this->csvFileParser->addCellParser('bool', new BooleanParser());
        $this->csvFileParser->addCellParser('stringMoneyAmount', new StringMoneyAmountParser());
        $this->csvFileParser->addCellParser('stringTime', new StringTimeParser());
    }

    /**
     * @param TrainingImportFilesDto $trainingFiles
     * @return array<string, TrainingDto>
     * @throws CsvColumnCountMismatch
     */
    public function import(TrainingImportFilesDto $trainingFiles): array
    {
        $trainings = $this->importTrainings((string)$trainingFiles->trainings);
        $trainings = $this->importTrainingsResources((string)$trainingFiles->trainingsResources, $trainings);
        $trainings = $this->importTrainingsSchedules((string)$trainingFiles->trainingsSchedules, $trainings);
        $trainings = $this->importTrainingsParticipants((string)$trainingFiles->trainingsAllowedParticipants, $trainings);

        return $trainings;
    }

    /**
     * @param string $file
     * @return array<string, TrainingDto>
     * @throws CsvColumnCountMismatch
     */
    private function importTrainings(string $file): array
    {
        $trainings = [];

        $columnTypes = [
            'string', //PLVAR
            'string', //OTYPE
            'string', //OBJID
            'string', //SHORT
            'string', //STEXT
            'date', //BEGDA
            'date', //ENDDA
            'string', //ISTAT
            'string', //TTYP_ID
            'string', //ORT_ID
            'string', //ORT_TXT
            'string', //ORT_PLZ
            'string', //ORT_ORT
            'string', //ORT_STRAS
            'string', //ORT_HAUSNR
            'string', //ORT_STRS2
            'dateNullable', //DAT_MELDE
            'int', //KAP_MIN
            'int', //KAP_OPT
            'int', //KAP_MAX
            'float', //NDAYS
            'float', //NHOURS
            'stringMoneyAmount', //IKOST
            'string', //IWAER
            'stringMoneyAmount', //EKOST
            'string', //EWAER
            'int', //FCONT
            'int', //WCONT
            'string', //BUCHUNGSCODE
        ];

        $today = new \DateTimeImmutable();
        foreach ($this->csvFileParser->parseFile($file, $columnTypes) as $row) {
            if ($row['ENDDA'] < $today) {
                continue;
            }

            $training = new TrainingDto();
            $training->id = trim($row['OBJID']);
            $training->trainingTypeId = trim($row['TTYP_ID']);
            $training->short = trim($row['SHORT']);
            $training->planVariant = trim($row['PLVAR']);
            $training->objectType = trim($row['OTYPE']);
            $training->title = trim(preg_replace('/^"(.*)"$/u', '$1', preg_replace('/\\\"/u', '"', (string)$row["STEXT"])));
            $training->validFrom = $row['BEGDA'];
            $training->validUntil = $row['ENDDA'];
            $training->status = trim($row['ISTAT']);
            $training->iliasBookingCode = trim($row['BUCHUNGSCODE']);
            $training->location = new LocationDto();
            $training->location->id = trim($row['ORT_ID']);
            $training->location->title = trim($row['ORT_TXT']);
            $training->location->streetLine1 = trim($row['ORT_STRAS']);
            $training->location->streetLine2 = trim($row['ORT_STRS2']);
            $training->location->streetNumber = trim($row['ORT_HAUSNR']);
            $training->location->zipCode = trim($row['ORT_PLZ']);
            $training->location->city = trim($row['ORT_ORT']);
            $training->registrationDeadline = $row['DAT_MELDE'];
            $training->capacity = new CapacityDto();
            $training->capacity->minimum = $row['KAP_MIN'];
            $training->capacity->optimum = $row['KAP_OPT'];
            $training->capacity->maximum = $row['KAP_MAX'];
            $training->occupancy = new OccupancyDto();
            $training->occupancy->free = $row['FCONT'];
            $training->occupancy->waiting = $row['WCONT'];
            $training->duration = new DurationDto();
            $training->duration->days = $row['NDAYS'];
            $training->duration->hours = $row['NHOURS'];
            $training->priceInternal = new MoneyDto();
            $training->priceInternal->amount = trim($row['IKOST']);
            $training->priceInternal->currency = !empty($row['IWAER']) ? trim($row['IWAER']) : 'EUR';
            $training->priceExternal = new MoneyDto();
            $training->priceExternal->amount = trim($row['EKOST']);
            $training->priceExternal->currency = !empty($row['EWAER']) ? trim($row['EWAER']) : 'EUR';

            $trainings[$training->id] = $training;
        }

        return $trainings;
    }

    /**
     * @param string $file
     * @param array<string, TrainingDto> $trainings
     * @return array<string, TrainingDto>
     * @throws CsvColumnCountMismatch
     */
    private function importTrainingsResources(string $file, array $trainings): array
    {
        $columnTypes = [
            'string', // OBJID
            'string', // V_OBJID
            'string', // V_OTYPE
            'string', // V_SHORT
            'string', // V_STEXT
            'string', // V_MAIL
            'date', // REBEG
            'date', // REEND
            'stringTime', // BEGUZ
            'stringTime', // ENDUZ
            'string', // RELAT
        ];

        $roomMapping = [
            'G' => 'rooms',
        ];

        $contactMapping = [
            'P' => 'speaker',
            'H' => 'speaker',
        ];

        foreach ($this->csvFileParser->parseFile($file, $columnTypes) as $row) {
            $trainingId = trim($row['OBJID']);
            $linkType = trim($row['RELAT']);
            $type = trim($row['V_OTYPE']);

            if (!isset($trainings[$trainingId])) {
                continue;
            }

            if ($linkType !== 'A023') {
                throw new \Exception("Unsupported link type: \"$linkType\"");
            }

            $training = $trainings[$trainingId];

            if (array_key_exists($type, $contactMapping)) {
                $contact = new ContactDto();
                $contact->employeeNumber = trim($row['V_OBJID']);
                $contact->fullName = trim($row['V_STEXT']);
                $contact->email = trim($row['V_MAIL']);
                $training->{$contactMapping[$type]}[] = $contact;
                continue;
            }

            if (array_key_exists($type, $roomMapping)) {
                $objLink = new ObjectLinkDto();
                $objLink->id = trim($row['V_OBJID']);
                $objLink->type = trim($row['V_OTYPE']);
                $objLink->short = trim($row['V_SHORT']);
                $objLink->label = trim($row['V_STEXT']);

                $training->{$roomMapping[$type]}[] = $objLink;
                continue;
            }
        }

        return $trainings;
    }

    /**
     * @param string $file
     * @param array<string, TrainingDto> $trainings
     * @return array<string, TrainingDto>
     * @throws CsvColumnCountMismatch
     */
    private function importTrainingsSchedules(string $file, array $trainings): array
    {
        $columnTypes = [
            'string', //OBJID
            'date',//EVDAT
            'stringTime',//BEGUZ
            'stringTime',//ENDUZ
        ];

        foreach ($this->csvFileParser->parseFile($file, $columnTypes) as $row) {
            $trainingId = trim($row['OBJID']);

            if (!isset($trainings[$trainingId])) {
                continue;
            }

            $schedule = new TrainingScheduleEntryDto();
            $schedule->date = $row['EVDAT'];
            $schedule->startTime = trim($row['BEGUZ']);
            $schedule->endTime = trim($row['ENDUZ']);

            $trainings[$trainingId]->schedule[] = $schedule;
        }

        return $trainings;
    }

    /**
     * @param string $file
     * @param array<string, TrainingDto> $trainings
     * @return array<string, TrainingDto>
     * @throws CsvColumnCountMismatch
     */
    private function importTrainingsParticipants(string $file, array $trainings): array
    {
        $columnTypes = [
            'string', //OBJID
            'string', //MAIL_HASH
        ];

        foreach ($this->csvFileParser->parseFile($file, $columnTypes) as $row) {
            $trainingId = trim($row['OBJID']);
            $hash = trim($row['MAIL_HASH']);

            if (!isset($trainings[$trainingId]) || empty($hash)) {
                continue;
            }

            $trainings[$trainingId]->allowedParticipantMailHashes[] = $hash;
        }

        return $trainings;
    }
}
