<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Import;

use DateTime;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Import\Dto\BilbaoImportFilesDto;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Import\Dto\TrainingImportFilesDto;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Import\Dto\TrainingTypeImportFilesDto;
use DRVBund\Plugins\CGAutomation\Ilias\Adapter\FilesystemAdapter;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\DirectoryPath;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\FilePath;
use Exception;
use ilFileUtils;

class BilbaoImportOrchestrator
{
    private FilesystemAdapter $filesystem;
    private BilbaoImporter $bilbaoImporter;
    private DirectoryPath $unzipDir;

    public function __construct(
        FilesystemAdapter $filesystem,
        BilbaoImporter $bilbaoImporter,
        DirectoryPath $unzipDir
    ) {
        $this->filesystem = $filesystem;
        $this->bilbaoImporter = $bilbaoImporter;
        $this->unzipDir = $unzipDir;
    }

    public function import(FilePath $archive): void
    {
        $this->unzipFilesToTempDirectory($archive);
        $importFiles = $this->resolveImportFiles();
        $this->bilbaoImporter->import($importFiles);
    }

    private function unzipFilesToTempDirectory(FilePath $archive): void
    {
        $unzipDirString = (string)$this->unzipDir;
        $tmpArchivePathString = (string)$this->unzipDir->join($archive->getFileName());

        if ($this->filesystem->hasDir($unzipDirString)) {
            $this->filesystem->deleteDir($unzipDirString);
        }
        $this->filesystem->createDir($unzipDirString);

        $this->filesystem->copy((string)$archive , $tmpArchivePathString);
        ilFileUtils::unzip((string)(new DirectoryPath(ilFileUtils::getWebspaceDir()))->join($tmpArchivePathString), true, true);
        $this->filesystem->delete($tmpArchivePathString);
    }

    private function resolveImportFiles(): BilbaoImportFilesDto
    {
        $currentYearDirectory = $this->unzipDir->join('current_year');
        $nextYearDirectory = $this->unzipDir->join('next_year');

        $this->filesystem->createDir((string)$currentYearDirectory);
        $this->filesystem->createDir((string)$nextYearDirectory);

        $today = new DateTime();
        $today->setTime(0, 0, 0);

        $nextYear = (int)$today->format('Y') + 1;
        $nextYearFileRegex = sprintf('/(LSO_.*)_NY%s_(.*\.csv)$/iu', $nextYear);

        foreach ($this->filesystem->listContents((string)$this->unzipDir) as $file) {
            if (!$file->isFile() || empty($file->getPath())) {
                continue;
            }

            $filePath = FilePath::fromPath($file->getPath());
            if (preg_match($nextYearFileRegex, $filePath->getFileName())) {
                $this->filesystem->rename(
                    (string)$filePath,
                    (string)$nextYearDirectory->join(preg_replace($nextYearFileRegex, '$1_$2', $filePath->getFileName()))
                );
            } else if (preg_match('/LSO_(?!.*_NY\\d+_).*\\.csv$/iu', $filePath->getFileName())) {
                $this->filesystem->rename(
                    (string)$filePath,
                    (string)$currentYearDirectory->join($filePath->getFileName())
                );
            }
        }

        $cutoffDate = new DateTime();
        $cutoffDate->setDate((int)$cutoffDate->format('Y'), 11, 1);
        $cutoffDate->setTime(0, 0, 0);

        $importDirectory = $currentYearDirectory;
        if ($today >= $cutoffDate && count($this->filesystem->listContents((string)$nextYearDirectory)) > 0) {
            $importDirectory = $nextYearDirectory;
        }

        return $this->matchFiles($importDirectory);
    }

    private function matchFiles(DirectoryPath $filesPath): BilbaoImportFilesDto
    {
        $fileMatcher = [
            'trainingGroups' => [
                'file' => null,
                'regex' => '/LSO_Trainingsgruppen_\d+_\d+\.csv/iu',
            ],
            'trainingTypes' => [
                'file' => null,
                'regex' => '/LSO_Trainingstypen_\d+_\d+\.csv/iu',
            ],
            'trainingTypesReferences' => [
                'file' => null,
                'regex' => '/LSO_Trainingstypen_Verkn.{0,2}pfungen_\d+_\d+\.csv/iu',
            ],
            'trainingTypesVerbalDescriptions' => [
                'file' => null,
                'regex' => '/LSO_Trainingstypen_Verb_Beschreibungen_\d+_\d+\.csv/iu',
            ],
            'trainings' => [
                'file' => null,
                'regex' => '/LSO_Trainings_\d+_\d+\.csv/iu',
            ],
            'trainingsResources' => [
                'file' => null,
                'regex' => '/LSO_Trainings_Ressourcen_\d+_\d+\.csv/iu',
            ],
            'trainingsSchedules' => [
                'file' => null,
                'regex' => '/LSO_Trainings_Ablauf_\d+_\d+\.csv/iu',
            ],
            'trainingsAllowedParticipants' => [
                'file' => null,
                'regex' => '/LSO_Trainings_TN_\d+_\d+\.csv/iu',
            ],
        ];

        foreach ($this->filesystem->listContents((string)$filesPath) as $file) {
            foreach ($fileMatcher as $key => $item) {
                if ('' === $file->getPath()) {
                    continue;
                }

                $filePath = FilePath::fromPath($file->getPath());

                if (preg_match($item['regex'], $filePath->getFileName())) {
                    $fileMatcher[$key]['file'] = $filePath;
                }
            }
        }

        $errors = [];
        foreach ($fileMatcher as $key => $item) {
            if ($item['file'] === null) {
                $errors[] = "No file found for \"$key\"";
            }
        }

        if (count($errors) > 0) {
            throw new Exception('Not all files found. ' . implode($errors), );
        }

        $bilbaoImportFiles = new BilbaoImportFilesDto();
        /** @psalm-suppress PossiblyNullPropertyAssignmentValue */
        $bilbaoImportFiles->trainingGroupFile = $fileMatcher['trainingGroups']['file'];
        /** @psalm-suppress PossiblyNullArgument */
        $bilbaoImportFiles->trainingTypeFiles = new TrainingTypeImportFilesDto(
            $fileMatcher['trainingTypes']['file'],
            $fileMatcher['trainingTypesReferences']['file'],
            $fileMatcher['trainingTypesVerbalDescriptions']['file'],
        );
        /** @psalm-suppress PossiblyNullArgument */
        $bilbaoImportFiles->trainingFiles = new TrainingImportFilesDto(
            $fileMatcher['trainings']['file'],
            $fileMatcher['trainingsResources']['file'],
            $fileMatcher['trainingsSchedules']['file'],
            $fileMatcher['trainingsAllowedParticipants']['file'],
        );

        return $bilbaoImportFiles;
    }
}
