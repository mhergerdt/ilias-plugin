<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Import;

use DRVBund\Plugins\CGAutomation\CsvParser\CsvFileParser;
use DRVBund\Plugins\CGAutomation\CsvParser\CsvFileParserConfig;
use DRVBund\Plugins\CGAutomation\CsvParser\Exception\CsvColumnCountMismatch;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingGroupDto;

class BilbaoTrainingGroupImporter
{
    private CsvFileParser $csvFileParser;

    public function __construct(CsvFileParser $csvFileParser)
    {
        $config = new CsvFileParserConfig();
        $config->delimiter = ';';
        $config->escape = '"';
        $config->hasHeader = true;

        $this->csvFileParser = $csvFileParser->setConfig($config);
    }

    /**
     * @param string $file
     * @return array<TrainingGroupDto>
     * @throws CsvColumnCountMismatch
     */
    public function import(string $file): array
    {
        $columnTypes = [
            'string', //OBJID
            'string', //SHORT
            'string', //STEXT
            'string', //PARENT
        ];

        $trainingGroups = [];

        foreach ($this->csvFileParser->parseFile($file, $columnTypes) as $row) {
            $trainingGroups[] = TrainingGroupDto::fromValues(
                $row['OBJID'],
                $row['SHORT'],
                $row['STEXT'],
                $row['PARENT'],
            );
        }

        return $trainingGroups;
    }
}
