<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Import\Dto;

use DRVBund\Plugins\CGAutomation\Shared\ValueObject\FilePath;

class BilbaoImportFilesDto
{
    public FilePath $trainingGroupFile;
    public TrainingTypeImportFilesDto $trainingTypeFiles;
    public TrainingImportFilesDto $trainingFiles;
}
