<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Import\Dto;

use DRVBund\Plugins\CGAutomation\Shared\ValueObject\FilePath;

class TrainingImportFilesDto
{
    public FilePath $trainings;
    public FilePath $trainingsResources;
    public FilePath $trainingsSchedules;
    public FilePath $trainingsAllowedParticipants;

    public function __construct(
        FilePath $trainings,
        FilePath $trainingsResources,
        FilePath $trainingsSchedules,
        FilePath $trainingsAllowedParticipants
    ) {
        $this->trainings = $trainings;
        $this->trainingsResources = $trainingsResources;
        $this->trainingsSchedules = $trainingsSchedules;
        $this->trainingsAllowedParticipants = $trainingsAllowedParticipants;
    }
}
