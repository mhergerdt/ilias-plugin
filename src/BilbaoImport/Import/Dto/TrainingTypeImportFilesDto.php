<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Import\Dto;

use DRVBund\Plugins\CGAutomation\Shared\ValueObject\FilePath;

class TrainingTypeImportFilesDto
{
    public FilePath $trainingTypes;
    public FilePath $trainingTypesReferences;
    public FilePath $trainingTypesVerbalDescriptions;

    public function __construct(FilePath $trainingTypes, FilePath $trainingTypesReferences, FilePath $trainingTypesVerbalDescriptions)
    {
        $this->trainingTypes = $trainingTypes;
        $this->trainingTypesReferences = $trainingTypesReferences;
        $this->trainingTypesVerbalDescriptions = $trainingTypesVerbalDescriptions;
    }
}
