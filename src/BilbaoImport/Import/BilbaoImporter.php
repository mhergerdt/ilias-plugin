<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Import;

use DateTimeImmutable;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Import\Dto\BilbaoImportFilesDto;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Provider\DummyTrainingTypeProvider;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Provider\IliasContactProvider;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Repository\BilbaoDataRepository;
use DRVBund\Plugins\CGAutomation\CsvParser\Exception\CsvColumnCountMismatch;
use DRVBund\Plugins\CGAutomation\Shared\Dto\BilbaoDataDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\ContactDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\DummyTrainingTypeDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\IliasContactDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingForm;

class BilbaoImporter
{
    private BilbaoTrainingGroupImporter $bilbaoTrainingGroupImporter;
    private BilbaoTrainingTypeImporter $bilbaoTrainingTypeImporter;
    private BilbaoTrainingImporter $bilbaoTrainingImporter;
    private IliasContactProvider $iliasContactProvider;
    private BilbaoDataRepository $bilbaoDataRepository;
    private DummyTrainingTypeProvider $dummyTrainingTypeProvider;

    public function __construct(
        BilbaoTrainingGroupImporter $bilbaoTrainingGroupImporter,
        BilbaoTrainingTypeImporter  $bilbaoTrainingTypeImporter,
        BilbaoTrainingImporter      $bilbaoTrainingImporter,
        IliasContactProvider        $iliasContactProvider,
        BilbaoDataRepository        $bilbaoDataRepository,
        DummyTrainingTypeProvider   $dummyTrainingTypeProvider
    ) {
        $this->bilbaoTrainingGroupImporter = $bilbaoTrainingGroupImporter;
        $this->bilbaoTrainingTypeImporter = $bilbaoTrainingTypeImporter;
        $this->bilbaoTrainingImporter = $bilbaoTrainingImporter;
        $this->iliasContactProvider = $iliasContactProvider;
        $this->bilbaoDataRepository = $bilbaoDataRepository;
        $this->dummyTrainingTypeProvider = $dummyTrainingTypeProvider;
    }

    /**
     * @throws CsvColumnCountMismatch
     */
    public function import(BilbaoImportFilesDto $bilbaoImportFiles): void
    {
        /** @var DateTimeImmutable $exportedAt */
        $exportedAt = DateTimeImmutable::createFromFormat('Ymd_His', preg_replace('/.*_(\d{8}_\d{6}).*/', '$1', (string)$bilbaoImportFiles->trainingGroupFile));
        $employeeNumbers = [];

        $importedTrainingGroups = $this->bilbaoTrainingGroupImporter->import((string)$bilbaoImportFiles->trainingGroupFile);

        $dummyTrainingTypes = $this->dummyTrainingTypeProvider->provide();

        $skipTrainingTypesByShort = array_reduce($dummyTrainingTypes, function (array $carry, DummyTrainingTypeDto $dummyTrainingType): array {
            array_unshift($carry, ...$dummyTrainingType->replacesTrainingTypes);

            return $carry;
        }, []);

        $importedTrainingTypes = $this->bilbaoTrainingTypeImporter->import($bilbaoImportFiles->trainingTypeFiles);
        $importedTrainings = $this->bilbaoTrainingImporter->import($bilbaoImportFiles->trainingFiles);

        $trainingTypes = [];
        foreach ($dummyTrainingTypes as $dummyTrainingType) {
            $trainingTypes[$dummyTrainingType->id] = $dummyTrainingType->trainingType;
        }

        foreach ($importedTrainingTypes as $importedTrainingType) {
            if (in_array($importedTrainingType->short, $skipTrainingTypesByShort, true)) {
                continue;
            }

            $employeeNumbers = $this->collectEmployeeNumbers($importedTrainingType->contentResponsible, $employeeNumbers);
            $employeeNumbers = $this->collectEmployeeNumbers($importedTrainingType->eventResponsible, $employeeNumbers);
            $employeeNumbers = $this->collectEmployeeNumbers($importedTrainingType->speaker, $employeeNumbers);
            $trainingTypes[$importedTrainingType->id] = $importedTrainingType;
        }

        foreach ($importedTrainings as $training) {
            $trainingType = $trainingTypes[$training->trainingTypeId] ?? null;
            $trainingShort = substr($training->short, 0, 9);

            foreach ($dummyTrainingTypes as $dummyTrainingType) {
                if ((in_array($trainingShort, $dummyTrainingType->replacesTrainingTypes) && $dummyTrainingType->includeReplacedTrainingTypeTrainings) ||
                    in_array($training->id, $dummyTrainingType->containsTrainingIds)) {
                    $trainingType = $dummyTrainingType->trainingType;
                    break;
                }
            }

            if ($trainingType === null) {
                continue;
            }

            $employeeNumbers = $this->collectEmployeeNumbers($training->contentResponsible, $employeeNumbers);
            $employeeNumbers = $this->collectEmployeeNumbers($training->eventResponsible, $employeeNumbers);
            $employeeNumbers = $this->collectEmployeeNumbers($training->speaker, $employeeNumbers);
            $training->trainingForm = $trainingType->trainingForm;
            $trainingType->trainings[] = $training;
        }

        $iliasContacts = $this->iliasContactProvider->provide(array_unique($employeeNumbers));
        $iliasContactsByEmployeeNumber = array_reduce($iliasContacts, function ($carry, IliasContactDto $iliasContact) {
            $carry[$iliasContact->employeeNumber] = $iliasContact;

            return $carry;
        }, []);

        foreach ($trainingTypes as $trainingType) {
            $trainingType->contentResponsible = $this->mapIliasContact($trainingType->contentResponsible, $iliasContactsByEmployeeNumber);
            $trainingType->eventResponsible = $this->mapIliasContact($trainingType->eventResponsible, $iliasContactsByEmployeeNumber);
            $trainingType->speaker = $this->mapIliasContact($trainingType->speaker, $iliasContactsByEmployeeNumber);

            foreach ($trainingType->trainings as $training) {
                $training->contentResponsible = $this->mapIliasContact($training->contentResponsible, $iliasContactsByEmployeeNumber);
                $training->eventResponsible = $this->mapIliasContact($training->eventResponsible, $iliasContactsByEmployeeNumber);
                $training->speaker = $this->mapIliasContact($training->speaker, $iliasContactsByEmployeeNumber);
            }
        }

        usort($trainingTypes, fn(TrainingTypeDto $a, TrainingTypeDto $b) => $a->id <=> $b->id);

        $bilbaoData = new BilbaoDataDto();
        $bilbaoData->exportedAt = $exportedAt;
        $bilbaoData->trainingGroups = $importedTrainingGroups;
        $bilbaoData->trainingTypes = $trainingTypes;
        $bilbaoData->iliasContacts = $iliasContacts;

        $this->bilbaoDataRepository->save($bilbaoData);
    }

    /**
     * @param array<ContactDto> $contacts
     * @return array<string>
     */
    private function collectEmployeeNumbers(array $contacts, array $employeeNumbers = []): array
    {
        foreach ($contacts as $contact) {
            if ($contact->employeeNumber) {
                $employeeNumbers[] = $contact->employeeNumber;
            }
        }

        return $employeeNumbers;
    }

    /**
     * @param array<string, ContactDto> $contacts
     * @param array<string, IliasContactDto> $iliasContactsByEmployeeNumber
     * @return array<string, ContactDto>
     */
    private function mapIliasContact(array $contacts, array $iliasContactsByEmployeeNumber): array
    {
        foreach ($contacts as $contact) {
            if (isset($contact->iliasReference)) {// || !isset($contact->employeeNumber)) {
                continue;
            }

            $employeeNumber = str_pad(ltrim($contact->employeeNumber, '0'), 6, '0', STR_PAD_LEFT);
            $iliasContact = $iliasContactsByEmployeeNumber[$employeeNumber] ?? null;

            if ($iliasContact === null) {
                continue;
            }

            $contact->iliasReference = $iliasContact->iliasContactReference;
        }

        return $contacts;
    }
}
