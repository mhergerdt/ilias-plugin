<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Synchronization;

use DRVBund\Plugins\CGAutomation\BilbaoImport\ImportId\CategoryImportId;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Repository\BilbaoDataRepository;
use DRVBund\Plugins\CGAutomation\Ilias\Adapter\FilesystemAdapter;
use DRVBund\Plugins\CGAutomation\Ilias\PluginSettings;
use DRVBund\Plugins\CGAutomation\Shared\Dto\BilbaoDataDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingGroupDto;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingBookingCode;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingForm;

class BilbaoSynchronizationOrchestrator
{
    private BilbaoDataRepository $bilbaoDataRepository;
    private CategorySynchronizer $categorySynchronizer;
    private CourseSynchronizer $courseSynchronizer;
    private GroupSynchronizer $groupSynchronizer;
    /**
     * @var array
     * @psalm-var array{groupInfo: array<int, array{groupId: int, allowedEmailHashes: string[]}>, bookingCodeMapping: array<string, int>}
     */
    private array $bookingCodeMapping = [
        'groupInfo' => [],
        'bookingCodeMapping' => [],
    ];
    private array $categoryRefIds = [];
    private PluginSettings $pluginSettings;
    private FilesystemAdapter $filesystem;

    public function __construct(
        BilbaoDataRepository $bilbaoDataRepository,
        CategorySynchronizer $categorySynchronizer,
        CourseSynchronizer   $courseSynchronizer,
        GroupSynchronizer    $groupSynchronizer,
        PluginSettings       $pluginSettings,
        FilesystemAdapter    $filesystem
    )
    {
        $this->bilbaoDataRepository = $bilbaoDataRepository;
        $this->categorySynchronizer = $categorySynchronizer;
        $this->courseSynchronizer = $courseSynchronizer;
        $this->groupSynchronizer = $groupSynchronizer;
        $this->pluginSettings = $pluginSettings;
        $this->filesystem = $filesystem;
    }

    public function sync(int $containerRefId): void
    {
        $bilbaoData = $this->bilbaoDataRepository->load();

        $this->syncCategories($bilbaoData, $containerRefId);
        $this->syncCourses($bilbaoData, $containerRefId);

        $this->saveBookingCodeMapping();
    }

    public function syncCategories(BilbaoDataDto $bilbaoData, int $containerRefId): void
    {
        $trainingGroups = [];
        foreach ($bilbaoData->trainingGroups as $trainingGroup) {
            $trainingGroups[$trainingGroup->id] = $trainingGroup;
        }

        $rootTrainingGroups = [];
        foreach ($trainingGroups as $trainingGroup) {
            if (key_exists($trainingGroup->parent, $trainingGroups)) {
                $trainingGroups[$trainingGroup->parent]->children[] = $trainingGroup;
                continue;
            }
            $rootTrainingGroups[] = $trainingGroup;
        }

        $this->syncCategoriesRecursive($containerRefId, $rootTrainingGroups);
    }

    /**
     * @param int $containerRefId
     * @param TrainingGroupDto[] $trainingGroups
     * @return void
     */
    private function syncCategoriesRecursive(int $containerRefId, array $trainingGroups): void
    {
        foreach ($trainingGroups as $trainingGroup) {
            $category = $this->categorySynchronizer->sync($containerRefId, $trainingGroup);
            $this->categoryRefIds[$category->getImportId()] = $category->getRefId();

            if (count($trainingGroup->children) > 0) {
                $this->syncCategoriesRecursive($category->getRefId(), $trainingGroup->children);
            }
        }
    }

    private function syncCourses(BilbaoDataDto $bilbaoData, int $containerRefId): void
    {
        foreach ($bilbaoData->trainingTypes as $trainingType) {
            $courseRefId = $this->courseSynchronizer->sync(
                $this->resolveCategoryRefId(new CategoryImportId($trainingType->trainingGroupId), $containerRefId),
                $trainingType
            );

            $this->syncGroups($trainingType->trainings, $courseRefId);
        }
    }

    private function resolveCategoryRefId(CategoryImportId $importId, int $containerRefId): int
    {
        if (!array_key_exists((string)$importId, $this->categoryRefIds)) {
            return $containerRefId;
        }

        return $this->categoryRefIds[(string)$importId];
    }

    /**
     * @param TrainingDto[] $trainings
     */
    private function syncGroups(array $trainings, int $containerRefId): void
    {
        foreach ($trainings as $training) {
            $groupId = $this->groupSynchronizer->sync($containerRefId, $training);

            if ('' === $training->iliasBookingCode) {
                continue;
            }

            $this->addBookingCodeMapping(
                new TrainingBookingCode($training->iliasBookingCode),
                $groupId,
                $training->allowedParticipantMailHashes,
            );
        }
    }

    /**
     * @param string[] $allowedEmailHashes
     */
    private function addBookingCodeMapping(TrainingBookingCode $trainingBookingCode, int $groupId, array $allowedEmailHashes): void
    {
        $this->bookingCodeMapping['groupInfo'][$groupId] = [
            'groupId' => $groupId,
            'allowedEmailHashes' => $allowedEmailHashes,
        ];
        $this->bookingCodeMapping['bookingCodeMapping'][(string)$trainingBookingCode] = $groupId;
    }

    private function saveBookingCodeMapping(): void
    {
        $this->filesystem->put(
            (string)$this->pluginSettings->getBookingCodeMappingFilePath(),
            json_encode($this->bookingCodeMapping)
        );
    }
}
