<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Synchronization;

use DateTimeInterface;
use DRVBund\Plugins\CGAutomation\BilbaoImport\ImportId\GroupImportId;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Provider\LocationProvider;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Group\CreateGroup;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Group\CreateGroupFromGroup;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Group\UpdateGroup;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Metadata\SetCustomMetadata;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\RoleTemplate\ApplyRoleTemplateToLocalRole;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem\RelocateTreeItemIfParentChanged;
use DRVBund\Plugins\CGAutomation\Ilias\CustomMetadataCollection;
use DRVBund\Plugins\CGAutomation\Ilias\Dispatcher;
use DRVBund\Plugins\CGAutomation\Ilias\DTO\GroupDTO;
use DRVBund\Plugins\CGAutomation\Ilias\Facade\ParticipationFacade;
use DRVBund\Plugins\CGAutomation\Ilias\PluginSettings;
use DRVBund\Plugins\CGAutomation\Ilias\Repository\ObjectRepository;
use DRVBund\Plugins\CGAutomation\Ilias\Service\TreeService;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\GroupParticipantRole;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;
use DRVBund\Plugins\CGAutomation\Shared\Dto\ContactDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\LocationDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\ObjectLinkDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingDto;
use ilObjGroup;

class GroupSynchronizer
{
    private ObjectRepository $objectRepository;
    private PluginSettings $pluginSettings;
    private LocationProvider $locationProvider;
    private TreeService $treeService;
    private ParticipationFacade $participationFacade;
    private Dispatcher $dispatcher;
    private ?int $defaultGroupTemplateRefId;
    /**
     * @var int[]
     */
    private array $groupTemplateRefIdByTrainingForm;

    public function __construct(
        ObjectRepository    $objectRepository,
        PluginSettings      $pluginSettings,
        LocationProvider    $locationProvider,
        TreeService         $treeService,
        ParticipationFacade $participationFacade,
        Dispatcher          $dispatcher
    )
    {
        $this->objectRepository = $objectRepository;
        $this->pluginSettings = $pluginSettings;
        $this->locationProvider = $locationProvider;
        $this->treeService = $treeService;
        $this->participationFacade = $participationFacade;
        $this->dispatcher = $dispatcher;

        $this->defaultGroupTemplateRefId = $this->pluginSettings->refIdDefaultGroupTemplate()->getValueAsIntOrNull();
        $this->groupTemplateRefIdByTrainingForm = [];

        foreach ($this->pluginSettings->groupTemplateRefIds() as $trainingForm => $item) {
            $value = $item->getValueAsIntOrNull();

            if (null === $value) {
                continue;
            }

            $this->groupTemplateRefIdByTrainingForm[$trainingForm] = $value;
        }
    }

    public function sync(int $containerRefId, TrainingDto $training): int
    {
        $importId = new GroupImportId($training->id);
        $title = $this->buildTitle($training->short, $training->validFrom, $training->validUntil);

        $groupDTO = (new GroupDTO($title, (string)$importId))
            ->setPeriodFrom($training->validFrom)
            ->setPeriodUntil($training->validUntil)
            ->setOfflineStatus(false)
            ->setUseAutoNotification(false)
            ->setUseShowMembers(false)
            ->mailToMembersTypeAdmin()
            ->setUseNewsTimeline(false)
            ->setNewsBlockActivated(false)
            ->setUseRegistrationAccessCode(true)
            ->registrationTypeDeactivated()
            ->groupTypeClosed()
            ->setUseInfoTabVisibility(false)
            ->setUseNews(false)
            ->setWithCustomMetadata(true)
            ->setWithAutoRatingNewObjects(false)
            ->setUseTagCloud(false)
            ->setUseNewsVisibility(false)
            ->setUseBadges(false)
            ->setUseCalendarActivation(false)
            ->setUseCalendarVisibility(false);

        $groupId = $this->getGroupId($importId);
        $templateGroupRefId = $this->groupTemplateRefIdByTrainingForm[(string)$training->trainingForm] ?? $this->defaultGroupTemplateRefId;

        if (null === $groupId) {
            /** @var int $groupId */
            $groupId = $this->dispatcher->dispatchCommandWithResult(
                (null !== $templateGroupRefId ?
                    new CreateGroupFromGroup(
                        $containerRefId,
                        $groupDTO,
                        $templateGroupRefId,
                        [
                            ObjectType::CATEGORY(),
                            ObjectType::COURSE(),
                            ObjectType::GROUP(),
                        ]
                    ) :
                    new CreateGroup($containerRefId, $groupDTO)
                ));
            $groupRefId = $this->treeService->resolveObjRefIdUnderParentTreeItem($groupId, $containerRefId);
        } else {
            $groupRefId = $this->treeService->resolveObjRefIdUnderParentTreeItem($groupId, $containerRefId);
            $this->dispatcher->dispatchCommandWithResult(new UpdateGroup($groupRefId, $groupDTO));
        }

        $this->grantAccess($groupId, $training);

        $this->dispatcher->dispatchCommand(new RelocateTreeItemIfParentChanged($containerRefId, $groupRefId));

        $this->syncRolePermissions($groupRefId);
        $this->syncMetadata($groupRefId, $training);

        return $groupId;
    }

    private function buildTitle(string $short, DateTimeInterface $from, DateTimeInterface $until): string
    {
        $dateString = $from->format('d.m.Y');

        if ($from !== $until) {
            $dateString .= ' - ' . $until->format('d.m.Y');
        }

        return sprintf('[%s] %s', $short, $dateString);
    }

    private function getGroupId(GroupImportId $importId): ?int
    {
        /** @var ilObjGroup|null $group */
        $group = $this->objectRepository->loadByImportId((string)$importId, (string)ObjectType::GROUP());
        return null === $group ? null : $group->getId();
    }

    private function grantAccess(int $groupId, TrainingDto $training): void
    {
        $this->grantAccessForContacts($groupId, $training->speaker, GroupParticipantRole::Admin());
        $this->grantAccessForParticipants($groupId, $training->allowedParticipantMailHashes);
    }

    /**
     * @param ContactDto[] $contacts
     */
    private function grantAccessForContacts(int $groupId, array $contacts, GroupParticipantRole $participantRole): void
    {
        foreach ($contacts as $contact) {
            if ($contact->email === null) {
                continue;
            }

            $this->participationFacade->addGroupParticipantByEmail($groupId, $contact->email, $participantRole);
        }
    }

    /**
     * @param string[] $participantEmailHashes
     */
    private function grantAccessForParticipants(int $groupId, array $participantEmailHashes): void
    {
        foreach ($participantEmailHashes as $emailHash) {
            $this->participationFacade->addGroupParticipantByEmailHash($groupId, $emailHash, GroupParticipantRole::Member());
        }
    }

    private function syncRolePermissions(int $groupRefId): void
    {
        $this->dispatcher->dispatchCommand(new ApplyRoleTemplateToLocalRole(
            $this->pluginSettings->groupAdminRoleTemplateId()->getValueAsInt(),
            $groupRefId,
            ObjectType::GROUP(),
            GroupParticipantRole::Admin()->getParticipantRole()
        ));
        $this->dispatcher->dispatchCommand(new ApplyRoleTemplateToLocalRole(
            $this->pluginSettings->groupMemberRoleTemplateId()->getValueAsInt(),
            $groupRefId,
            ObjectType::GROUP(),
            GroupParticipantRole::Member()->getParticipantRole()
        ));
    }

    private function syncMetadata(int $groupRefId, TrainingDto $training): void
    {
        $locations = [$training->location];

        $locationRoomsMap = [];
        foreach ($training->rooms as $room) {
            $location = $this->resolveLocation($locations, $room);

            if (!array_key_exists($location->id, $locationRoomsMap)) {
                $locationRoomsMap[$location->id] = [];
            }

            $locationRoomsMap[$location->id][$room->short] = $room->short;
        }

        $locationsData = [];
        foreach ($locations as $location) {
            $locationsData[] = [
                'title' => $location->title,
                'streetLine1' => $location->streetLine1,
                'streetNumber' => $location->streetNumber,
                'streetLine2' => $location->streetLine2,
                'zipCode' => $location->zipCode,
                'city' => $location->city,
                'rooms' => array_key_exists($location->id, $locationRoomsMap) ? array_values($locationRoomsMap[$location->id]) : [],
            ];
        }

        $customMetadataCollection = (new CustomMetadataCollection())
            ->addStringItem('BILBAO_LOCATION', json_encode($locationsData));

        $this->dispatcher->dispatchCommand(new SetCustomMetadata(
            $groupRefId,
            'BILBAO_METADATA',
            $customMetadataCollection
        ));
    }

    /**
     * @param array<int, LocationDto> $locations
     */
    private function resolveLocation(array $locations, ObjectLinkDto $room): LocationDto
    {
        $roomPrefix = trim(mb_substr($room->short, 0, 2));

        if ($roomPrefix === 'VL') {
            return $this->locationProvider->provideBigBlueButtonLocation();
        }

        foreach ($locations as $location) {
            $locationPrefixList = $this->locationProvider->provideLocationPrefix($location);

            if (in_array($roomPrefix, $locationPrefixList, true)) {
                return $location;
            }
        }

        if (count($locations) === 1 && array_key_exists(0, $locations)) {
            return $locations[0];
        }

        return $this->locationProvider->provideUnknownLocation();
    }
}
