<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Synchronization;

use DRVBund\Plugins\CGAutomation\BilbaoImport\ImportId\CategoryImportId;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Category\CreateCategory;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Category\UpdateCategory;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem\RelocateTreeItemIfParentChanged;
use DRVBund\Plugins\CGAutomation\Ilias\Dispatcher;
use DRVBund\Plugins\CGAutomation\Ilias\DTO\CategoryDTO;
use DRVBund\Plugins\CGAutomation\Ilias\Repository\ObjectRepository;
use DRVBund\Plugins\CGAutomation\Ilias\Service\TreeService;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingGroupDto;
use ilObjCategory;

class CategorySynchronizer
{
    private ObjectRepository $objectRepository;
    private TreeService $treeService;
    private Dispatcher $dispatcher;

    public function __construct(
        ObjectRepository $objectRepository,
        TreeService      $treeService,
        Dispatcher       $dispatcher
    )
    {
        $this->objectRepository = $objectRepository;
        $this->treeService = $treeService;
        $this->dispatcher = $dispatcher;
    }

    public function sync(int $containerRefId, TrainingGroupDto $trainingGroup): ilObjCategory
    {
        $importId = CategoryImportId::asString($trainingGroup->id);
        $title = sprintf('[%s] %s', $trainingGroup->short, $trainingGroup->title);

        $categoryId = $this->getCategoryId($importId);

        $categoryDTO = (new CategoryDTO($title, $importId));

        /** @var int $categoryId */
        $categoryId = $this->dispatcher->dispatchCommandWithResult(
            null === $categoryId ? new CreateCategory($containerRefId, $categoryDTO) : new UpdateCategory($categoryId, $categoryDTO)
        );

        $categoryRefId = $this->treeService->resolveObjRefIdUnderParentTreeItem($categoryId, $containerRefId);

        $this->dispatcher->dispatchCommand(new RelocateTreeItemIfParentChanged($containerRefId, $categoryRefId));

        /** @var ilObjCategory $object */
        $object = $this->objectRepository->loadByRefId($categoryRefId);
        return $object;
    }

    private function getCategoryId(string $importId): ?int
    {
        /** @var ilObjCategory|null $category */
        $category = $this->objectRepository->loadByImportId($importId, (string)ObjectType::CATEGORY());
        return null === $category ? null : $category->getId();
    }
}
