<?php

namespace DRVBund\Plugins\CGAutomation\BilbaoImport\Synchronization;

use DRVBund\Plugins\CGAutomation\BilbaoImport\ImportId\CourseImportId;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Course\CreateCourse;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Course\CreateCourseFromCourse;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Course\UpdateCourse;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\RoleTemplate\ApplyRoleTemplateToLocalRole;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem\RelocateTreeItemIfParentChanged;
use DRVBund\Plugins\CGAutomation\Ilias\Dispatcher;
use DRVBund\Plugins\CGAutomation\Ilias\DTO\CourseDTO;
use DRVBund\Plugins\CGAutomation\Ilias\Facade\ParticipationFacade;
use DRVBund\Plugins\CGAutomation\Ilias\PluginSettings;
use DRVBund\Plugins\CGAutomation\Ilias\Repository\ObjectRepository;
use DRVBund\Plugins\CGAutomation\Ilias\Service\TreeService;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\CourseParticipantRole;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;
use DRVBund\Plugins\CGAutomation\Shared\Dto\ContactDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingForm;
use ilObjCourse;

class CourseSynchronizer
{
    private ObjectRepository $objectRepository;
    private PluginSettings $pluginSettings;
    private TreeService $treeService;
    private ParticipationFacade $participationFacade;
    private Dispatcher $dispatcher;
    private ?int $defaultCourseTemplateRefId;
    /**
     * @var array<array-key, int> $courseTemplateRefIdByTrainingForm
     */
    private array $courseTemplateRefIdByTrainingForm;

    public function __construct(
        ObjectRepository    $objectRepository,
        PluginSettings      $pluginSettings,
        TreeService         $treeService,
        ParticipationFacade $participationFacade,
        Dispatcher          $dispatcher
    )
    {
        $this->objectRepository = $objectRepository;
        $this->pluginSettings = $pluginSettings;
        $this->treeService = $treeService;
        $this->participationFacade = $participationFacade;
        $this->dispatcher = $dispatcher;

        $this->defaultCourseTemplateRefId = $this->pluginSettings->refIdDefaultCourseTemplate()->getValueAsIntOrNull();
        $this->courseTemplateRefIdByTrainingForm = [];

        foreach ($this->pluginSettings->courseTemplateRefIds() as $trainingForm => $item) {
            $value = $item->getValueAsIntOrNull();

            if (null === $value) {
                continue;
            }

            $this->courseTemplateRefIdByTrainingForm[$trainingForm] = $value;
        }
    }

    public function sync(int $containerRefId, TrainingTypeDto $trainingType): int
    {
        $importId = new CourseImportId($trainingType->id);
        $title = sprintf('[%s] %s', $trainingType->short, $trainingType->title);

        $courseDTO = (new CourseDTO($title, (string)$importId))
            ->setDescription(join(' ', $trainingType->verbalDescriptions->goals))
            ->setUseShowMembers(false)
            ->setOfflineStatus(false)
            ->setUseAutoNotification(false)
            ->setUseRegistrationAccessCode(true)
            ->subscriptionDeactivated()
            ->timingModeAbsolute()
            ->mailToMembersTypeAllowedTutors()
            ->setPeriodFrom($trainingType->validFrom)
            ->setPeriodUntil($trainingType->validUntil)
            ->setUseInfoTabVisibility(false)
            ->setUseNews(false)
            ->setWithCustomMetadata(false)
            ->setWithAutoRatingNewObjects(false)
            ->setUseTagCloud(false)
            ->setUseNewsVisibility(false)
            ->setUseBadges(false)
            ->setUseBooking(false)
            ->viewModeViewByType()
            ->setUseCalendarActivation(false)
            ->setUseCalendarVisibility(false);

        $courseId = $this->getCourseId($importId);
        $templateCourseRefId = $this->courseTemplateRefIdByTrainingForm[(string)$trainingType->trainingForm] ?? $this->defaultCourseTemplateRefId;

        /** @var int $courseId */
        $courseId = $this->dispatcher->dispatchCommandWithResult(
            null !== $courseId ?
                new UpdateCourse($courseId, $courseDTO) :
                (null !== $templateCourseRefId ?
                    new CreateCourseFromCourse(
                        $containerRefId,
                        $courseDTO,
                        $templateCourseRefId,
                        [
                            ObjectType::CATEGORY(),
                            ObjectType::COURSE(),
                            ObjectType::GROUP(),
                        ]
                    ) :
                    new CreateCourse($containerRefId, $courseDTO)
                )
        );

        $this->grantAccess($courseId, $trainingType);

        $courseRefId = $this->treeService->resolveObjRefIdUnderParentTreeItem($courseId, $containerRefId);

        $this->dispatcher->dispatchCommand(new RelocateTreeItemIfParentChanged($containerRefId, $courseRefId));

        $this->syncRolePermissions($courseRefId);

        return $courseRefId;
    }

    private function getCourseId(CourseImportId $importId): ?int
    {
        /** @var ilObjCourse|null $course */
        $course = $this->objectRepository->loadByImportId((string)$importId, (string)ObjectType::COURSE());
        return null === $course ? null : $course->getId();
    }

    private function grantAccess(int $courseId, TrainingTypeDto $trainingType): void
    {
        $this->grantAccessForContacts($courseId, $trainingType->contentResponsible, CourseParticipantRole::Admin());
        $this->grantAccessForContacts($courseId, $trainingType->speaker, CourseParticipantRole::Tutor());

        foreach ($trainingType->trainings as $training) {
            $this->grantAccessForContacts($courseId, $training->contentResponsible, CourseParticipantRole::Admin());
            $this->grantAccessForContacts($courseId, $training->speaker, CourseParticipantRole::Tutor());
            $this->grantAccessForParticipants($courseId, $training->allowedParticipantMailHashes);
        }
    }

    /**
     * @param ContactDto[] $contacts
     */
    private function grantAccessForContacts(int $courseId, array $contacts, CourseParticipantRole $participantRole): void
    {
        foreach ($contacts as $contact) {
            if ($contact->email === null) {
                continue;
            }

            $this->participationFacade->addCourseParticipantByEmail($courseId, $contact->email, $participantRole);
        }
    }

    /**
     * @param string[] $participantEmailHashes
     */
    private function grantAccessForParticipants(int $courseId, array $participantEmailHashes): void
    {
        foreach ($participantEmailHashes as $emailHash) {
            $this->participationFacade->addCourseParticipantByEmailHash($courseId, $emailHash, CourseParticipantRole::Member());
        }
    }

    private function syncRolePermissions(int $courseRefId): void
    {
        $this->dispatcher->dispatchCommand(new ApplyRoleTemplateToLocalRole(
            $this->pluginSettings->courseTutorRoleTemplateId()->getValueAsInt(),
            $courseRefId,
            ObjectType::COURSE(),
            CourseParticipantRole::Tutor()->getParticipantRole()
        ));
        $this->dispatcher->dispatchCommand(new ApplyRoleTemplateToLocalRole(
            $this->pluginSettings->courseMemberRoleTemplateId()->getValueAsInt(),
            $courseRefId,
            ObjectType::COURSE(),
            CourseParticipantRole::Member()->getParticipantRole()
        ));
    }
}
