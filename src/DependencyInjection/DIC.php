<?php

namespace DRVBund\Plugins\CGAutomation\DependencyInjection;

use DI\Container;
use DI\ContainerBuilder;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Import\BilbaoImporter;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Import\BilbaoImportOrchestrator;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Repository\BilbaoDataRepository;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Repository\JsonBilbaoDataRepository;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Repository\LegacyJsonFileBilbaoDataRepository;
use DRVBund\Plugins\CGAutomation\Development\GetUsersByEmailInMemoryHandler;
use DRVBund\Plugins\CGAutomation\Development\GetUsersByEmailShaHashInMemoryHandler;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Categories\CategorizationResolver;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Categories\SimpleCategorizationResolver;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Filter\EducationCatalogueItemFilterFactory;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Generate\DefaultEducationCatalogueGenerator;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Generate\EducationCatalogueGenerator;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Generate\JsonEducationCatalogueGeneratorStrategy;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Mapper\ProtectedEducationCatalogueMapper;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Mapper\PublicEducationCatalogueMapper;
use DRVBund\Plugins\CGAutomation\Ilias\Adapter\FilesystemAdapter;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandWithResultHandler;
use DRVBund\Plugins\CGAutomation\Ilias\DevModeTrait;
use DRVBund\Plugins\CGAutomation\Ilias\Dispatcher;
use DRVBund\Plugins\CGAutomation\Ilias\InMemoryDispatcher;
use DRVBund\Plugins\CGAutomation\Ilias\PluginSettings;
use DRVBund\Plugins\CGAutomation\Ilias\Query\Handler\QueryHandler;
use DRVBund\Plugins\CGAutomation\Shared\PluginConstants;
use ilCtrlInterface;
use ilDBInterface;
use ILIAS\DI\UIServices;
use ILIAS\HTTP\Services;
use ILIAS\Refinery\Factory;
use ilObjectDataCache;
use ilObjectDefinition;
use ilObjUser;
use ilPluginLanguage;
use ilRbacAdmin;
use ilRbacSystem;
use ilSetting;
use ilTabsGUI;
use ilTree;
use function DI\autowire;

/**
 * @property mixed $composerAutoload
 * @psalm-api
 */
class DIC
{
    use DevModeTrait;

    private Container $container;
    /**
     * @var array<string, object[]>
     */
    private static $resolvedObjectsCache = [];


    public function __construct()
    {
        global $DIC;
        $this->composerAutoload = require join(DIRECTORY_SEPARATOR, [__DIR__, '..', '..', 'vendor/autoload.php']);

        $builder = new ContainerBuilder();
        $builder->addDefinitions([
            // region Ilias
            FilesystemAdapter::class => fn(): FilesystemAdapter => new FilesystemAdapter($DIC->filesystem()->web()),
            ilTabsGUI::class => fn(): ilTabsGUI => $DIC->tabs(),
            ilCtrlInterface::class => $DIC->ctrl(),
            UIServices::class => $DIC->ui(),
            Services::class => $DIC->http(),
            Factory::class => $DIC->refinery(),
            ilObjectDataCache::class => $DIC['ilObjDataCache'],
            ilObjectDefinition::class => $DIC['objDefinition'],
            ilObjUser::class => $DIC->user(),
            ilSetting::class => $DIC->settings(),
            ilPluginLanguage::class => new ilPluginLanguage($DIC['component.repository']->getPluginById(PluginConstants::PLUGIN_ID)),
            ilDBInterface::class => $DIC->database(),
            ilTree::class => $DIC->repositoryTree(),
            ilRbacAdmin::class => $DIC->rbac()->admin(),
            ilRbacSystem::class => $DIC->rbac()->system(),
            'object_logger' => $DIC->logger()->obj(),
            Dispatcher::class => fn(InMemoryDispatcher $dispatcher) => $dispatcher,
            // endregion
            // region Bilbao import
            JsonBilbaoDataRepository::class => function (PluginSettings $pluginSettings, FilesystemAdapter $filesystem) {
                $repository = new JsonBilbaoDataRepository($filesystem);
                $repository->setFilePath($pluginSettings->getDataSrcDirectory()->appendFileName('bilbao_v2.json'));
                return $repository;
            },
            LegacyJsonFileBilbaoDataRepository::class => function (
                PluginSettings           $pluginSettings,
                FilesystemAdapter        $filesystem,
                JsonBilbaoDataRepository $jsonBilbaoDataRepository
            ) {
                $repository = new LegacyJsonFileBilbaoDataRepository($jsonBilbaoDataRepository, $filesystem);
                $repository->setFilePath($pluginSettings->getDataSrcDirectory()->appendFileName('bilbao_legacy.json'));

                return $repository;
            },
            BilbaoDataRepository::class => fn(LegacyJsonFileBilbaoDataRepository $repository) => $repository,
            BilbaoImportOrchestrator::class => fn(Container $container) => new BilbaoImportOrchestrator(
                $container->get(FilesystemAdapter::class),
                $container->get(BilbaoImporter::class),
                $container->get(PluginSettings::class)->getDataSrcDirectory()->join('bilbao_tmp')
            ),
            // endregion
            // region Education catalogue
            CategorizationResolver::class => fn(SimpleCategorizationResolver $resolver) => $resolver,
            DefaultEducationCatalogueGenerator::class => function (Container $container) {
                $educationCatalogueFilterItemFactory = $container->get(EducationCatalogueItemFilterFactory::class);
                $pluginSettings = $container->get(PluginSettings::class);

                return new DefaultEducationCatalogueGenerator(
                    $container->get(BilbaoDataRepository::class),
                    [
                        new JsonEducationCatalogueGeneratorStrategy(
                            $educationCatalogueFilterItemFactory->createDrvBundFilter(),
                            $container->get(ProtectedEducationCatalogueMapper::class),
                            $container->get(FilesystemAdapter::class),
                            $pluginSettings->getCatalogueDataBundDirectory()->appendFileName('education_catalogue_complete.json')
                        ),
                        new JsonEducationCatalogueGeneratorStrategy(
                            $educationCatalogueFilterItemFactory->createDefaultFilter(),
                            $container->get(ProtectedEducationCatalogueMapper::class),
                            $container->get(FilesystemAdapter::class),
                            $pluginSettings->getCatalogueDataTraegerDirectory()->appendFileName('education_catalogue_traeger.json')
                        ),
                        new JsonEducationCatalogueGeneratorStrategy(
                            $educationCatalogueFilterItemFactory->createDefaultFilter(),
                            $container->get(PublicEducationCatalogueMapper::class),
                            $container->get(FilesystemAdapter::class),
                            $pluginSettings->getCatalogueStageDirectory()->appendFileName('education_catalogue_public.json')
                        ),
                        new JsonEducationCatalogueGeneratorStrategy(
                            $educationCatalogueFilterItemFactory->createDefaultFilter(),
                            $container->get(PublicEducationCatalogueMapper::class),
                            $container->get(FilesystemAdapter::class),
                            $pluginSettings->getCatalogueProdDirectory()->appendFileName('education_catalogue_public.json')
                        ),
                    ],
                    $container->get(CategorizationResolver::class)
                );
            },
            EducationCatalogueGenerator::class => fn(DefaultEducationCatalogueGenerator $generator) => $generator,
            // endregion
        ]);
        if (!$this->isDevModeActive()) {
            $builder->enableCompilation(__DIR__ . '/tmp');
        }

        $this->container = $builder->build();

        $container = $this->container;
        $dispatcher = $this->container->get(InMemoryDispatcher::class);

        foreach ($this->getByInterface($container, 'DRVBund\\Plugins\\CGAutomation\\Ilias\\Commands', CommandHandler::class) as $commandHandler) {
            $dispatcher->addCommandHandler($commandHandler);
        }
        foreach ($this->getByInterface($container, 'DRVBund\\Plugins\\CGAutomation\\Ilias\\Commands', CommandWithResultHandler::class) as $commandHandler) {
            $dispatcher->addCommandWithResultHandler($commandHandler);
        }
        foreach($this->getByInterface($container, 'DRVBund\\Plugins\\CGAutomation\\Ilias\\Query', QueryHandler::class) as $queryHandler) {
            $dispatcher->addQueryHandler($queryHandler);
        }

        if ($this->isDevModeActive()) {
            $dispatcher->addQueryHandler($container->get(GetUsersByEmailInMemoryHandler::class));
            $dispatcher->addQueryHandler($container->get(GetUsersByEmailShaHashInMemoryHandler::class));
        }
    }

    /**
     * @template T
     * @param string|class-string<T> $name Entry name or a class name.
     * @return mixed|T
     */
    public function get(string $name)
    {
        return $this->container->get($name);
    }

    /**
     * @template T as object
     * @param class-string<T> $interfaceClassName
     * @return T[]
     */
    private function getByInterface(Container $container, string $namespace, string $interfaceClassName): array
    {
        $cacheKey = $namespace . $interfaceClassName;
        if (array_key_exists($cacheKey, self::$resolvedObjectsCache)) {
            return self::$resolvedObjectsCache[$cacheKey];
        }

        $objects = [];

        $namespaceMapping = $this->resolveNamespaceMapping($namespace);
        foreach ($namespaceMapping['paths'] as $path) {
            $namespaceDirectory = join(DIRECTORY_SEPARATOR, [
                $path,
                str_replace('\\', DIRECTORY_SEPARATOR, str_replace($namespaceMapping['prefix'], '', $namespace))
            ]);

            $files = scandir($namespaceDirectory);
            foreach ($files as $file) {
                if (in_array($file, ['.', '..'])) {
                    continue;
                }

                if (is_dir($namespaceDirectory . DIRECTORY_SEPARATOR . $file)) {
                    foreach ($this->getByInterface($container, $namespace . '\\' . $file, $interfaceClassName) as $object) {
                        $objects[] = $object;
                    }
                    continue;
                }

                $className = $namespace . '\\' . str_replace('.php', '', $file);
                if (!class_exists($className)) {
                    continue;
                }

                $reflectionClass = new \ReflectionClass($className);
                if (!$reflectionClass->implementsInterface($interfaceClassName) || $reflectionClass->isAbstract()) {
                    continue;
                }

                $container->set($className, autowire($className));
                try {
                    $objects[] = $container->get($className);
                } catch (\Throwable $e) {
                    $container->set($className, null);
                    continue;
                }
            }
        }

        self::$resolvedObjectsCache[$cacheKey] = $objects;
        return $objects;
    }

    /**
     * @return array{prefix: string, paths: string[]}
     */
    private function resolveNamespaceMapping(string $namespace): array
    {
        foreach ($this->composerAutoload->getPrefixesPsr4() as $prefix => $paths) {
            if (strpos($namespace, $prefix) === 0) {
                return [
                    'prefix' => $prefix,
                    'paths' => $paths,
                ];
            }
        }

        return [];
    }
}
