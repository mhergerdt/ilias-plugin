<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Generate;

use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\EducationCatalogueDataDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;

interface EducationCatalogueGeneratorStrategy
{
    public function includesTrainingType(TrainingTypeDto $trainingType): bool;
    public function save(EducationCatalogueDataDto $data): void;
}
