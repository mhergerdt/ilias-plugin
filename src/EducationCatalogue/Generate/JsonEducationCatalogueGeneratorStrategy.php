<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Generate;

use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\EducationCatalogueDataDto;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Filter\EducationCatalogueItemFilter;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Mapper\EducationCatalogueDataJsonMapper;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Repository\JsonEducationCatalogueRepository;
use DRVBund\Plugins\CGAutomation\Ilias\Adapter\FilesystemAdapter;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\FilePath;

class JsonEducationCatalogueGeneratorStrategy implements EducationCatalogueGeneratorStrategy
{
    private EducationCatalogueItemFilter $itemFilter;
    private JsonEducationCatalogueRepository $repository;

    public function __construct(
        EducationCatalogueItemFilter     $itemFilter,
        EducationCatalogueDataJsonMapper $mapper,
        FilesystemAdapter                $filesystem,
        FilePath                         $filePath
    )
    {
        $this->itemFilter = $itemFilter;
        $this->repository = new JsonEducationCatalogueRepository(
            $mapper,
            $filesystem,
            $filePath
        );
    }

    public function includesTrainingType(TrainingTypeDto $trainingType): bool
    {
        return $this->itemFilter->includes($trainingType);
    }

    public function save(EducationCatalogueDataDto $data): void
    {
        $this->repository->save($data);
    }
}
