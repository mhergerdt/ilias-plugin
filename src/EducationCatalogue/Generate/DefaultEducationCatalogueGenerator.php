<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Generate;

use DRVBund\Plugins\CGAutomation\BilbaoImport\Repository\BilbaoDataRepository;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Categories\CategorizationResolver;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\EducationCatalogueDataDto;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\EducationCatalogueItemDto;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Filter\EducationCatalogueItemFilter;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Repository\EducationCatalogueRepository;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingDto;

class DefaultEducationCatalogueGenerator implements EducationCatalogueGenerator
{
    private BilbaoDataRepository $bilbaoDataRepository;
    /**
     * @var EducationCatalogueGeneratorStrategy[]
     */
    private array $strategies;
    private CategorizationResolver $categorizationResolver;

    /**
     * @param EducationCatalogueGeneratorStrategy[] $strategies
     */
    public function __construct(
        BilbaoDataRepository   $bilbaoDataRepository,
        array                  $strategies,
        CategorizationResolver $categorizationResolver
    )
    {
        $this->bilbaoDataRepository = $bilbaoDataRepository;
        $this->strategies = $strategies;
        $this->categorizationResolver = $categorizationResolver;
    }

    public function generate(): void
    {
        $bilbaoData = $this->bilbaoDataRepository->load();

        /**
         * @var array<int, array{data: EducationCatalogueDataDto, strategy: EducationCatalogueGeneratorStrategy}> $dataByStrategy
         */
        $dataByStrategy = [];

        foreach ($this->strategies as $strategy) {
            $educationCatalogueData = new EducationCatalogueDataDto();
            $educationCatalogueData->exportedAt = $bilbaoData->exportedAt;
            $educationCatalogueData->categories = $this->categorizationResolver->getCategories();

            $dataByStrategy[] = [
                'data' => $educationCatalogueData,
                'strategy' => $strategy,
            ];
        }

        foreach ($bilbaoData->trainingTypes as $trainingType) {
            foreach ($dataByStrategy as $item) {
                if ($item['strategy']->includesTrainingType($trainingType)) {
                    $catalogueItem = new EducationCatalogueItemDto();
                    $catalogueItem->trainingType = $trainingType;
                    $catalogueItem->trainingType->trainings = $this->filterTrainings($trainingType->trainings);
                    $catalogueItem->categorization = $this->categorizationResolver->resolve($trainingType->short);

                    $item['data']->items[] = $catalogueItem;
                }
            }
        }

        foreach ($dataByStrategy as $item) {
            $item['strategy']->save($item['data']);
        }
    }

    /**
     * @param TrainingDto[] $trainings
     * @return TrainingDto[]
     */
    private function filterTrainings(array $trainings): array
    {
        $filtered = [];

        foreach ($trainings as $training) {
            if ($training->status === 'fixiert' || $training->status === 'geplant') {
                $filtered[] = $training;
            }
        }

        return $filtered;
    }
}
