<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Generate;

interface EducationCatalogueGenerator
{
    public function generate(): void;
}
