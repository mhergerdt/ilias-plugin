<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Filter;

use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;

class OrFilter implements EducationCatalogueItemFilter
{
    /**
     * @var EducationCatalogueItemFilter[]
     */
    private array $filters;

    public function __construct(EducationCatalogueItemFilter ...$filters)
    {
        $this->filters = $filters;
    }

    public function includes(TrainingTypeDto $trainingType): bool
    {
        foreach ($this->filters as $filter) {
            if ($filter->includes($trainingType)) {
                return true;
            }
        }

        return false;
    }
}
