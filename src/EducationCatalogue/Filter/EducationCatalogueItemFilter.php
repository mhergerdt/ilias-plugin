<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Filter;

use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;

interface EducationCatalogueItemFilter
{
    public function includes(TrainingTypeDto $trainingType): bool;
}
