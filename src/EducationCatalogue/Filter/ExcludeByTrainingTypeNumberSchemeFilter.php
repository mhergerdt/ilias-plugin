<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Filter;

use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingTypeNumberScheme;

class ExcludeByTrainingTypeNumberSchemeFilter implements EducationCatalogueItemFilter
{
    /**
     * @var TrainingTypeNumberScheme[]
     */
    private array $schemes;

    /**
     * @param TrainingTypeNumberScheme[] $schemes
     */
    public function __construct(array $schemes)
    {
        $this->schemes = $schemes;
    }

    public static function fromStrings(string ...$schemes): self
    {
        return new self(TrainingTypeNumberScheme::createMany(...$schemes));
    }

    public function includes(TrainingTypeDto $trainingType): bool
    {
        foreach ($this->schemes as $scheme) {
            if ($scheme->matches($trainingType->short)) {
                return false;
            }
        }

        return true;
    }
}
