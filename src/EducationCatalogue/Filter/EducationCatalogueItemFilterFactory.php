<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Filter;

use DRVBund\Plugins\CGAutomation\BilbaoImport\Provider\DummyTrainingTypeProvider;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingTypeNumberScheme;

class EducationCatalogueItemFilterFactory
{
    private DummyTrainingTypeProvider $dummyTrainingTypeProvider;

    public function __construct(DummyTrainingTypeProvider $dummyTrainingTypeProvider)
    {
        $this->dummyTrainingTypeProvider = $dummyTrainingTypeProvider;
    }

    public function createDrvBundFilter(): EducationCatalogueItemFilter
    {
        return new AndFilter(
            $this->getBasicFilter(),
            new OrFilter(
                $this->getDrvFilter(),
                IncludeByTrainingTypeNumberSchemeFilter::fromStrings('xxxx.2xxx')
            )
        );
    }

    public function createDefaultFilter(): EducationCatalogueItemFilter
    {
        return new AndFilter(
            $this->getBasicFilter(),
            $this->getDrvFilter()
        );
    }

    private function getBasicFilter(): EducationCatalogueItemFilter
    {
        /** @var TrainingTypeNumberScheme[] $excludedTrainingTypeNumberSchemes */
        $excludedTrainingTypeNumberSchemes = [
            /** OLD Agile Coach Ausbildung (5570.101X) */
            new TrainingTypeNumberScheme('5570.1010'),
            new TrainingTypeNumberScheme('5570.1011'),
            new TrainingTypeNumberScheme('5570.1012'),
            new TrainingTypeNumberScheme('5570.1013'),
            new TrainingTypeNumberScheme('5570.1014'),
            new TrainingTypeNumberScheme('5570.1015'),
            new TrainingTypeNumberScheme('5570.1016'),
            new TrainingTypeNumberScheme('5570.1017'),
            new TrainingTypeNumberScheme('5570.1018'),
            new TrainingTypeNumberScheme('5570.1019'),
            /* New Agile Coach Ausbildung EV (4131.101x) */
            new TrainingTypeNumberScheme('4131.1010'),
            new TrainingTypeNumberScheme('4131.1018'),
            new TrainingTypeNumberScheme('4131.1019'),
            /* Resilienz für Frauen in Führung-online (Fokus Teamsteuerung) (5030.1065) */
            new TrainingTypeNumberScheme('5030.1061'),
            new TrainingTypeNumberScheme('5030.1062'),
            new TrainingTypeNumberScheme('5030.1063'),
            new TrainingTypeNumberScheme('5030.1064'),
            /* Resilienz für Frauen in Führung-online (5030.1055) */
            new TrainingTypeNumberScheme('5030.1051'),
            new TrainingTypeNumberScheme('5030.1052'),
            new TrainingTypeNumberScheme('5030.1053'),
            new TrainingTypeNumberScheme('5030.1054'),
            /* Fallmanagement in der Rehabilitation (4522.1080) */
            new TrainingTypeNumberScheme('4522.1081'),
            new TrainingTypeNumberScheme('4522.1082'),
            new TrainingTypeNumberScheme('4522.1083'),
            new TrainingTypeNumberScheme('4522.1084'),
            new TrainingTypeNumberScheme('4522.1085'),
            new TrainingTypeNumberScheme('4522.1086'),
            new TrainingTypeNumberScheme('4522.1087'),
            new TrainingTypeNumberScheme('4522.1088'),
            new TrainingTypeNumberScheme('4522.1089'),
            /* Agile Coach Ausbildung (4130.1022) */
            new TrainingTypeNumberScheme('4130.1011'),
            new TrainingTypeNumberScheme('4130.1012'),
            new TrainingTypeNumberScheme('4130.1013'),
            new TrainingTypeNumberScheme('4130.1014'),
            new TrainingTypeNumberScheme('4130.1015'),
            new TrainingTypeNumberScheme('4130.1016'),
            new TrainingTypeNumberScheme('4131.1016'),
            new TrainingTypeNumberScheme('4130.1017'),
            new TrainingTypeNumberScheme('4131.1017'),
            new TrainingTypeNumberScheme('4130.1018'),
            /* Bildungsangebot des Multiprojekts rvEvolution (7500.1000) */
            new TrainingTypeNumberScheme('7510.1010'),
            new TrainingTypeNumberScheme('7510.1050'),
            new TrainingTypeNumberScheme('7510.1020'),
            new TrainingTypeNumberScheme('7540.1010'),
            new TrainingTypeNumberScheme('7520.1020'),
            new TrainingTypeNumberScheme('7520.1030'),
            new TrainingTypeNumberScheme('7520.1040'),
            new TrainingTypeNumberScheme('7550.1010'),
            new TrainingTypeNumberScheme('7550.1020'),
        ];
        foreach ($this->dummyTrainingTypeProvider->provide() as $dummyItem) {
            if ($dummyItem->includeReplacedTrainingTypeTrainings) {
                continue;
            }

            foreach ($dummyItem->replacesTrainingTypes as $trainingTypeNumber) {
                $excludedTrainingTypeNumberSchemes[] = new TrainingTypeNumberScheme($trainingTypeNumber);
            }
        }

        return new AndFilter(
            new RelevantForEducationCatalogueItemFilter(),
            new ExcludeByTrainingTypeNumberSchemeFilter($excludedTrainingTypeNumberSchemes),
        );
    }

    private function getDrvFilter(): EducationCatalogueItemFilter
    {
        return new AndFilter(
            new TargetGroupContainsItemFilter('trägerüb'),
            IncludeByTrainingTypeNumberSchemeFilter::fromStrings(
                'xxxx.1xxx',
                '96xx.xxxx',
            ),
            ExcludeByTrainingTypeNumberSchemeFilter::fromStrings(
                '56xx.xxxx',
                '93xx.xxxx',
            ),
        );
    }
}
