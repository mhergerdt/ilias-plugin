<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Filter;

use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;

class RelevantForEducationCatalogueItemFilter implements EducationCatalogueItemFilter
{
    public function includes(TrainingTypeDto $trainingType): bool
    {
        return $trainingType->showInBrochure;
    }
}
