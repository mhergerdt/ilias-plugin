<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Filter;

use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;

class TargetGroupContainsItemFilter implements EducationCatalogueItemFilter
{
    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function includes(TrainingTypeDto $trainingType): bool
    {
        return strpos($trainingType->targetGroup, $this->value) !== false;
    }
}
