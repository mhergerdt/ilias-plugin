<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Mapper;

use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\CategoryDto;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\EducationCatalogueItemDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\IliasContactDto;

class PublicEducationCatalogueMapper implements EducationCatalogueDataJsonMapper
{
    private ProtectedEducationCatalogueMapper $mapper;

    public function __construct(ProtectedEducationCatalogueMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    public function mapItem(EducationCatalogueItemDto $item): array
    {
        $trainingTypeDataArray = $this->mapper->mapItem($item);

        $data = [
            'id' => $trainingTypeDataArray['id'],
            'short' => $trainingTypeDataArray['short'],
            'title' => $trainingTypeDataArray['title'],
            'validFrom' => $trainingTypeDataArray['validFrom'],
            'validUntil' => $trainingTypeDataArray['validUntil'],
            'trainingForm' => $trainingTypeDataArray['trainingForm'],
            'linkToCourse' => $trainingTypeDataArray['linkToCourse'] ?? null,
            'imgUrl' => $trainingTypeDataArray['imgUrl'] ?? null,
            'verbalDescription' => $trainingTypeDataArray['verbalDescription'],
            'price' => [
                'vatExempt' => $trainingTypeDataArray['price']['vatExempt'],
                'external' => $trainingTypeDataArray['price']['external'],
            ],
            'capacity' => $trainingTypeDataArray['capacity'],
            'targetGroup' => $trainingTypeDataArray['targetGroup'],
            'accountingCategory' => $trainingTypeDataArray['accountingCategory'],
            'duration' => $trainingTypeDataArray['duration'],
            'belongsTo' => $trainingTypeDataArray['belongsTo'],
            'requires' => $trainingTypeDataArray['requires'],
            'intendedFor' => $trainingTypeDataArray['intendedFor'],
            'hostedBy' => $trainingTypeDataArray['hostedBy'],
            'mandatoryFor' => $trainingTypeDataArray['mandatoryFor'],
            'needs' => $trainingTypeDataArray['needs'],
            'mediates' => $trainingTypeDataArray['mediates'],
            'categorization' => $trainingTypeDataArray['categorization'],
            'publication' => $trainingTypeDataArray['publication'],
            'trainings' => array_map(function (array $trainingDataArray) {
                return [
                    'id' => $trainingDataArray['id'],
                    'short' => $trainingDataArray['short'],
                    'title' => $trainingDataArray['title'],
                    'validFrom' => $trainingDataArray['validFrom'],
                    'validUntil' => $trainingDataArray['validUntil'],
                    'location' => $trainingDataArray['location'],
                    'registrationDeadline' => $trainingDataArray['registrationDeadline'],
                    'capacity' => $trainingDataArray['capacity'],
                    'duration' => $trainingDataArray['duration'],
                    'price' => [
                        'external' => $trainingDataArray['price']['external'],
                    ],
                    'rooms' => $trainingDataArray['rooms'],
                    'schedule' => $trainingDataArray['schedule'],
                    'status' => $trainingDataArray['status'],
                    'trainingType' => $trainingDataArray['trainingType'],
                ];
            }, $trainingTypeDataArray['trainings']),
        ];

        if (empty($trainingTypeDataArray['linkToCourse'])) {
            unset($data['linkToCourse']);
        }

        if (empty($trainingTypeDataArray['imgUrl'])) {
            unset($data['imgUrl']);
        }

        return $data;
    }

    public function mapIliasContact(IliasContactDto $contactDto): array
    {
        return $this->mapper->mapIliasContact($contactDto);
    }

    public function mapCategory(CategoryDto $category): array
    {
        return $this->mapper->mapCategory($category);
    }
}
