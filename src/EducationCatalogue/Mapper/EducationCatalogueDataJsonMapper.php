<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Mapper;

use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\CategoryDto;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\EducationCatalogueItemDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\IliasContactDto;

interface EducationCatalogueDataJsonMapper
{
    public function mapItem(EducationCatalogueItemDto $item): array;
    public function mapIliasContact(IliasContactDto $contactDto): array;
    public function mapCategory(CategoryDto $category): array;
}
