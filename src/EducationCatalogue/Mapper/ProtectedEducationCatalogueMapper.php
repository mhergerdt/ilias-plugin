<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Mapper;

use DateTime;
use DateTimeInterface;
use DateTimeZone;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\CategoryDto;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\EducationCatalogueItemDto;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\ResolvedCategoryDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\ContactDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\IliasContactDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\ObjectLinkDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingScheduleEntryDto;

class ProtectedEducationCatalogueMapper implements EducationCatalogueDataJsonMapper
{
    public function mapItem(EducationCatalogueItemDto $item): array
    {
        $trainingType = $item->trainingType;
        $categorization = $item->categorization;

        $trainingTypeIdIsNumeric = !!preg_match('/^\d+$/', $trainingType->id);

        $categorizationData = [];
        $categorizationData['type'] = $categorization->type;
        $categorizationData['topic'] = [
            'label' => $categorization->topicLabel,
            'color' => $categorization->topicColor,
        ];
        if (isset($categorization->category)) {
            $categorizationData['category'] = $this->mapResolvedCategory($categorization->category);
        }
        if (isset($categorization->subCategory)) {
            $categorizationData['subCategory'] = $this->mapResolvedCategory($categorization->subCategory);
        }
        if (count($categorization->categories) > 0) {
            $categorizationData['categories'] = $categorization->categories;
        }

        $accountingCategory = strtolower($trainingType->accountingChar) === 'a' ? 'kostenfrei' : $trainingType->accountingCategory;

        $data = [
            'id' => $trainingType->short,
            'short' => $trainingType->short,
            'title' => $trainingType->title,
            'validFrom' => $this->formatDateTime($trainingType->validFrom),
            'validUntil' => $this->formatDateTime($trainingType->validUntil),
            'trainingForm' => $trainingType->trainingForm,
            'verbalDescription' => [
                'content' => [
                    'short' => $trainingType->verbalDescriptions->contentShort,
                    'long' => $trainingType->verbalDescriptions->contentLong,
                ],
                'notes' => $trainingType->verbalDescriptions->notes,
                'targetGroup' => $trainingType->verbalDescriptions->targetGroup,
                'goals' => $trainingType->verbalDescriptions->goals,
                'preconditions' => $trainingType->verbalDescriptions->preconditions,
                'methods' => $trainingType->verbalDescriptions->methods,
            ],
            'price' => [
                'vatExempt' => $trainingType->priceVatExempt,
                'external' => $trainingType->priceExternal,
            ],
            'capacity' => $trainingType->capacity,
            'targetGroup' => $trainingType->targetGroup,
            'accountingCategory' => $trainingType->accountingChar ? "{$trainingType->accountingChar} - {$accountingCategory}" : '',
            'duration' => [
                'hours' => $trainingType->duration->hours,
                'days' => $trainingType->duration->days,
            ],
            'belongsTo' => array_map([$this, 'convertObjectLink'], $trainingType->belongsTo),
            'requires' => array_map([$this, 'convertObjectLink'], $trainingType->requires),
            'intendedFor' => array_map([$this, 'convertObjectLink'], $trainingType->intendedFor),
            'hostedBy' => array_map([$this, 'convertObjectLink'], $trainingType->hostedBy),
            'mandatoryFor' => array_map([$this, 'convertObjectLink'], $trainingType->mandatoryFor),
            'needs' => array_map([$this, 'convertObjectLink'], $trainingType->needs),
            'mediates' => array_map([$this, 'convertObjectLink'], $trainingType->mediates),
            'contact' => [
                'content' => array_map([$this, 'convertContact'], $trainingType->contentResponsible),
                'event' => array_map([$this, 'convertContact'], $trainingType->eventResponsible),
                'speaker' => array_map([$this, 'convertContact'], $trainingType->speaker),
            ],
            'publication' => [
                'brochure' => $trainingType->showInBrochure,
                'intranet' => $trainingType->showInIntranet,
                'internet' => $trainingType->showInInternet,
            ],
            'categorization' => $categorizationData,
            'trainings' => array_map(function (TrainingDto $training) use ($trainingTypeIdIsNumeric) {
                $data = [
                    'id' => $training->short,
                    'short' => $training->short,
                    'objectType' => $training->objectType,
                    'title' => $training->title,
                    'validFrom' => $this->formatDateTime($training->validFrom),
                    'validUntil' => $this->formatDateTime($training->validUntil),
                    'location' => $training->location,
                    'registrationDeadline' => $this->formatDateTime($training->registrationDeadline),
                    'capacity' => [
                        'minimum' => $training->capacity->minimum,
                        'optimum' => $training->capacity->optimum,
                        'maximum' => $training->capacity->maximum,
                        'free' => $training->occupancy->free,
                        'waiting' => $training->occupancy->waiting,
                    ],
                    'duration' => [
                        'days' => $training->duration->days,
                        'hours' => $training->duration->hours,
                    ],
                    'price' => [
                        'external' => $training->priceExternal,
                    ],
                    'contact' => [
                        'content' => array_map([$this, 'convertContact'], $training->contentResponsible),
                        'event' => array_map([$this, 'convertContact'], $training->eventResponsible),
                        'speaker' => array_map([$this, 'convertContact'], $training->speaker),
                    ],
                    'rooms' => array_map([$this, 'convertObjectLink'], $training->rooms),
                    'schedule' => array_map(function (TrainingScheduleEntryDto $scheduleEntry) {
                        return [
                            'date' => $this->formatDateTime($scheduleEntry->date),
                            'begin' => $scheduleEntry->startTime,
                            'end' => $scheduleEntry->endTime,
                        ];
                    }, $training->schedule),
                    'status' => $training->status,
                    'linkToRegistration' => $trainingTypeIdIsNumeric ? $this->generateLinkToRegistration($training->id, $training->objectType, $training->planVariant) : null,
                    'trainingType' => $training->location->city === 'BigBlueButton' ? 'Online' : 'Präsenz',
                ];

                if (null === $data['linkToRegistration']) {
                    unset($data['linkToRegistration']);
                }

                return $data;
            }, $trainingType->trainings),
        ];

        if ($trainingTypeIdIsNumeric) {
            $data['linkToRegistration'] = $this->generateLinkToRegistration($trainingType->id, $trainingType->objectType, $trainingType->planVariant, true);
        }

        return $data;
    }

    public function mapIliasContact(IliasContactDto $contactDto): array
    {
        return [];
    }

    public function mapCategory(CategoryDto $category): array
    {
        $unsetEmpty = [
            'color',
            'specificTT',
            'ttCodeStartsWith',
            'specificTTAsAdditionalCategory',
        ];

        $data = [
            'title' => $category->title,
            'color' => $category->color ?? null,
            'children' => array_map(
                fn(CategoryDto $item): array => $this->mapCategory($item),
                $category->children
            ),
            'description' => $category->description,
            'iconImg' => $category->iconImg ?? null,
            'specificTT' => $category->specificTT,
            'ttCodeStartsWith' => $category->ttCodeStartsWith,
            'specificTTAsAdditionalCategory' => $category->specificTTAsAdditionalCategory,
        ];

        foreach ($unsetEmpty as $key) {
            if (array_key_exists($key, $data) && (null === $data[$key] || [] === $data[$key])) {
                unset($data[$key]);
            }
        }

        return $data;
    }

    private function formatDateTime(?DateTimeInterface $dateTime, DateTimeZone $timeZone = null): ?string
    {
        if ($dateTime === null) {
            return null;
        }

        if ($timeZone) {
            $dateTime = new DateTime($dateTime->format(DateTimeInterface::ATOM));
            $dateTime->setTimezone($timeZone);
            return preg_replace('/\+\d{2}:\d{2}$/', '.000Z', $dateTime->format(DateTimeInterface::ATOM));
        }

        return preg_replace('/(\+\d{2}:\d{2})$/', '.000$1', $dateTime->format(DateTimeInterface::ATOM));
    }

    private function convertObjectLink(ObjectLinkDto $objectLink): array
    {
        return [
            'type' => $objectLink->type,
            'short' => $objectLink->short,
            'label' => $objectLink->label,
        ];
    }

    private function convertContact(ContactDto $contact): array
    {
        return [
            'fullName' => $contact->fullName,
            'iliasContactReference' => $contact->iliasReference,
        ];
    }

    private function generateLinkToRegistration(string $id, string $objectType, string $planVariant, bool $isTrainingType = false): string
    {
        $fpmStartPageId = $isTrainingType ? 'COURSE_TYPE' : 'COURSE';

        return "https://ess.prod.bund.drv/irj/portal/fiori#EP--1878962114-Lernportal?DynamicParameter={$this->encodeURIComponent("\"FPM_START_PAGE_ID={$fpmStartPageId}&PLVAR={$planVariant}&OTYPE={$objectType}&OBJID={$id}\"")}";
    }

    private function encodeURIComponent(string $string): string
    {
        return urlencode($string);
    }

    private function mapResolvedCategory(ResolvedCategoryDto $category): array
    {
        $data = [
            'label' => $category->label,
            'color' => $category->color,
        ];

        if (isset($category->iconImg)) {
            $data['iconImg'] = $category->iconImg;
        }

        return $data;
    }
}
