<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Categories;

use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\CategorizationResultDto;

interface CategorizationResolver
{
    public function getCategories(): array;

    public function resolve(string $trainingTypeNumber): CategorizationResultDto;
}
