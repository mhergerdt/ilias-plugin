<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Categories;

use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\CategoryDto;

class CategoryRepository
{
    /**
     * @return CategoryDto[]
     */
    public function findAll(): array
    {
        return array_map(
            fn(array $item) => $this->mapCategory($item),
            json_decode(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'categories.json'), true)
        );
    }

    private function mapCategory(array $itemData): CategoryDto
    {
        $category = new CategoryDto();
        $category->title = $itemData['title'];
        $category->description = $itemData['description'] ?? null;
        $category->color = $itemData['color'];
        $category->iconImg = $itemData['iconImg'] ?? null;
        $category->ttCodeStartsWith = $itemData['ttCodeStartsWith'] ?? [];
        $category->specificTT = $itemData['specificTT'] ?? [];
        $category->specificTTAsAdditionalCategory = $itemData['specificTTAsAdditionalCategory'] ?? [];
        $category->children = array_map(
            fn(array $childItemData) => $this->mapCategory($childItemData),
            $itemData['children'] ?? []
        );

        return $category;
    }
}
