<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Categories;

use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\CategorizationResultDto;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\CategoryDto;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\ResolvedCategoryDto;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingTypeNumberScheme;

/**
 * @psalm-type FlattenedCategory = array{path: array<int, int>, schemes: array<TrainingTypeNumberScheme>, schemesAdditionalCategory: array<TrainingTypeNumberScheme>}
 */
class SimpleCategorizationResolver implements CategorizationResolver
{
    private CategoryRepository $categoryRepository;
    /**
     * @var array|null
     * @psalm-var array<int, FlattenedCategory>
     */
    private ?array $flattenedCategories;
    private array $typeMapping;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->typeMapping = [
            [
                'label' => 'Fachtagungen',
                'match' => [
                    '01',
                    '03',
                    '04',
                    '05',
                    '06',
                    '07',
                    '08',
                    '09',
                ],
            ],
            [
                'label' => 'Seminare und Workshops',
                'match' => [
                    '10',
                    '12',
                    '15',
                    '20',
                    '21',
                    '25',
                    '30',
                    '35',
                    '40',
                    '41',
                    '42',
                    '44',
                    '45',
                    '48',
                    '50',
                    '55',
                    '56',
                    '60',
                    '70',
                    '71',
                    '72',
                    '78',
                    '75',
                    '96',
                ],
            ],
        ];
    }

    public function getCategories(): array
    {
        return $this->categoryRepository->findAll();
    }

    public function resolve(string $trainingTypeNumber): CategorizationResultDto
    {
        if (!isset($this->flattenedCategories)) {
            $this->flattenedCategories = $this->flattenCategories($this->categoryRepository->findAll());
        }

        $categories = $this->categoryRepository->findAll();

        $result = new CategorizationResultDto();
        $result->type = 'Unbekannt';
        $result->topicLabel = 'N/A';
        $result->topicColor = '#aaa';
        $result->category = null;
        $result->subCategory = null;
        $result->categories = [];

        $resolvedCategoryPaths = [];
        /**
         * @psalm-var array<int, int>|null $resolvedMainCategoryPath
         */
        $resolvedMainCategoryPath = null;
        foreach ($this->flattenedCategories as $flattenCategory) {
            foreach ($flattenCategory['schemes'] as $scheme) {
                if ($scheme->matches($trainingTypeNumber)) {
                    if ($resolvedMainCategoryPath === null) {
                        $resolvedMainCategoryPath = $flattenCategory['path'];
                        $resolvedCategoryPaths[] = $flattenCategory['path'];
                    }
                }
            }

            foreach ($flattenCategory['schemesAdditionalCategory'] as $scheme) {
                if ($scheme->matches($trainingTypeNumber)) {
                    $resolvedCategoryPaths[] = $flattenCategory['path'];
                }
            }
        }

        if ($resolvedMainCategoryPath !== null) {
            $firstLevel = $resolvedMainCategoryPath[0] ?? null;
            if (null !== $firstLevel) {
                $topic = $categories[$firstLevel];
                $result->topicLabel = $topic->title;
                $result->topicColor = $topic->color ?? $result->topicColor;

                $secondLevel = $resolvedMainCategoryPath[1] ?? null;
                if (null !== $secondLevel) {
                    $category = $topic->children[$secondLevel] ?? null;
                    if ($category !== null) {
                        $result->category = new ResolvedCategoryDto();
                        $result->category->label = $category->title;
                        $result->category->color = $category->color ?? $result->topicColor;

                        if (isset($category->iconImg)) {
                            $result->category->iconImg = $category->iconImg;
                        }

                        $thirdLevel = $resolvedMainCategoryPath[2] ?? null;
                        if (null !== $thirdLevel) {
                            $subCategory = $category->children[$thirdLevel] ?? null;
                            if ($subCategory !== null) {
                                $result->subCategory = new ResolvedCategoryDto();
                                $result->subCategory->label = $subCategory->title;
                                $result->subCategory->color = $subCategory->color ?? $result->category->color;

                                if (isset($subCategory->iconImg)) {
                                    $result->subCategory->iconImg = $subCategory->iconImg;
                                }
                            }
                        }
                    }
                }
            }
        }

        $firstTwoDigits = substr($trainingTypeNumber, 0, 2);
        foreach ($this->typeMapping as $type) {
            if (in_array($firstTwoDigits, $type['match'])) {
                $result->type = $type['label'];
            }
        }

        $indexTree = [];

        foreach ($resolvedCategoryPaths as $resolvedCategoryPath) {
            $tmpJoinedPath = &$indexTree;

            do {
                $index = array_shift($resolvedCategoryPath);
                if (!isset($tmpJoinedPath[$index])) {
                    $tmpJoinedPath[$index] = [];
                }
                $tmpJoinedPath = &$tmpJoinedPath[$index];
            } while(count($resolvedCategoryPath) > 0);
        }

        $result->categories = $this->resolveCategories($indexTree, $categories, '#aaa');

        return $result;
    }

    /**
     * @param CategoryDto[] $categories
     * @param array<int, int> $parentPath
     * @psalm-return array<int, FlattenedCategory>
     */
    private function flattenCategories(array $categories, array $parentPath = []): array
    {
        $flattenCategories = [];

        foreach ($categories as $index => $category) {
            $path = [...$parentPath, $index];
            $entry = [
                'path' => $path,
                'schemes' => [],
                'schemesAdditionalCategory' => [],
            ];

            foreach ($category->ttCodeStartsWith as $item) {
                $entry['schemes'][] = new TrainingTypeNumberScheme($item . 'xx.xxxx');
            }

            foreach ($category->specificTT as $item) {
                $entry['schemes'][] = new TrainingTypeNumberScheme($item);
            }

            foreach ($category->specificTTAsAdditionalCategory as $item) {
                $entry['schemesAdditionalCategory'][] = new TrainingTypeNumberScheme($item);
            }

            $flattenCategories[] = $entry;

            if (count($category->children) > 0) {
                foreach ($this->flattenCategories($category->children, $path) as $item) {
                    $flattenCategories[] = $item;
                }
            }
        }

        return $flattenCategories;
    }

    /**
     * @param CategoryDto[] $categories
     * @return ResolvedCategoryDto[]
     */
    private function resolveCategories(array $indexTree, array $categories, string $parentColor): array
    {
        $resolvedCategories = [];

        foreach ($indexTree as $index => $childIndexTree) {
            $category = $categories[$index];
            $color = $category->color ?? $parentColor;

            $resolvedCategory = new ResolvedCategoryDto();
            $resolvedCategory->label = $category->title;

            if (isset($category->iconImg)) {
                $resolvedCategory->iconImg = $category->iconImg;
            }
            $resolvedCategory->color = $color;
            $resolvedCategory->childCategories = $this->resolveCategories($childIndexTree, $category->children, $color);
            $resolvedCategories[] = $resolvedCategory;
        }

        return $resolvedCategories;
    }
}
