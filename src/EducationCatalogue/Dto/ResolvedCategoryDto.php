<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto;

class ResolvedCategoryDto
{
    public string $label;
    public ?string $iconImg;
    public ?string $color;
    /**
     * @var ResolvedCategoryDto[]
     */
    public array $childCategories = [];
}
