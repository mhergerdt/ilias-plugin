<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto;

use DateTimeInterface;
use DRVBund\Plugins\CGAutomation\Shared\Dto\IliasContactDto;
use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;

class EducationCatalogueDataDto
{
    /**
     * @var EducationCatalogueItemDto[]
     */
    public array $items = [];
    /**
     * @var IliasContactDto[]
     */
    public array $iliasContacts = [];
    /**
     * @var CategoryDto[]
     */
    public array $categories = [];
    public DateTimeInterface $exportedAt;
}
