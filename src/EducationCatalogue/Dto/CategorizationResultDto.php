<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto;

class CategorizationResultDto
{
    public string $type;
    public string $topicLabel;
    public string $topicColor;
    public ?ResolvedCategoryDto $category;
    public ?ResolvedCategoryDto $subCategory;
    /**
     * @var ResolvedCategoryDto[]
     */
    public array $categories;
}
