<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto;

class CategoryDto
{
    public string $title;
    public ?string $description;
    public ?string $color;
    public ?string $iconImg;
    /**
     * @var string[]
     */
    public array $ttCodeStartsWith;
    /**
     * @var string[]
     */
    public array $specificTT;
    /**
     * @var string[]
     */
    public array $specificTTAsAdditionalCategory;
    /**
     * @var CategoryDto[]
     */
    public array $children;
}
