<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto;

use DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto;

class EducationCatalogueItemDto
{
    public TrainingTypeDto $trainingType;
    public CategorizationResultDto $categorization;
}
