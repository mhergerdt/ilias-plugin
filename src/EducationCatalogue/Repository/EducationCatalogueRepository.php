<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Repository;

use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\EducationCatalogueDataDto;

interface EducationCatalogueRepository
{
    public function save(EducationCatalogueDataDto $educationCatalogueData): void;
}
