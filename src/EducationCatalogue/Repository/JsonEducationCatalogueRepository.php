<?php

namespace DRVBund\Plugins\CGAutomation\EducationCatalogue\Repository;

use DateTime;
use DateTimeInterface;
use DateTimeZone;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\CategoryDto;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\EducationCatalogueDataDto;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Dto\EducationCatalogueItemDto;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Mapper\EducationCatalogueDataJsonMapper;
use DRVBund\Plugins\CGAutomation\Ilias\Adapter\FilesystemAdapter;
use DRVBund\Plugins\CGAutomation\Shared\Dto\IliasContactDto;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\FilePath;

class JsonEducationCatalogueRepository implements EducationCatalogueRepository
{
    private EducationCatalogueDataJsonMapper $mapper;
    private FilesystemAdapter $filesystem;
    private FilePath $filePath;

    public function __construct(EducationCatalogueDataJsonMapper $mapper, FilesystemAdapter $filesystem, FilePath $filePath)
    {
        $this->mapper = $mapper;
        $this->filesystem = $filesystem;
        $this->filePath = $filePath;
    }

    public function save(EducationCatalogueDataDto $educationCatalogueData): void
    {
        $exportedAt = new DateTime($educationCatalogueData->exportedAt->format(DateTimeInterface::ATOM));
        $exportedAt->setTimezone(new DateTimeZone('Europe/Berlin'));

        $data = [
            'exportedAt' => preg_replace('/(\+\d{2}:\d{2})$/', '.000$1', $exportedAt->format(DateTimeInterface::ATOM)),
            'data' => [
                'trainingTypes' => array_map(
                    fn(EducationCatalogueItemDto $item): array => $this->mapper->mapItem($item),
                    $educationCatalogueData->items
                ),
                'iliasContacts' => array_map(
                    fn(IliasContactDto $item): array => $this->mapper->mapIliasContact($item),
                    $educationCatalogueData->iliasContacts
                ),
                'categories' => array_map(
                    fn(CategoryDto $item): array => $this->mapper->mapCategory($item),
                    $educationCatalogueData->categories
                ),
            ]
        ];

        $this->filesystem->put(
            (string)$this->filePath,
            json_encode($data, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
        );

        $this->createChecksumFile($this->filePath->getPath()->appendFileName('checksum.json'), $this->filePath);
    }

    private function createChecksumFile(FilePath $filePath, FilePath $source): void
    {
        $data = str_replace('"iliasContacts": []', '"iliasContacts": {}', $this->filesystem->read((string)$source));
        $checksum = hash('sha512', $data);
        $this->filesystem->put($filePath.'.test', $data);
        $this->filesystem->put((string)$filePath, json_encode($checksum));
    }
}
