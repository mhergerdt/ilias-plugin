<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\DTO;

use DateTimeInterface;
use ilGroupConstants;
use ilObjGroup;

final class GroupDTO
{
    private string $title;
    private string $description;
    private string $importId;
    private bool $useShowMembers;
    private bool $offlineStatus;
    private bool $useAutoNotification;
    private bool $useRegistrationAccessCode;
    private ?DateTimeInterface $periodFrom;
    private ?DateTimeInterface $periodUntil;
    private bool $useNewsTimeline;
    private bool $newsBlockActivated;
    private bool $useInfoTabVisibility;
    private bool $useNews;
    private bool $withCustomMetadata;
    private bool $withAutoRatingNewObjects;
    private bool $useTagCloud;
    private bool $useNewsVisibility;
    private bool $useBadges;
    private bool $useBooking;
    private bool $useCalendarActivation;
    private bool $useCalendarVisibility;
    private string $externalMailPrefix;
    private int $mailToMembersType;
    private int $registrationType;
    private int $groupType;

    public function __construct(string $title, ?string $importId = null)
    {
        $this->title = $title;
        $this->description = '';
        $this->importId = $importId ?? '';
        $this->useShowMembers = false;
        $this->offlineStatus = false;
        $this->useAutoNotification = false;
        $this->useRegistrationAccessCode = false;
        $this->periodFrom = null;
        $this->periodUntil = null;
        $this->useNewsTimeline = false;
        $this->newsBlockActivated = false;
        $this->useInfoTabVisibility = false;
        $this->useNews = false;
        $this->withCustomMetadata = false;
        $this->withAutoRatingNewObjects = false;
        $this->useTagCloud = false;
        $this->useNewsVisibility = false;
        $this->useBadges = false;
        $this->useBooking = false;
        $this->useCalendarActivation = false;
        $this->useCalendarVisibility = false;
        $this->externalMailPrefix = '';
        $this->mailToMembersType = ilObjGroup::GRP_ADMIN;
        $this->registrationType = ilGroupConstants::GRP_REGISTRATION_DEACTIVATED;
        $this->groupType = ilGroupConstants::GRP_TYPE_CLOSED;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): GroupDTO
    {
        $this->title = $title;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): GroupDTO
    {
        $this->description = $description;
        return $this;
    }

    public function getImportId(): string
    {
        return $this->importId;
    }

    public function setImportId(string $importId): GroupDTO
    {
        $this->importId = $importId;
        return $this;
    }

    public function getUseShowMembers(): bool
    {
        return $this->useShowMembers;
    }

    public function setUseShowMembers(bool $useShowMembers): GroupDTO
    {
        $this->useShowMembers = $useShowMembers;
        return $this;
    }

    public function getOfflineStatus(): bool
    {
        return $this->offlineStatus;
    }

    public function setOfflineStatus(bool $offlineStatus): GroupDTO
    {
        $this->offlineStatus = $offlineStatus;
        return $this;
    }

    public function getUseAutoNotification(): bool
    {
        return $this->useAutoNotification;
    }

    public function setUseAutoNotification(bool $useAutoNotification): GroupDTO
    {
        $this->useAutoNotification = $useAutoNotification;
        return $this;
    }

    public function getUseRegistrationAccessCode(): bool
    {
        return $this->useRegistrationAccessCode;
    }

    public function setUseRegistrationAccessCode(bool $useRegistrationAccessCode): GroupDTO
    {
        $this->useRegistrationAccessCode = $useRegistrationAccessCode;
        return $this;
    }

    public function getPeriodFrom(): ?DateTimeInterface
    {
        return $this->periodFrom;
    }

    public function setPeriodFrom(?DateTimeInterface $periodFrom): GroupDTO
    {
        $this->periodFrom = $periodFrom;
        return $this;
    }

    public function getPeriodUntil(): ?DateTimeInterface
    {
        return $this->periodUntil;
    }

    public function setPeriodUntil(?DateTimeInterface $periodUntil): GroupDTO
    {
        $this->periodUntil = $periodUntil;
        return $this;
    }

    public function getUseNewsTimeline(): bool
    {
        return $this->useNewsTimeline;
    }

    public function setUseNewsTimeline(bool $useNewsTimeline): GroupDTO
    {
        $this->useNewsTimeline = $useNewsTimeline;
        return $this;
    }

    public function getNewsBlockActivated(): bool
    {
        return $this->newsBlockActivated;
    }

    public function setNewsBlockActivated(bool $newsBlockActivated): GroupDTO
    {
        $this->newsBlockActivated = $newsBlockActivated;
        return $this;
    }

    public function getUseInfoTabVisibility(): bool
    {
        return $this->useInfoTabVisibility;
    }

    public function setUseInfoTabVisibility(bool $useInfoTabVisibility): GroupDTO
    {
        $this->useInfoTabVisibility = $useInfoTabVisibility;
        return $this;
    }

    public function getUseNews(): bool
    {
        return $this->useNews;
    }

    public function setUseNews(bool $useNews): GroupDTO
    {
        $this->useNews = $useNews;
        return $this;
    }

    public function getWithCustomMetadata(): bool
    {
        return $this->withCustomMetadata;
    }

    public function setWithCustomMetadata(bool $withCustomMetadata): GroupDTO
    {
        $this->withCustomMetadata = $withCustomMetadata;
        return $this;
    }

    public function getWithAutoRatingNewObjects(): bool
    {
        return $this->withAutoRatingNewObjects;
    }

    public function setWithAutoRatingNewObjects(bool $withAutoRatingNewObjects): GroupDTO
    {
        $this->withAutoRatingNewObjects = $withAutoRatingNewObjects;
        return $this;
    }

    public function getUseTagCloud(): bool
    {
        return $this->useTagCloud;
    }

    public function setUseTagCloud(bool $useTagCloud): GroupDTO
    {
        $this->useTagCloud = $useTagCloud;
        return $this;
    }

    public function getUseNewsVisibility(): bool
    {
        return $this->useNewsVisibility;
    }

    public function setUseNewsVisibility(bool $useNewsVisibility): GroupDTO
    {
        $this->useNewsVisibility = $useNewsVisibility;
        return $this;
    }

    public function getUseBadges(): bool
    {
        return $this->useBadges;
    }

    public function setUseBadges(bool $useBadges): GroupDTO
    {
        $this->useBadges = $useBadges;
        return $this;
    }

    public function getUseBooking(): bool
    {
        return $this->useBooking;
    }

    public function setUseBooking(bool $useBooking): GroupDTO
    {
        $this->useBooking = $useBooking;
        return $this;
    }

    public function getUseCalendarActivation(): bool
    {
        return $this->useCalendarActivation;
    }

    public function setUseCalendarActivation(bool $useCalendarActivation): GroupDTO
    {
        $this->useCalendarActivation = $useCalendarActivation;
        return $this;
    }

    public function getUseCalendarVisibility(): bool
    {
        return $this->useCalendarVisibility;
    }

    public function setUseCalendarVisibility(bool $useCalendarVisibility): GroupDTO
    {
        $this->useCalendarVisibility = $useCalendarVisibility;
        return $this;
    }

    public function getExternalMailPrefix(): string
    {
        return $this->externalMailPrefix;
    }

    public function setExternalMailPrefix(string $externalMailPrefix): GroupDTO
    {
        $this->externalMailPrefix = $externalMailPrefix;
        return $this;
    }

    public function getMailToMembersType(): int
    {
        return $this->mailToMembersType;
    }

    public function mailToMembersTypeAdmin(): GroupDTO
    {
        $this->mailToMembersType = ilObjGroup::GRP_ADMIN;
        return $this;
    }

    public function getRegistrationType(): int
    {
        return $this->registrationType;
    }

    public function registrationTypeDeactivated(): GroupDTO
    {
        $this->registrationType = ilGroupConstants::GRP_REGISTRATION_DEACTIVATED;
        return $this;
    }

    public function getGroupType(): int
    {
        return $this->groupType;
    }

    public function groupTypeClosed(): GroupDTO
    {
        $this->groupType = ilGroupConstants::GRP_TYPE_CLOSED;
        return $this;
    }
}
