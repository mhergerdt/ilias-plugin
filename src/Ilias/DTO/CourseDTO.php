<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\DTO;

use DateTime;
use ilCourseConstants;

final class CourseDTO
{
    private string $title;
    private string $description;
    private string $importId;
    private bool $useShowMembers;
    private bool $offlineStatus;
    private bool $useAutoNotification;
    private bool $useRegistrationAccessCode;
    private ?DateTime $periodFrom;
    private ?DateTime $periodUntil;
    private bool $useInfoTabVisibility;
    private bool $useNews;
    private bool $useCustomMetadata;
    private bool $useAutoRatingNewObjects;
    private bool $useTagCloud;
    private bool $useNewsVisibility;
    private bool $useBadges;
    private bool $useBooking;
    private string $externalMailPrefix;
    private bool $useCalendarActivation;
    private bool $useCalendarVisibility;
    private int $subscriptionType;
    private int $timingMode;
    private int $mailToMembersType;
    private int $viewMode;

    public function __construct(string $title, ?string $importId = null)
    {
        $this->importId = $importId ?? '';
        $this->title = $title;
        $this->description = '';
        $this->useShowMembers = false;
        $this->offlineStatus = false;
        $this->useAutoNotification = false;
        $this->useRegistrationAccessCode = false;
        $this->periodFrom = null;
        $this->periodUntil = null;
        $this->useInfoTabVisibility = false;
        $this->useNews = false;
        $this->useCustomMetadata = false;
        $this->useAutoRatingNewObjects = false;
        $this->useTagCloud = false;
        $this->useNewsVisibility = false;
        $this->useBadges = false;
        $this->useBooking = false;
        $this->externalMailPrefix = '';
        $this->useCalendarActivation = false;
        $this->useCalendarVisibility = false;
        $this->subscriptionType = ilCourseConstants::IL_CRS_SUBSCRIPTION_DEACTIVATED;
        $this->timingMode = ilCourseConstants::IL_CRS_VIEW_TIMING_ABSOLUTE;
        $this->mailToMembersType = ilCourseConstants::MAIL_ALLOWED_TUTORS;
        $this->viewMode = ilCourseConstants::IL_CRS_VIEW_BY_TYPE;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $value): CourseDTO
    {
        $this->description = $value;
        return $this;
    }

    public function getImportId(): string
    {
        return $this->importId;
    }

    public function getUseShowMembers(): bool
    {
        return $this->useShowMembers;
    }

    public function setUseShowMembers(bool $value): CourseDTO
    {
        $this->useShowMembers = $value;
        return $this;
    }

    public function getOfflineStatus(): bool
    {
        return $this->offlineStatus;
    }

    public function setOfflineStatus(bool $value): CourseDTO
    {
        $this->offlineStatus = $value;
        return $this;
    }

    public function getUseAutoNotification(): bool
    {
        return $this->useAutoNotification;
    }

    public function setUseAutoNotification(bool $value): CourseDTO
    {
        $this->useAutoNotification = $value;
        return $this;
    }

    public function getUseRegistrationAccessCode(): bool
    {
        return $this->useRegistrationAccessCode;
    }

    public function setUseRegistrationAccessCode(bool $value): CourseDTO
    {
        $this->useRegistrationAccessCode = $value;
        return $this;
    }

    public function getPeriodFrom(): ?DateTime
    {
        return $this->periodFrom;
    }

    public function setPeriodFrom(?DateTime $periodFrom): CourseDTO
    {
        $this->periodFrom = $periodFrom;
        return $this;
    }

    public function getPeriodUntil(): ?DateTime
    {
        return $this->periodUntil;
    }

    public function setPeriodUntil(?DateTime $periodUntil): CourseDTO
    {
        $this->periodUntil = $periodUntil;
        return $this;
    }

    public function getUseInfoTabVisibility(): bool
    {
        return $this->useInfoTabVisibility;
    }

    public function setUseInfoTabVisibility(bool $value): CourseDTO
    {
        $this->useInfoTabVisibility = $value;
        return $this;
    }

    public function getUseNews(): bool
    {
        return $this->useNews;
    }

    public function setUseNews(bool $value): CourseDTO
    {
        $this->useNews = $value;
        return $this;
    }

    public function getWithCustomMetadata(): bool
    {
        return $this->useCustomMetadata;
    }

    public function setWithCustomMetadata(bool $value): CourseDTO
    {
        $this->useCustomMetadata = $value;
        return $this;
    }

    public function getWithAutoRatingNewObjects(): bool
    {
        return $this->useAutoRatingNewObjects;
    }

    public function setWithAutoRatingNewObjects(bool $value): CourseDTO
    {
        $this->useAutoRatingNewObjects = $value;
        return $this;
    }

    public function getUseTagCloud(): bool
    {
        return $this->useTagCloud;
    }

    public function setUseTagCloud(bool $value): CourseDTO
    {
        $this->useTagCloud = $value;
        return $this;
    }

    public function getUseNewsVisibility(): bool
    {
        return $this->useNewsVisibility;
    }

    public function setUseNewsVisibility(bool $value): CourseDTO
    {
        $this->useNewsVisibility = $value;
        return $this;
    }

    public function getUseBadges(): bool
    {
        return $this->useBadges;
    }

    public function setUseBadges(bool $value): CourseDTO
    {
        $this->useBadges = $value;
        return $this;
    }

    public function getUseBooking(): bool
    {
        return $this->useBooking;
    }

    public function setUseBooking(bool $value): CourseDTO
    {
        $this->useBooking = $value;
        return $this;
    }

    public function getExternalMailPrefix(): string
    {
        return $this->externalMailPrefix;
    }

    public function setExternalMailPrefix(string $value): CourseDTO
    {
        $this->externalMailPrefix = $value;
        return $this;
    }

    public function getUseCalendarActivation(): bool
    {
        return $this->useCalendarActivation;
    }

    public function setUseCalendarActivation(bool $value): CourseDTO
    {
        $this->useCalendarActivation = $value;
        return $this;
    }

    public function getUseCalendarVisibility(): bool
    {
        return $this->useCalendarVisibility;
    }

    public function setUseCalendarVisibility(bool $value): CourseDTO
    {
        $this->useCalendarVisibility = $value;
        return $this;
    }

    public function getSubscriptionType(): int
    {
        return $this->subscriptionType;
    }

    public function subscriptionDeactivated(): CourseDTO
    {
        $this->subscriptionType = ilCourseConstants::IL_CRS_SUBSCRIPTION_DEACTIVATED;
        return $this;
    }

    public function getTimingMode(): int
    {
        return $this->timingMode;
    }

    public function timingModeAbsolute(): CourseDTO
    {
        $this->timingMode = ilCourseConstants::IL_CRS_VIEW_TIMING_ABSOLUTE;
        return $this;
    }

    public function getMailToMembersType(): int
    {
        return $this->mailToMembersType;
    }

    public function mailToMembersTypeAllowedTutors(): CourseDTO
    {
        $this->mailToMembersType = ilCourseConstants::MAIL_ALLOWED_TUTORS;
        return $this;
    }

    public function getViewMode(): int
    {
        return $this->viewMode;
    }

    public function viewModeViewByType(): CourseDTO
    {
        $this->viewMode = ilCourseConstants::IL_CRS_VIEW_BY_TYPE;
        return $this;
    }
}
