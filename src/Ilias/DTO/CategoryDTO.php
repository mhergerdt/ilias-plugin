<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\DTO;

final class CategoryDTO
{
    private string $title;
    private string $importId;

    public function __construct(string $title, ?string $importId = null)
    {
        $this->title = $title;
        $this->importId = $importId ?? '';
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getImportId(): string
    {
        return $this->importId;
    }
}
