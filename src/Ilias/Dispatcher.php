<?php

namespace DRVBund\Plugins\CGAutomation\Ilias;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;
use DRVBund\Plugins\CGAutomation\Ilias\Query\Query;

/**
 * @template TCommandResult
 * @template TQueryResult
 */
interface Dispatcher
{
    public function dispatchCommand(Command $command): void;

    /**
     * @return TCommandResult
     */
    public function dispatchCommandWithResult(CommandWithResult $command);

    /**
     * @return TQueryResult
     */
    public function dispatchQuery(Query $query);
}
