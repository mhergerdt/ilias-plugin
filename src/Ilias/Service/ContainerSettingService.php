<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Service;

use ilContainer;

class ContainerSettingService
{
    public function setBooleanSetting(int $objId, string $key, bool $value): self
    {
        ilContainer::_writeContainerSetting($objId, $key, $value ? '1' : '0');
        return $this;
    }

    public function setStringSetting(int $objId, string $key, string $value): self
    {
        ilContainer::_writeContainerSetting($objId, $key, $value);
        return $this;
    }
}
