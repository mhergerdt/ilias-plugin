<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Service;

use DRVBund\Plugins\CGAutomation\Ilias\Repository\ObjectRepository;
use Exception;
use ilTree;

class TreeService
{
    private ilTree $tree;
    private ObjectRepository $objectRepository;

    public function __construct(ilTree $tree, ObjectRepository $objectRepository)
    {
        $this->tree = $tree;
        $this->objectRepository = $objectRepository;
    }

    public function getParentId(int $refId): ?int
    {
        return $this->tree->getParentId($refId);
    }

    public function resolveObjRefIdUnderParentTreeItem(int $objId, int $parentRefId): int
    {
        $refIds = $this->objectRepository->loadRefIdsForObjectId($objId);

        $objRefId = null;
        foreach ($refIds as $refId) {
            if (in_array($parentRefId, $this->tree->getPathId($refId))) {
                $objRefId = $refId;
            }
        }

        if ($objRefId === null) {
            throw new Exception(sprintf(
                'Object with id "%s" is not a child of parent ref id %s',
                $objId,
                $parentRefId,
            ));
        }

        return $objRefId;
    }
}
