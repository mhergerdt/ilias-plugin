<?php

namespace DRVBund\Plugins\CGAutomation\Ilias;

trait DevModeTrait
{
    private function isDevModeActive(): bool
    {
        return defined('DEVMODE') && DEVMODE;
    }
}
