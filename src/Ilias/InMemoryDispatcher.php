<?php

namespace DRVBund\Plugins\CGAutomation\Ilias;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandWithResultHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Query\Handler\QueryHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Query\Query;
use Exception;

/**
 * @template TCommandResult
 * @template TQueryResult
 * @template-implements Dispatcher<TCommandResult, TQueryResult>
 */
class InMemoryDispatcher implements Dispatcher
{
    /**
     * @var array<string, CommandHandler>
     */
    private array $mappedCommandHandler;
    /**
     * @var array<string, CommandWithResultHandler>
     */
    private array $mappedCommandWithResultHandler;

    /**
     * @var array<string, QueryHandler>
     */
    private array $mappedQueryHandler;

    public function addCommandHandler(CommandHandler $commandHandler): void
    {
        $this->mappedCommandHandler[$commandHandler->handles()] = $commandHandler;
    }

    public function addCommandWithResultHandler(CommandWithResultHandler $commandHandler): void
    {
        $this->mappedCommandWithResultHandler[$commandHandler->handles()] = $commandHandler;
    }

    public function addQueryHandler(QueryHandler $queryHandler): void
    {
        $this->mappedQueryHandler[$queryHandler->handles()] = $queryHandler;
    }

    public function dispatchCommand(Command $command): void
    {
        $key = $command::getName();

        if (!array_key_exists($key, $this->mappedCommandHandler)) {
            throw new Exception("No handler found for command '{$key}'");
        }

        $this->mappedCommandHandler[$key]->handle($command);
    }

    /**
     * @return TCommandResult
     */
    public function dispatchCommandWithResult(CommandWithResult $command)
    {
        $key = $command::getName();

        if (!array_key_exists($key, $this->mappedCommandWithResultHandler)) {
            throw new Exception("No handler found for command '{$key}'");
        }

        return $this->mappedCommandWithResultHandler[$key]->handle($command);
    }

    /**
     * @return TQueryResult
     */
    public function dispatchQuery(Query $query)
    {
        $key = $query::getName();

        if (!array_key_exists($key, $this->mappedQueryHandler)) {
            throw new Exception("No handler found for query '{$key}'");
        }

        return $this->mappedQueryHandler[$key]->handle($query);
    }
}
