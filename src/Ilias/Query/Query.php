<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Query;

/**
 * @psalm-immutable
 */
interface Query
{
    public static function getName(): string;
}
