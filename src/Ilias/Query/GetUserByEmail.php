<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Query;


/**
 * @psalm-immutable
 */
class GetUserByEmail implements Query
{
    private string $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public static function getName(): string
    {
        return 'GetUserByEmail';
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}
