<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Query\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Query\GetLocalRolesForTreeItem;
use DRVBund\Plugins\CGAutomation\Ilias\Query\Query;
use ilObjectFactory;
use ilObjRole;
use ilRbacReview;

/**
 * @psalm-api
 * @template-implements QueryHandler<GetLocalRolesForTreeItem, ilObjRole[]>
 */
class GetLocalRolesForTreeItemHandler implements QueryHandler
{
    private ilRbacReview $rbacReview;

    public function __construct(ilRbacReview $rbacReview)
    {
        $this->rbacReview = $rbacReview;
    }

    public function handles(): string
    {
        return GetLocalRolesForTreeItem::getName();
    }

    /**
     * @psalm-suppress UnusedForeachValue
     */
    public function handle(Query $query)
    {
        $refId = $query->getRefId();

        /** @var int[] $roleIds */
        $roleIds = $this->rbacReview->getLocalRoles($refId);

        $roles = [];
        foreach ($roleIds as $roleId) {
            $role = ilObjectFactory::getInstanceByObjId($roleId);

            if ($role instanceof ilObjRole) {
                $roles[] = $role;
            }
        }

        return $roles;
    }
}
