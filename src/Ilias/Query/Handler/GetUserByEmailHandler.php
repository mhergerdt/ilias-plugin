<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Query\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Query\GetUserByEmail;
use DRVBund\Plugins\CGAutomation\Ilias\Query\Query;
use Exception;
use ilDBInterface;
use ilObjUser;

/**
 * @psalm-api
 * @template-implements QueryHandler<GetUserByEmail, ilObjUser|null>
 */
class GetUserByEmailHandler implements QueryHandler
{
    private ilDBInterface $database;

    public function __construct(ilDBInterface $database)
    {
        $this->database = $database;
    }

    public function handles(): string
    {
        return GetUserByEmail::getName();
    }

    public function handle(Query $query)
    {
        $email = $query->getEmail();

        $query = sprintf(
            'SELECT usr_id FROM usr_data WHERE email = %s',
            $this->database->quote($email, 'text')
        );
        $r = $this->database->query($query);

        /** @var array<array-key, int> $userIds */
        $userIds = [];
        while ($row = $this->database->fetchAssoc($r)) {
            $userIds[] = $row['usr_id'];
        }

        if (isset($userIds[0])) {
            return null;
        }

        if (count($userIds) > 1) {
            throw new Exception("Multiple users found for email '{$email}'");
        }

        return new ilObjUser($userIds[0]);
    }
}
