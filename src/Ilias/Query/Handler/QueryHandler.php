<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Query\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Query\Query;

/**
 * @psalm-api
 * @template TQuery as Query
 * @template TQueryResult
 */
interface QueryHandler
{
    /**
     * @return string
     */
    public function handles(): string;

    /**
     * @psalm-param TQuery $query
     * @return TQueryResult
     */
    public function handle(Query $query);
}
