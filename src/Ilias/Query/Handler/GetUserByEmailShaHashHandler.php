<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Query\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Query\GetUserByEmailShaHash;
use DRVBund\Plugins\CGAutomation\Ilias\Query\Query;
use Exception;
use ilDBInterface;
use ilObjUser;

/**
 * @psalm-api
 * @template-implements QueryHandler<GetUserByEmailShaHash, ilObjUser|null>
 */
class GetUserByEmailShaHashHandler implements QueryHandler
{
    private ilDBInterface $database;

    public function __construct(ilDBInterface $database)
    {
        $this->database = $database;
    }

    public function handles(): string
    {
        return GetUserByEmailShaHash::getName();
    }

    public function handle(Query $query)
    {
        $emailHash = $query->getEmailHash();
        $shaType = $query->getShaType();

        $query = sprintf(
            'SELECT usr_id FROM usr_data WHERE SHA2(email, %d) = %s',
            $shaType,
            $this->database->quote($emailHash, 'text')
        );
        $r = $this->database->query($query);

        /** @var array<int, int> $userIds */
        $userIds = [];
        while ($row = $this->database->fetchAssoc($r)) {
            $userIds[] = $row['usr_id'];
        }

        if (!isset($userIds[0])) {
            return null;
        }

        if (count($userIds) > 1) {
            throw new Exception("Multiple users found for email hash '{$emailHash}'");
        }

        return new ilObjUser($userIds[0]);
    }
}
