<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Query\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Dispatcher;
use DRVBund\Plugins\CGAutomation\Ilias\Query\GetLocalRoleByParticipantRole;
use DRVBund\Plugins\CGAutomation\Ilias\Query\GetLocalRolesForTreeItem;
use DRVBund\Plugins\CGAutomation\Ilias\Query\Query;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ParticipantRole;
use ilObjRole;

/**
 * @psalm-api
 * @template-implements QueryHandler<GetLocalRoleByParticipantRole, ?ilObjRole>
 */
class GetLocalRoleByParticipantRoleHandler implements QueryHandler
{
    private Dispatcher $dispatcher;

    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function handles(): string
    {
        return 'GetLocalRoleByParticipantRole';
    }

    public function handle(Query $query)
    {
        $objRefId = $query->getObjRefId();
        $participantRole = $query->getParticipantRole();

        /** @var ilObjRole[] $roles */
        $roles = $this->dispatcher->dispatchQuery(new GetLocalRolesForTreeItem($objRefId));

        foreach ($roles as $role) {
            if ($this->matches($role->getTitle(), $objRefId, $participantRole)) {
                return $role;
            }
        }

        return null;
    }

    public function matches(string $roleTitle, int $objRefId, ParticipantRole $participantRole): bool
    {
        return $roleTitle === sprintf('%s_%s', $participantRole->getRolePrefix(), $objRefId);
    }
}
