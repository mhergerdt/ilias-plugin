<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Query\Handler\Decorator;

use DRVBund\Plugins\CGAutomation\Ilias\Query\Handler\QueryHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Query\Query;

/**
 * @psalm-api
 * @template-implements QueryHandler<Query, mixed>
 */
class InMemoryCachingQueryHandler implements QueryHandler
{
    private QueryHandler $handler;
    private array $cache = [];

    public function __construct(QueryHandler $handler)
    {
        $this->handler = $handler;
    }

    public function handles(): string
    {
        return $this->handler->handles();
    }

    public function handle(Query $query)
    {
        $cacheKey = $this->getCacheKey($query);

        if (!isset($this->cache[$cacheKey])) {
            $this->cache[$cacheKey] = $this->handler->handle($query);
        }

        return $this->cache[$cacheKey];
    }

    private function getCacheKey(Query $query): string
    {
        return $query->getName() . '_' . json_encode($query);
    }
}
