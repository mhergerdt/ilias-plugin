<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Query;


/**
 * @psalm-immutable
 */
class GetLocalRolesForTreeItem implements Query
{
    private int $refId;

    public function __construct(int $refId)
    {
        $this->refId = $refId;
    }

    public static function getName(): string
    {
        return 'GetLocalRolesForTreeItem';
    }

    public function getRefId(): int
    {
        return $this->refId;
    }
}
