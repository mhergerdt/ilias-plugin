<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Query;


/**
 * @psalm-immutable
 */
class GetUserByEmailShaHash implements Query
{
    private string $emailHash;
    private int $shaType;

    private function __construct(string $emailHash, int $shaType)
    {
        $this->emailHash = $emailHash;
        $this->shaType = $shaType;
    }

    public static function sha256(string $emailHash): self
    {
        return new self($emailHash, 256);
    }

    public static function getName(): string
    {
        return 'GetUserByEmailHash';
    }

    public function getEmailHash(): string
    {
        return $this->emailHash;
    }

    public function getShaType(): int
    {
        return $this->shaType;
    }
}
