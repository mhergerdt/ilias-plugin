<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Query;

use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ParticipantRole;

/**
 * @psalm-immutable
 */
class GetLocalRoleByParticipantRole implements Query
{
    private int $objRefId;
    private ParticipantRole $participantRole;

    public function __construct(int $objRefId, ParticipantRole $participantRole)
    {
        $this->objRefId = $objRefId;
        $this->participantRole = $participantRole;
    }

    public static function getName(): string
    {
        return 'GetLocalRoleByParticipantRole';
    }

    public function getObjRefId(): int
    {
        return $this->objRefId;
    }

    public function getParticipantRole(): ParticipantRole
    {
        return $this->participantRole;
    }
}
