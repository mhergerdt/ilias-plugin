<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\ValueObjects;

use DRVBund\Plugins\CGAutomation\Shared\ValueObject\ValueObject;
use ilParticipants;
use InvalidArgumentException;

/**
 * @psalm-immutable
 * @template-extends ValueObject<ParticipantRole>
 */
final class ParticipantRole extends ValueObject
{
    public const TYPE_COURSE = 'course';
    public const TYPE_GROUP = 'group';

    private int $value;
    private string $type;
    private string $rolePrefix;

    public static function fromJson($data): self
    {
        if (!is_array($data) || !isset($data['value'], $data['type'])) {
            throw new InvalidArgumentException('Invalid json data');
        }

        return new self($data['value'], $data['type']);
    }

    private function __construct(int $value, string $type)
    {
        $this->value = $value;
        $this->type = $type;
        $this->rolePrefix = $this->getMapping()[$type][$value];
    }

    public static function CourseAdmin(): self
    {
        return new self(ilParticipants::IL_CRS_ADMIN, self::TYPE_COURSE);
    }

    public static function CourseTutor(): self
    {
        return new self(ilParticipants::IL_CRS_TUTOR, self::TYPE_COURSE);
    }

    public static function CourseMember(): self
    {
        return new self(ilParticipants::IL_CRS_MEMBER, self::TYPE_COURSE);
    }

    public static function GroupAdmin(): self
    {
        return new self(ilParticipants::IL_GRP_ADMIN, self::TYPE_GROUP);
    }

    public static function GroupMember(): self
    {
        return new self(ilParticipants::IL_GRP_MEMBER, self::TYPE_GROUP);
    }

    public static function fromValue(int $role): self
    {
        if (in_array($role, [ilParticipants::IL_CRS_ADMIN, ilParticipants::IL_CRS_TUTOR, ilParticipants::IL_CRS_MEMBER])) {
            return new self($role, self::TYPE_COURSE);
        }

        if (in_array($role, [ilParticipants::IL_GRP_ADMIN, ilParticipants::IL_GRP_MEMBER])) {
            return new self($role, self::TYPE_GROUP);
        }

        throw new InvalidArgumentException('Invalid role!');
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function getRolePrefix(): string
    {
        return $this->rolePrefix;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function jsonSerialize(): array
    {
        return [
            'value' => $this->value,
            'type' => $this->type,
        ];
    }

    protected function isEqual($other): bool
    {
        return $this->value === $other->value && $this->type === $other->type;
    }

    private function getMapping(): array
    {
        return [
            self::TYPE_COURSE => [
                ilParticipants::IL_CRS_MEMBER => 'il_crs_member',
                ilParticipants::IL_CRS_TUTOR => 'il_crs_tutor',
                ilParticipants::IL_CRS_ADMIN => 'il_crs_admin',
            ],
            self::TYPE_GROUP => [
                ilParticipants::IL_GRP_MEMBER => 'il_grp_member',
                ilParticipants::IL_GRP_ADMIN => 'il_grp_admin',
            ],
        ];
    }
}
