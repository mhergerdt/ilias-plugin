<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\ValueObjects;

use DRVBund\Plugins\CGAutomation\Shared\ValueObject\ValueObject;

/**
 * @psalm-immutable
 * @template-extends ValueObject<GroupParticipantRole>
 */
final class GroupParticipantRole extends ValueObject
{
    private ParticipantRole $participantRole;

    public static function fromJson($data): self
    {
        if (!is_array($data)) {
            throw new \InvalidArgumentException('Invalid json data');
        }

        return new self(ParticipantRole::fromJson($data));
    }

    private function __construct(ParticipantRole $participantRole)
    {
        if ($participantRole->getType() !== ParticipantRole::TYPE_GROUP) {
            throw new \InvalidArgumentException('Invalid role type!');
        }

        $this->participantRole = $participantRole;
    }

    public static function fromValue(int $role): self
    {
        return new self(ParticipantRole::fromValue($role));
    }

    public static function Admin(): self
    {
        return new self(ParticipantRole::GroupAdmin());
    }

    public static function Member(): self
    {
        return new self(ParticipantRole::GroupMember());
    }

    public function getParticipantRole(): ParticipantRole
    {
        return $this->participantRole;
    }

    public function getRolePrefix(): string
    {
        return $this->participantRole->getRolePrefix();
    }

    public function getValue(): int
    {
        return $this->participantRole->getValue();
    }

    public function getType(): string
    {
        return $this->participantRole->getType();
    }

    public function jsonSerialize(): array
    {
        return $this->participantRole->jsonSerialize();
    }

    protected function isEqual($other): bool
    {
        return $this->participantRole->equals($other->participantRole);
    }
}
