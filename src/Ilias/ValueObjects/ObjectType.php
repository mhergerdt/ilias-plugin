<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\ValueObjects;

use DRVBund\Plugins\CGAutomation\Shared\ValueObject\ValueObject;
use Exception;

/**
 * @psalm-immutable
 * @template-extends ValueObject<ObjectType>
 */
class ObjectType extends ValueObject
{
    private const CATEGORY = 'cat';
    private const COURSE = 'crs';
    private const GROUP = 'grp';
    private const ROLE_TEMPLATE = 'rolt';
    private const ROLE = 'role';
    private const HTML_LEARNING_MODULE = 'htlm';

    /**
     * @var string[]
     */
    public const TYPES = [
        self::CATEGORY,
        self::COURSE,
        self::GROUP,
        self::ROLE_TEMPLATE,
        self::ROLE,
        self::HTML_LEARNING_MODULE,
    ];
    private string $type;

    public static function fromJson($data): self
    {
        if (!is_string($data)) {
            throw new \InvalidArgumentException('Invalid json data');
        }

        return new self($data);
    }

    public function __construct(string $type)
    {
        if (!in_array($type, self::TYPES)) {
            throw new Exception(sprintf('Unsupported type "%s"', $type));
        }

        $this->type = $type;
    }

    public static function CATEGORY(): self
    {
        return new self(self::CATEGORY);
    }

    public static function COURSE(): self
    {
        return new self(self::COURSE);
    }

    public static function GROUP(): self
    {
        return new self(self::GROUP);
    }

    public static function ROLE_TEMPLATE(): self
    {
        return new self(self::ROLE_TEMPLATE);
    }

    public static function ROLE(): self
    {
        return new self(self::ROLE);
    }

    public static function HTML_LEARNING_MODULE(): self
    {
        return new self(self::HTML_LEARNING_MODULE);
    }

    public function jsonSerialize(): string
    {
        return $this->type;
    }

    public function __toString(): string
    {
        return $this->type;
    }

    protected function isEqual($other): bool
    {
        return $this->type === $other->type;
    }
}
