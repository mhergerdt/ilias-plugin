<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Adapter;

use DateTimeImmutable;
use ILIAS\Data\DataSize;
use ILIAS\Filesystem\Filesystem;
use ILIAS\Filesystem\Finder\Finder;
use ILIAS\Filesystem\Stream\FileStream;
use ILIAS\Filesystem\Visibility;

class FilesystemAdapter implements Filesystem
{
    private Filesystem $filesystem;

    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function hasDir(string $path): bool
    {
        return $this->filesystem->hasDir($path);
    }

    public function listContents(string $path = '', bool $recursive = false): array
    {
        return $this->filesystem->listContents($path, $recursive);
    }

    /**
     * @param string $visibility Either 'public' or 'private'
     */
    public function createDir(string $path, string $visibility = Visibility::PUBLIC_ACCESS): void
    {
        $this->filesystem->createDir($path, $visibility);
    }

    public function copyDir(string $source, string $destination): void
    {
        $this->filesystem->copyDir($source, $destination);
    }

    public function deleteDir(string $path): void
    {
        $this->filesystem->deleteDir($path);
    }

    public function read(string $path): string
    {
        return $this->filesystem->read($path);
    }

    public function has(string $path): bool
    {
        return $this->filesystem->has($path);
    }

    public function getMimeType(string $path): string
    {
        return $this->filesystem->getMimeType($path);
    }

    public function getTimestamp(string $path): DateTimeImmutable
    {
        return $this->filesystem->getTimestamp($path);
    }

    public function getSize(string $path, int $fileSizeUnit): DataSize
    {
        return $this->filesystem->getSize($path, $fileSizeUnit);
    }

    public function setVisibility(string $path, string $visibility): bool
    {
        return $this->filesystem->setVisibility($path, $visibility);
    }

    public function getVisibility(string $path): string
    {
        return $this->filesystem->getVisibility($path);
    }

    public function readStream(string $path): FileStream
    {
        return $this->filesystem->readStream($path);
    }

    public function writeStream(string $path, FileStream $stream): void
    {
        $this->filesystem->writeStream($path, $stream);
    }

    public function putStream(string $path, FileStream $stream): void
    {
        $this->filesystem->putStream($path, $stream);
    }

    public function updateStream(string $path, FileStream $stream): void
    {
        $this->filesystem->updateStream($path, $stream);
    }

    public function write(string $path, string $content): void
    {
        $this->filesystem->write($path, $content);
    }

    public function update(string $path, string $new_content): void
    {
        $this->filesystem->update($path, $new_content);
    }

    public function put(string $path, string $content): void
    {
        $this->filesystem->put($path, $content);
    }

    public function delete(string $path): void
    {
        $this->filesystem->delete($path);
    }

    public function readAndDelete(string $path): string
    {
        return $this->filesystem->readAndDelete($path);
    }

    public function rename(string $path, string $new_path): void
    {
        $this->filesystem->rename($path, $new_path);
    }

    public function copy(string $path, string $copy_path): void
    {
        $this->filesystem->copy($path, $copy_path);
    }

    public function finder(): Finder
    {
        return $this->filesystem->finder();
    }
}
