<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Adapter;

use DateTimeInterface;
use ilDate;
use ilDateTime;

class LegacyDateTimeAdapter
{
    private DateTimeInterface $dateTime;

    public function __construct(DateTimeInterface $dateTime)
    {
        $this->dateTime = $dateTime;
    }

    public function toIliasDate(): ilDate
    {
        return new ilDate($this->dateTime->getTimestamp(), IL_CAL_UNIX);
    }

    public function toIliasDateTime(): ilDateTime
    {
        return new ilDateTime($this->dateTime->getTimestamp(), IL_CAL_UNIX);
    }

    /**
     * @return ilDate|ilDateTime
     */
    public function toIliasDateOrDateTime()
    {
        if ($this->dateTime->format('H:i:s') === '00:00:00') {
            return $this->toIliasDate();
        }

        return $this->toIliasDateTime();
    }
}
