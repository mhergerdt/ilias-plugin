<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Adapter;

use DateTimeInterface;
use ilDate;
use ilDateTime;

class NullableLegacyDateTimeAdapter
{
    private ?LegacyDateTimeAdapter $adapter;

    public function __construct(?DateTimeInterface $dateTime)
    {
        $this->adapter = $dateTime ? new LegacyDateTimeAdapter($dateTime) : null;
    }

    public function toIliasDate(): ?ilDate
    {
        return $this->adapter ? $this->adapter->toIliasDate() : null;
    }

    public function toIliasDateTime(): ?ilDateTime
    {
        return $this->adapter ? $this->adapter->toIliasDateTime() : null;
    }

    /**
     * @return ilDate|ilDateTime|null
     */
    public function toIliasDateOrDateTime()
    {
        return $this->adapter ? $this->adapter->toIliasDateOrDateTime() : null;
    }
}
