<?php

namespace DRVBund\Plugins\CGAutomation\Ilias;

/**
 * @template TValue
 * @psalm-immutable
 */
class CustomMetadataItem
{
    private string $type;
    /**
     * @var TValue
     */
    private $value;

    /**
     * @template T
     * @param T $value
     * @return CustomMetadataItem<T>
     */
    public static function typeString($value): self
    {
        $self = new self();
        $self->type = 'string';
        $self->value = $value;
        return $self;
    }

    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return TValue
     */
    public function getValue()
    {
        return $this->value;
    }
}
