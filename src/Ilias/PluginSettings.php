<?php

namespace DRVBund\Plugins\CGAutomation\Ilias;

use DRVBund\Plugins\CGAutomation\Shared\PluginConstants;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\DirectoryPath;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\FilePath;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingForm;
use Exception;
use Generator;
use ilSetting;

class PluginSettings
{
    private ilSetting $settings;

    public function __construct()
    {
        $this->settings = new ilSetting(PluginConstants::PLUGIN_ID);
    }

    public function zipFileName(): PluginSettingItem
    {
        return new PluginSettingItem($this->settings, 'zip_file_name');
    }

    public function objIdDataSrc(): PluginSettingItem
    {
        return new PluginSettingItem($this->settings, 'obj_id_data_src');
    }

    public function refIdTargetCategory(): PluginSettingItem
    {
        return new PluginSettingItem($this->settings, 'ref_id_target_category');
    }

    public function objIdCatalogueProd(): PluginSettingItem
    {
        return new PluginSettingItem($this->settings, 'obj_id_catalogue_prod');
    }

    public function objIdCatalogueStage(): PluginSettingItem
    {
        return new PluginSettingItem($this->settings, 'obj_id_catalogue_stage');
    }

    public function objIdCatalogueDataBund(): PluginSettingItem
    {
        return new PluginSettingItem($this->settings, 'obj_id_catalogue_data_bund');
    }

    public function objIdCatalogueDataTraeger(): PluginSettingItem
    {
        return new PluginSettingItem($this->settings, 'obj_id_catalogue_data_traeger');
    }

    public function courseTutorRoleTemplateId(): PluginSettingItem
    {
        return new PluginSettingItem($this->settings, 'id_course_tutor_role_template');
    }

    public function courseMemberRoleTemplateId(): PluginSettingItem
    {
        return new PluginSettingItem($this->settings, 'id_course_member_role_template');
    }

    public function groupAdminRoleTemplateId(): PluginSettingItem
    {
        return new PluginSettingItem($this->settings, 'id_group_admin_role_template');
    }

    public function groupMemberRoleTemplateId(): PluginSettingItem
    {
        return new PluginSettingItem($this->settings, 'id_group_member_role_template');
    }

    public function refIdDefaultCourseTemplate(): PluginSettingItem
    {
        return new PluginSettingItem($this->settings, 'ref_id_default_course_template');
    }

    public function refIdDefaultGroupTemplate(): PluginSettingItem
    {
        return new PluginSettingItem($this->settings, 'ref_id_default_group_template');
    }

    /**
     * @return PluginSettingItem[]
     */
    public function courseTemplateRefIds(): array
    {
        $items = [];

        foreach ($this->mappedTrainingForms() as $key => $value) {
            $items[$key] = new PluginSettingItem($this->settings, sprintf('id_crs_template_%s', $value));
        }

        return $items;
    }

    /**
     * @return PluginSettingItem[]
     */
    public function groupTemplateRefIds(): array
    {
        $items = [];

        foreach ($this->mappedTrainingForms() as $key => $value) {
            $items[$key] = new PluginSettingItem($this->settings, sprintf('id_grp_template_%s', $value));
        }

        return $items;
    }

    public function getDataSrcDirectory(): DirectoryPath
    {
        $objIdDataSrc = $this->objIdDataSrc()->getValue();

        if (null === $objIdDataSrc || '' === $objIdDataSrc) {
            throw new Exception('Obj id of data source learning module not defined');
        }

        return DirectoryPath::joinPartsWithDefaultSeparator('lm_data', 'lm_' . $objIdDataSrc);
    }

    public function getCatalogueProdDirectory(): DirectoryPath
    {
        $objIdCatalogueProd = $this->objIdCatalogueProd()->getValue();

        if (null === $objIdCatalogueProd || '' === $objIdCatalogueProd) {
            throw new Exception('Obj id of catalogue prod learning module not defined');
        }

        return DirectoryPath::joinPartsWithDefaultSeparator('lm_data', 'lm_' . $objIdCatalogueProd);
    }

    public function getCatalogueStageDirectory(): DirectoryPath
    {
        $objIdCatalogueStage = $this->objIdCatalogueStage()->getValue();

        if (null === $objIdCatalogueStage || '' === $objIdCatalogueStage) {
            throw new Exception('Obj id of catalogue stage learning module not defined');
        }

        return DirectoryPath::joinPartsWithDefaultSeparator('lm_data', 'lm_' . $objIdCatalogueStage);
    }

    public function getCatalogueDataBundDirectory(): DirectoryPath
    {
        $objIdCatalogueDataBund = $this->objIdCatalogueDataBund()->getValue();

        if (null === $objIdCatalogueDataBund || '' === $objIdCatalogueDataBund) {
            throw new Exception('Obj id of catalogue data bund learning module not defined');
        }

        return DirectoryPath::joinPartsWithDefaultSeparator('lm_data', 'lm_' . $objIdCatalogueDataBund);
    }

    public function getCatalogueDataTraegerDirectory(): DirectoryPath
    {
        $objIdCatalogueDataTraeger = $this->objIdCatalogueDataTraeger()->getValue();

        if (null === $objIdCatalogueDataTraeger || '' === $objIdCatalogueDataTraeger) {
            throw new Exception('Obj id of catalogue data traeger learning module not defined');
        }

        return DirectoryPath::joinPartsWithDefaultSeparator('lm_data', 'lm_' . $objIdCatalogueDataTraeger);
    }


    public function getBookingCodeMappingFilePath(): FilePath
    {
        return $this->getDataSrcDirectory()->appendFileName('booking_code_mapping.json');
    }

    public function getDataArchivePath(): FilePath
    {
        $zipFileName = $this->zipFileName()->getValue();

        if (null === $zipFileName || '' === $zipFileName) {
            throw new Exception('Zip file name not defined');
        }

        return $this->getDataSrcDirectory()->appendFileName($zipFileName);
    }

    public function ensureRequiredConfig(): void
    {
        if (!$this->zipFileName()->isSet()) {
            throw new Exception('Zip file name not defined');
        }

        if (!$this->objIdDataSrc()->isSet()) {
            throw new Exception('Obj id of data source learning module not defined');
        }

        if (!$this->refIdTargetCategory()->isSet()) {
            throw new Exception('Ref id of target category not defined');
        }

        if (!$this->objIdCatalogueProd()->isSet()) {
            throw new Exception('Obj id of catalogue prod not defined');
        }

        if (!$this->objIdCatalogueStage()->isSet()) {
            throw new Exception('Obj id of catalogue stage not defined');
        }

        if (!$this->objIdCatalogueDataBund()->isSet()) {
            throw new Exception('Obj id of catalogue data bund not defined');
        }

        if (!$this->objIdCatalogueDataTraeger()->isSet()) {
            throw new Exception('Obj id of catalogue data traeger not defined');
        }

        if (!$this->courseTutorRoleTemplateId()->isSet()) {
            throw new Exception('Id of course tutor role template not defined');
        }

        if (!$this->courseMemberRoleTemplateId()->isSet()) {
            throw new Exception('Id of course member role template not defined');
        }

        if (!$this->groupAdminRoleTemplateId()->isSet()) {
            throw new Exception('Id of group admin role template not defined');
        }

        if (!$this->groupMemberRoleTemplateId()->isSet()) {
            throw new Exception('Id of group member role template not defined');
        }
    }

    private function mappedTrainingForms(): Generator
    {
        $mapping = [
            (string)TrainingForm::Presence() => 'presence',
            (string)TrainingForm::Wbt() => 'wbt',
        ];

        foreach (TrainingForm::ALLOWED_VALUES as $value) {
            if (!array_key_exists($value, $mapping)) {
                continue;
            }

            yield $value => $mapping[$value];
        }
    }
}
