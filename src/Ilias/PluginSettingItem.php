<?php

namespace DRVBund\Plugins\CGAutomation\Ilias;

use ilSetting;

class PluginSettingItem
{
    private ilSetting $settings;
    private string $key;

    public function __construct(ilSetting $settings, string $key)
    {
        $this->settings = $settings;
        $this->key = $key;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getValue(): ?string
    {
        return $this->settings->get($this->key);
    }

    public function isSet(): bool
    {
        $value = $this->getValue();

        return null !== $value && '' !== $value;
    }

    public function getValueAsString(): string
    {
        return $this->getValueOrFail();
    }

    public function getValueAsInt(): int
    {
        return intval($this->getValueOrFail());
    }

    public function getValueAsIntOrNull(): ?int
    {
        $value = $this->getValue();

        if (!$this->isSet()) {
            return null;
        }

        return intval($value);
    }

    public function setValue(?string $value): void
    {
        $this->settings->set($this->key, $value ?? '');
    }

    private function getValueOrFail(): string
    {
        $value = $this->settings->get($this->key);

        if (null === $value) {
            throw new \RuntimeException('Setting ' . $this->key . ' not defined');
        }

        return $value;
    }
}
