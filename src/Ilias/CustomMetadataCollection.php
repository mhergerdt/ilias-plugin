<?php

namespace DRVBund\Plugins\CGAutomation\Ilias;

use InvalidArgumentException;

class CustomMetadataCollection
{
    private const LOCALIZED_TEXT_MAX_LENGTH = 4000;

    /**
     * @var array<string, CustomMetadataItem>
     */
    private array $items;

    public function __construct()
    {
        $this->items = [];
    }

    public function addStringItem(string $key, string $value): self
    {
        if (isset($this->items[$key])) {
            throw new \RuntimeException('Item already exists');
        }

        if (mb_strlen($value) > self::LOCALIZED_TEXT_MAX_LENGTH) {
            throw new InvalidArgumentException(sprintf('Max string length of %s exceeded.', self::LOCALIZED_TEXT_MAX_LENGTH));
        }

        $this->items[$key] = CustomMetadataItem::typeString($value);

        return $this;
    }

    /**
     * @return array<string, CustomMetadataItem>
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
