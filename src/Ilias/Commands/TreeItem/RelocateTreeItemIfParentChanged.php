<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;

/**
 * @psalm-immutable
 */
class RelocateTreeItemIfParentChanged implements Command
{
    private int $expectedParentRefId;
    private int $objRefId;

    public function __construct(int $expectedParentRefId, int $objRefId)
    {
        $this->expectedParentRefId = $expectedParentRefId;
        $this->objRefId = $objRefId;
    }

    public static function getName(): string
    {
        return 'RelocateTreeItemIfParentChanged';
    }

    public function getExpectedParentRefId(): int
    {
        return $this->expectedParentRefId;
    }

    public function getObjRefId(): int
    {
        return $this->objRefId;
    }
}
