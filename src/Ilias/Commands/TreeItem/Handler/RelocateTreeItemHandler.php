<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem\RelocateTreeItem;
use ilChangeEvent;
use ilConditionHandler;
use ilObjectDataCache;
use ilObjUser;
use ilRbacAdmin;
use ilTree;

/**
 * @psalm-api
 * @template-implements CommandHandler<RelocateTreeItem>
 */
class RelocateTreeItemHandler implements CommandHandler
{
    private ilTree $tree;
    private ilRbacAdmin $rbacAdmin;
    private ilObjectDataCache $objectDataCache;
    private ilObjUser $user;

    /**
     * @param ilTree $tree
     * @param ilRbacAdmin $rbacAdmin
     * @param ilObjectDataCache $objectDataCache
     * @param ilObjUser $user
     */
    public function __construct(ilTree $tree, ilRbacAdmin $rbacAdmin, ilObjectDataCache $objectDataCache, ilObjUser $user)
    {
        $this->tree = $tree;
        $this->rbacAdmin = $rbacAdmin;
        $this->objectDataCache = $objectDataCache;
        $this->user = $user;
    }

    public function handles(): string
    {
        return RelocateTreeItem::getName();
    }

    /**
     * @psalm-suppress UnusedVariable
     */
    public function handle(Command $command): void
    {
        $refId = $command->getRefId();
        $targetContainerRefId = $command->getTargetContainerRefId();

        // Store old parent
        $old_parent = $this->tree->getParentId($refId);
        $this->tree->moveTree($refId, $targetContainerRefId);
        $this->rbacAdmin->adjustMovedObjectPermissions($refId, $old_parent);

        ilConditionHandler::_adjustMovedObjectConditions($refId);

        // BEGIN ChangeEvent: Record cut event.
        $node_data = $this->tree->getNodeData($refId);
        $old_parent_data = $this->tree->getNodeData($old_parent);
        ilChangeEvent::_recordWriteEvent(
            $node_data['obj_id'],
            $this->user->getId(),
            'remove',
            $old_parent_data['obj_id']
        );
        ilChangeEvent::_recordWriteEvent(
            $node_data['obj_id'],
            $this->user->getId(),
            'add',
            $this->objectDataCache->lookupObjId($targetContainerRefId)
        );
        ilChangeEvent::_catchupWriteEvents($node_data['obj_id'], $this->user->getId());
    }
}
