<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem\RelocateTreeItem;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem\RelocateTreeItemChildren;
use ilTree;

/**
 * @psalm-api
 * @template-implements CommandHandler<RelocateTreeItemChildren>
 */
class RelocateTreeItemChildrenHandler implements CommandHandler
{
    private ilTree $tree;
    private RelocateTreeItemHandler $relocateTreeItemHandler;

    public function __construct(ilTree $tree, RelocateTreeItemHandler $relocateTreeItemHandler)
    {
        $this->tree = $tree;
        $this->relocateTreeItemHandler = $relocateTreeItemHandler;
    }

    public function handles(): string
    {
        return RelocateTreeItemChildren::getName();
    }

    public function handle(Command $command): void
    {
        $parentRefId = $command->getParentRefId();
        $targetRefId = $command->getTargetRefId();

        $childRefIds = $this->tree->getChildIds($parentRefId);

        foreach ($childRefIds as $childRefId) {
            $this->relocateTreeItemHandler->handle(new RelocateTreeItem($childRefId, $targetRefId));
        }
    }
}
