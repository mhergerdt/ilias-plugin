<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem\RelocateTreeItem;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem\RelocateTreeItemIfParentChanged;
use ilTree;

/**
 * @psalm-api
 * @template-implements CommandHandler<RelocateTreeItemIfParentChanged>
 */
class RelocateTreeItemIfParentChangedHandler implements CommandHandler
{
    private ilTree $tree;
    private RelocateTreeItemHandler $relocateTreeItemHandler;

    public function __construct(ilTree $tree, RelocateTreeItemHandler $relocateTreeItemHandler)
    {
        $this->tree = $tree;
        $this->relocateTreeItemHandler = $relocateTreeItemHandler;
    }

    public function handles(): string
    {
        return RelocateTreeItemIfParentChanged::getName();
    }

    public function handle(Command $command): void
    {
        $objectRefId = $command->getObjRefId();
        $expectedParentRefId = $command->getExpectedParentRefId();

        if (\ilObject::_isInTrash($objectRefId)) {
//            throw new Exception('Object is in trash');
            return;
        }

        $parentRefId = $this->tree->getParentId($objectRefId);
        if ($parentRefId !== $expectedParentRefId) {
            $this->relocateTreeItemHandler->handle(new RelocateTreeItem($objectRefId, $expectedParentRefId));
        }
    }
}
