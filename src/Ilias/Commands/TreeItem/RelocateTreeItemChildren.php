<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;

/**
 * @psalm-immutable
 */
class RelocateTreeItemChildren implements Command
{
    private int $parentRefId;
    private int $targetRefId;

    public function __construct(int $parentRefId, int $targetRefId)
    {
        $this->parentRefId = $parentRefId;
        $this->targetRefId = $targetRefId;
    }

    public static function getName(): string
    {
        return 'RelocateChildren';

    }

    public function getParentRefId(): int
    {
        return $this->parentRefId;
    }

    public function getTargetRefId(): int
    {
        return $this->targetRefId;
    }
}
