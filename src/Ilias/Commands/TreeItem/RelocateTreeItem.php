<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;

/**
 * @psalm-immutable
 */
class RelocateTreeItem implements Command
{
    private int $refId;
    private int $targetContainerRefId;

    public function __construct(int $refId, int $targetContainerRefId)
    {
        $this->refId = $refId;
        $this->targetContainerRefId = $targetContainerRefId;
    }

    public static function getName(): string
    {
        return 'RelocateTreeItem';
    }

    public function getRefId(): int
    {
        return $this->refId;
    }

    public function getTargetContainerRefId(): int
    {
        return $this->targetContainerRefId;
    }
}
