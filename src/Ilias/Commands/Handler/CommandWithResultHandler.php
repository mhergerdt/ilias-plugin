<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;

/**
 * @psalm-api
 * @template TCommand as CommandWithResult
 * @template TCommandResult
 */
interface CommandWithResultHandler
{
    /**
     * @return string
     */
    public function handles(): string;

    /**
     * @psalm-param TCommand $command
     * @return TCommandResult
     */
    public function handle(CommandWithResult $command);
}
