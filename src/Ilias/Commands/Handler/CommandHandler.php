<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;

/**
 * @psalm-api
 * @template TCommand as Command
 */
interface CommandHandler
{
    /**
     * @return string
     */
    public function handles(): string;

    /**
     * @psalm-param TCommand $command
     */
    public function handle(Command $command): void;
}
