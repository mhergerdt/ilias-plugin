<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Participant;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\GroupParticipantRole;

/**
 * @psalm-immutable
 */
class AddGroupParticipant implements Command
{
    private int $groupId;
    private int $userId;
    private GroupParticipantRole $participantRole;

    public function __construct(int $groupId, int $userId, GroupParticipantRole $participantRole)
    {
        $this->groupId = $groupId;
        $this->userId = $userId;
        $this->participantRole = $participantRole;
    }

    public static function getName(): string
    {
        return 'AddGroupParticipant';
    }

    public function getGroupId(): int
    {
        return $this->groupId;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getParticipantRole(): GroupParticipantRole
    {
        return $this->participantRole;
    }
}
