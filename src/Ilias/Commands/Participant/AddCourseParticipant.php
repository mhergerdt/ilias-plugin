<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Participant;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\CourseParticipantRole;

/**
 * @psalm-immutable
 */
class AddCourseParticipant implements Command
{
    private int $courseId;
    private int $userId;
    private CourseParticipantRole $participantRole;

    public function __construct(int $courseId, int $userId, CourseParticipantRole $participantRole)
    {
        $this->courseId = $courseId;
        $this->userId = $userId;
        $this->participantRole = $participantRole;
    }

    public static function getName(): string
    {
        return 'AddCourseParticipant';
    }

    public function getCourseId(): int
    {
        return $this->courseId;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getParticipantRole(): CourseParticipantRole
    {
        return $this->participantRole;
    }
}
