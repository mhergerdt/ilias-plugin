<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Participant\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Participant\AddCourseParticipant;
use ilCourseParticipants;

/**
 * @psalm-api
 * @template-implements CommandHandler<AddCourseParticipant>
 */
class AddCourseParticipantHandler implements CommandHandler
{
    public function handles(): string
    {
        return AddCourseParticipant::getName();
    }

    public function handle(Command $command): void
    {
        $courseParticipants = new ilCourseParticipants($command->getCourseId());
        $courseParticipants->add($command->getUserId(), $command->getParticipantRole()->getValue());
    }
}
