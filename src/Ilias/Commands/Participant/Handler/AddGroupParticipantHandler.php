<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Participant\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Participant\AddGroupParticipant;
use ilGroupParticipants;

/**
 * @psalm-api
 * @template-implements CommandHandler<AddGroupParticipant>
 */
class AddGroupParticipantHandler implements CommandHandler
{
    public function handles(): string
    {
        return AddGroupParticipant::getName();
    }

    public function handle(Command $command): void
    {
        $courseParticipants = new ilGroupParticipants($command->getGroupId());
        $courseParticipants->add($command->getUserId(), $command->getParticipantRole()->getValue());
    }
}
