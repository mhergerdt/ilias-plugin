<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Group;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\DTO\GroupDTO;

/**
 * @psalm-immutable
 */
class UpdateGroup implements GroupCommand
{
    private int $groupRefId;
    private GroupDTO $groupDTO;

    public function __construct(int $groupRefId, GroupDTO $groupDTO)
    {
        $this->groupRefId = $groupRefId;
        $this->groupDTO = $groupDTO;
    }

    public static function getName(): string
    {
        return 'UpdateGroup';
    }

    public function getGroupRefId(): int
    {
        return $this->groupRefId;
    }

    public function getGroupDTO(): GroupDTO
    {
        return $this->groupDTO;
    }
}
