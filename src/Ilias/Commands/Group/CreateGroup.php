<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Group;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;
use DRVBund\Plugins\CGAutomation\Ilias\DTO\GroupDTO;

/**
 * @psalm-immutable
 */
class CreateGroup implements GroupCommand
{
    private int $parentTreeItemRefId;
    private GroupDTO $groupDTO;

    public function __construct(int $parentTreeItemRefId, GroupDTO $groupDTO)
    {
        $this->parentTreeItemRefId = $parentTreeItemRefId;
        $this->groupDTO = $groupDTO;
    }

    public static function getName(): string
    {
        return 'CreateGroup';
    }

    public function getParentTreeItemRefId(): int
    {
        return $this->parentTreeItemRefId;
    }

    public function getGroupDTO(): GroupDTO
    {
        return $this->groupDTO;
    }
}
