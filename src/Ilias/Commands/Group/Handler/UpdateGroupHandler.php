<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Group\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Adapter\NullableLegacyDateTimeAdapter;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Group\GroupCommand;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Group\UpdateGroup;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Repository\ObjectRepository;
use DRVBund\Plugins\CGAutomation\Ilias\Service\ContainerSettingService;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;
use Exception;
use ilMembershipRegistrationCodeUtils;
use ilObjectServiceSettingsGUI;
use ilObjGroup;

/**
 * @psalm-api
 * @template-extends StoreGroupHandler<UpdateGroup>
 */
class UpdateGroupHandler extends StoreGroupHandler
{
    private ObjectRepository $objectRepository;

    public function __construct(
        ObjectRepository        $objectRepository,
        ContainerSettingService $containerSettingService
    )
    {
        parent::__construct($containerSettingService);

        $this->objectRepository = $objectRepository;
    }

    public function handles(): string
    {
        return UpdateGroup::getName();
    }

    protected function getGroup(GroupCommand $command): ilObjGroup
    {
        $groupRefId = $command->getGroupRefId();

        /** @var ilObjGroup|null $group */
        $group = $this->objectRepository->loadByRefId($groupRefId);

        if (null === $group || $group->getType() !== (string)ObjectType::GROUP()) {
            throw new Exception(sprintf('Group with ref id %s not found', $groupRefId));
        }

        return $group;
    }
}
