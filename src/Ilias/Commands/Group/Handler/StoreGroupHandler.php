<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Group\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Adapter\NullableLegacyDateTimeAdapter;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Group\GroupCommand;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandWithResultHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Service\ContainerSettingService;
use Exception;
use ilMembershipRegistrationCodeUtils;
use ilObjectServiceSettingsGUI;
use ilObjGroup;

/**
 * @psalm-api
 * @template TCommand of GroupCommand
 * @template-implements CommandWithResultHandler<TCommand, int>
 */
abstract class StoreGroupHandler implements CommandWithResultHandler
{
    private ContainerSettingService $containerSettingService;

    public function __construct(ContainerSettingService $containerSettingService)
    {
        $this->containerSettingService = $containerSettingService;
    }

    public function handle(CommandWithResult $command): int
    {
        $groupDTO = $command->getGroupDTO();

        $group = $this->getGroup($command);
        if (0 === $group->getRefId()) {
            throw new Exception('No ref id found for group');
        }

        $group->setTitle(mb_substr($groupDTO->getTitle(), 0, 128));
        $group->setDescription($groupDTO->getDescription());
        $group->setImportId($groupDTO->getImportId());
        $group->setShowMembers($groupDTO->getUseShowMembers());
        $group->setOfflineStatus($groupDTO->getOfflineStatus());
        $group->setAutoNotification($groupDTO->getUseAutoNotification());
        if ($groupDTO->getUseRegistrationAccessCode()) {
            $group->enableRegistrationAccessCode(true);
            $group->setRegistrationAccessCode(ilMembershipRegistrationCodeUtils::generateCode());
        }
        $group->setMailToMembersType($groupDTO->getMailToMembersType());
        $group->setPeriod(
            (new NullableLegacyDateTimeAdapter($groupDTO->getPeriodFrom()))->toIliasDateOrDateTime(),
            (new NullableLegacyDateTimeAdapter($groupDTO->getPeriodUntil()))->toIliasDateOrDateTime(),
        );
        $group->setNewsTimeline($groupDTO->getUseNewsTimeline());
        $group->setNewsBlockActivated($groupDTO->getNewsBlockActivated());
        $group->setRegistrationType($groupDTO->getRegistrationType());
        $group->setGroupType($groupDTO->getGroupType());
        $group->updateGroupType($groupDTO->getGroupType());

        $booleanContainerSettings = [
            ilObjectServiceSettingsGUI::INFO_TAB_VISIBILITY => $groupDTO->getUseInfoTabVisibility(),
            ilObjectServiceSettingsGUI::USE_NEWS => $groupDTO->getUseNews(),
            ilObjectServiceSettingsGUI::CUSTOM_METADATA => $groupDTO->getWithCustomMetadata(),
            ilObjectServiceSettingsGUI::AUTO_RATING_NEW_OBJECTS => $groupDTO->getWithAutoRatingNewObjects(),
            ilObjectServiceSettingsGUI::TAG_CLOUD => $groupDTO->getUseTagCloud(),
            ilObjectServiceSettingsGUI::NEWS_VISIBILITY => $groupDTO->getUseNewsVisibility(),
            ilObjectServiceSettingsGUI::BADGES => $groupDTO->getUseBadges(),
            ilObjectServiceSettingsGUI::BOOKING => $groupDTO->getUseBooking(),
            ilObjectServiceSettingsGUI::CALENDAR_ACTIVATION => $groupDTO->getUseCalendarActivation(),
            ilObjectServiceSettingsGUI::CALENDAR_VISIBILITY => $groupDTO->getUseCalendarVisibility(),
        ];

        foreach ($booleanContainerSettings as $key => $value) {
            $this->containerSettingService->setBooleanSetting($group->getId(), $key, $value);
        }

        $this->containerSettingService->setStringSetting(
            $group->getId(),
            ilObjectServiceSettingsGUI::EXTERNAL_MAIL_PREFIX,
            $groupDTO->getExternalMailPrefix()
        );

        $group->update();

        return $group->getId();
    }

    /**
     * @param TCommand $command
     */
    protected abstract function getGroup(GroupCommand $command): ilObjGroup;
}
