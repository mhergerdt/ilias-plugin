<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Group\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Group\CreateGroup;
use ilObjGroup;

/**
 * @psalm-api
 * @template-extends StoreGroupHandler<CreateGroup>
 */
class CreateGroupHandler extends StoreGroupHandler
{
    public function handles(): string
    {
        return CreateGroup::getName();
    }

    protected function getGroup(CommandWithResult $command): ilObjGroup
    {
        $title = $command->getGroupDTO()->getTitle();
        $parentTreeItemRefId = $command->getParentTreeItemRefId();

        $group = new ilObjGroup();
        $group->setTitle($title);
        $group->create();
        $group->createReference();
        $group->putInTree($parentTreeItemRefId);
        $group->setPermissions($parentTreeItemRefId);
        $group->update();

        return $group;
    }
}
