<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Group\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\CopyObject\CopyObject;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Group\CreateGroup;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Group\CreateGroupFromGroup;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandWithResultHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Dispatcher;
use DRVBund\Plugins\CGAutomation\Ilias\Repository\ObjectRepository;
use DRVBund\Plugins\CGAutomation\Ilias\Service\ContainerSettingService;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;
use Exception;
use ilObjGroup;

/**
 * @psalm-api
 * @template-extends StoreGroupHandler<CreateGroupFromGroup>
 */
class CreateGroupFromGroupHandler extends StoreGroupHandler
{
    private ObjectRepository $objectRepository;
    private Dispatcher $dispatcher;

    public function __construct(
        ObjectRepository        $objectRepository,
        ContainerSettingService $containerSettingService,
        Dispatcher              $dispatcher
    )
    {
        parent::__construct($containerSettingService);

        $this->objectRepository = $objectRepository;
        $this->dispatcher = $dispatcher;
    }

    public function handles(): string
    {
        return CreateGroupFromGroup::getName();
    }

    public function getGroup(CommandWithResult $command): ilObjGroup
    {
        $title = $command->getGroupDTO()->getTitle();
        $parentTreeItemRefId = $command->getParentTreeItemRefId();
        $copyGroupRefId = $command->getCopyGroupRefId();

        /** @var ?ilObjGroup $groupToCopy */
        $groupToCopy = $this->objectRepository->loadByRefId($copyGroupRefId);
        if (null === $groupToCopy || $groupToCopy->getType() !== (string)ObjectType::GROUP()) {
            throw new Exception('Can only copy another group');
        }

        /**
         * @var array{copy_id: int, ref_id: int} $result
         */
        $result = $this->dispatcher->dispatchCommandWithResult(
            new CopyObject($copyGroupRefId, $parentTreeItemRefId, $command->getExcludeTypes())
        );
        /** @var ilObjGroup $group */
        $group = $this->objectRepository->loadByRefId($result['ref_id']);
        $group->setTitle($title);
        $group->update();
        return $group;
    }
}
