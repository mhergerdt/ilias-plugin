<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Group;

use DRVBund\Plugins\CGAutomation\Ilias\DTO\GroupDTO;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;

/**
 * @psalm-immutable
 */
class CreateGroupFromGroup implements GroupCommand
{
    private int $parentTreeItemRefId;
    private GroupDTO $groupDTO;
    private int $copyGroupRefId;
    /**
     * @var ObjectType[]
     */
    private array $excludeTypes;

    public function __construct(int $parentTreeItemRefId, GroupDTO $groupDTO, int $copyGroupRefId, array $excludeTypes = [])
    {
        $this->parentTreeItemRefId = $parentTreeItemRefId;
        $this->groupDTO = $groupDTO;
        $this->copyGroupRefId = $copyGroupRefId;
        $this->excludeTypes = $excludeTypes;
    }

    public static function getName(): string
    {
        return 'CreateGroupFromGroup';
    }

    public function getParentTreeItemRefId(): int
    {
        return $this->parentTreeItemRefId;
    }

    public function getGroupDTO(): GroupDTO
    {
        return $this->groupDTO;
    }

    public function getCopyGroupRefId(): int
    {
        return $this->copyGroupRefId;
    }

    /**
     * @return ObjectType[]
     */
    public function getExcludeTypes(): array
    {
        return $this->excludeTypes;
    }
}
