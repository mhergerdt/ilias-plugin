<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Group;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;
use DRVBund\Plugins\CGAutomation\Ilias\DTO\GroupDTO;

interface GroupCommand extends CommandWithResult
{
    public function getGroupDTO(): GroupDTO;
}
