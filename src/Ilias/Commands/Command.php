<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands;

/**
 * @psalm-immutable
 */
interface Command
{
    public static function getName(): string;
}
