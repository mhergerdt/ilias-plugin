<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\CopyObject\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\CopyObject\CopyObject;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandWithResultHandler;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;
use ilContainer;
use ilCopyWizardOptions;
use ilObjectFactory;
use ilTree;
use InvalidArgumentException;

/**
 * @psalm-api
 * @template-implements CommandWithResultHandler<CopyObject, array{copy_id: int, ref_id: int}>
 */
class CopyObjectViaILIASAPIHandler implements CommandWithResultHandler
{
    private ilTree $tree;

    public function __construct(ilTree $tree)
    {
        $this->tree = $tree;
    }

    public function handles(): string
    {
        return CopyObject::getName();
    }

    public function handle(CommandWithResult $command)
    {
        $sourceRefId = $command->getSourceRefId();
        $targetRefId = $command->getTargetRefId();
        $excludedTypes = $command->getExcludedTypes();

        $object = ilObjectFactory::getInstanceByRefId($sourceRefId);

        if (!($object instanceof ilContainer)) {
            throw new InvalidArgumentException('Only ilContainer objects can be copied');
        }

        return $object->cloneAllObject(
            session_id(),
            CLIENT_ID,
            '', // Seems to be unused (checked in ilObjUser::_cloneAllObject)
            $targetRefId,
            $sourceRefId,
            $this->getOptions($sourceRefId, $excludedTypes),
            false, // Seems to be unused (checked in ilObjUser::_cloneAllObject)
            false
        );
    }

    /**
     * @param ObjectType[] $excludedTypes
     */
    private function getOptions(int $sourceRefId, array $excludedTypes): array
    {
        $excludedTypes = array_map(fn(ObjectType $type) => (string)$type, $excludedTypes);

        $excludedPaths = [];

        /**
         * @var array<int, array{ref_id: string, type: string, path: string}> $subTree
         */
        $subTree = $this->tree->getSubTree($this->tree->getNodeTreeData($sourceRefId));
        array_shift($subTree);

        $options = [];
        foreach ($subTree as $child) {
            $parentPath = rtrim(str_replace($child['ref_id'], '', $child['path']), '.');
            if (in_array($child['type'], $excludedTypes) || in_array($parentPath, $excludedPaths)) {
                $excludedPaths[] = $child['path'];
                continue;
            }

            $options[$child['ref_id']] = ['type' => ilCopyWizardOptions::COPY_WIZARD_COPY];
        }

        return $options;
    }
}
