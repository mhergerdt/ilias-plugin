<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\CopyObject;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;

/**
 * @psalm-immutable
 */
class CopyObject implements CommandWithResult
{
    private int $sourceRefId;
    private int $targetRefId;
    /**
     * @var ObjectType[]
     */
    private array $excludedTypes;

    /**
     * @param ObjectType[] $excludedTypes
     */
    public function __construct(int $sourceRefId, int $targetRefId, array $excludedTypes = [])
    {
        $this->sourceRefId = $sourceRefId;
        $this->targetRefId = $targetRefId;
        $this->excludedTypes = $excludedTypes;
    }

    public static function getName(): string
    {
        return 'CopyObject';
    }

    public function getSourceRefId(): int
    {
        return $this->sourceRefId;
    }

    public function getTargetRefId(): int
    {
        return $this->targetRefId;
    }

    /**
     * @return ObjectType[]
     */
    public function getExcludedTypes(): array
    {
        return $this->excludedTypes;
    }
}
