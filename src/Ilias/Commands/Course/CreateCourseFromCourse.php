<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Course;

use DRVBund\Plugins\CGAutomation\Ilias\DTO\CourseDTO;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;

/**
 * @psalm-immutable
 */
class CreateCourseFromCourse implements CourseCommand
{
    private int $parentTreeItemRefId;
    private CourseDTO $courseDTO;
    private int $copyCourseRefId;
    /**
     * @var ObjectType[]
     */
    private array $excludeTypes;

    public function __construct(int $parentTreeItemRefId, CourseDTO $courseDTO, int $copyCourseRefId, array $excludeTypes = [])
    {
        $this->parentTreeItemRefId = $parentTreeItemRefId;
        $this->copyCourseRefId = $copyCourseRefId;
        $this->excludeTypes = $excludeTypes;
        $this->courseDTO = $courseDTO;
    }

    public static function getName(): string
    {
        return 'CreateCourseFromCourse';
    }

    public function getParentTreeItemRefId(): int
    {
        return $this->parentTreeItemRefId;
    }

    public function getCopyCourseRefId(): int
    {
        return $this->copyCourseRefId;
    }

    /**
     * @return ObjectType[]
     */
    public function getExcludeTypes(): array
    {
        return $this->excludeTypes;
    }

    public function getCourseDTO(): CourseDTO
    {
        return $this->courseDTO;
    }
}
