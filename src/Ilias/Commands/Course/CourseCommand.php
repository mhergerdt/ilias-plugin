<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Course;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;
use DRVBund\Plugins\CGAutomation\Ilias\DTO\CourseDTO;

/**
 * @psalm-immutable
 */
interface CourseCommand extends CommandWithResult
{
    public function getCourseDTO(): CourseDTO;
}
