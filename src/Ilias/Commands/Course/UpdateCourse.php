<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Course;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\DTO\CourseDTO;

/**
 * @psalm-immutable
 */
class UpdateCourse implements CourseCommand
{
    private int $courseId;
    private CourseDTO $courseDTO;

    public function __construct(int $courseId, CourseDTO $courseDTO)
    {
        $this->courseId = $courseId;
        $this->courseDTO = $courseDTO;
    }

    public static function getName(): string
    {
        return 'UpdateCourse';
    }

    public function getCourseId(): int
    {
        return $this->courseId;
    }

    public function getCourseDTO(): CourseDTO
    {
        return $this->courseDTO;
    }
}
