<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Course;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;
use DRVBund\Plugins\CGAutomation\Ilias\DTO\CourseDTO;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;

/**
 * @psalm-immutable
 */
class CreateCourse implements CourseCommand
{
    private int $parentTreeItemRefId;
    private CourseDTO $courseDTO;

    public function __construct(int $parentTreeItemRefId, CourseDTO $courseDTO)
    {
        $this->parentTreeItemRefId = $parentTreeItemRefId;
        $this->courseDTO = $courseDTO;
    }

    public static function getName(): string
    {
        return 'CreateCourse';
    }

    public function getParentTreeItemRefId(): int
    {
        return $this->parentTreeItemRefId;
    }

    public function getCourseDTO(): CourseDTO
    {
        return $this->courseDTO;
    }
}
