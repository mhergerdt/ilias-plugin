<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Course\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Course\CourseCommand;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Course\CreateCourse;
use ilObjCourse;

/**
 * @psalm-api
 * @template-extends StoreCourseHandler<CreateCourse>
 */
class CreateCourseHandler extends StoreCourseHandler
{
    public function handles(): string
    {
        return CreateCourse::getName();
    }

    public function getCourse(CourseCommand $command): ilObjCourse
    {
        $title = $command->getCourseDTO()->getTitle();
        $parentTreeItemRefId = $command->getParentTreeItemRefId();

        $course = new ilObjCourse();
        $course->setTitle($title);
        $course->create();
        $course->createReference();
        $course->putInTree($parentTreeItemRefId);
        $course->setPermissions($parentTreeItemRefId);
        $course->update();

        return $course;
    }
}
