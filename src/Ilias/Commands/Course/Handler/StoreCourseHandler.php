<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Course\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Adapter\NullableLegacyDateTimeAdapter;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Course\CourseCommand;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandWithResultHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Service\ContainerSettingService;
use ilMembershipRegistrationCodeUtils;
use ilObjCourse;
use ilObjectServiceSettingsGUI;

/**
 * @psalm-api
 * @template TCommand of CourseCommand
 * @template-implements CommandWithResultHandler<TCommand, int>
 */
abstract class StoreCourseHandler implements CommandWithResultHandler
{
    private ContainerSettingService $containerSettingService;

    public function __construct(ContainerSettingService $containerSettingService)
    {
        $this->containerSettingService = $containerSettingService;
    }

    public function handle(CommandWithResult $command)
    {
        $courseDTO = $command->getCourseDTO();

        $course = $this->getCourse($command);

        $course->setTitle(mb_substr($courseDTO->getTitle(), 0, 128));
        $course->setDescription($courseDTO->getDescription());
        $course->setImportId($courseDTO->getImportId());
        $course->setShowMembers($courseDTO->getUseShowMembers());
        $course->setOfflineStatus($courseDTO->getOfflineStatus());
        $course->setAutoNotification($courseDTO->getUseAutoNotification());
        if ($courseDTO->getUseRegistrationAccessCode()) {
            $course->enableRegistrationAccessCode(true);
            $course->setRegistrationAccessCode(ilMembershipRegistrationCodeUtils::generateCode());
        }
        $course->setSubscriptionType($courseDTO->getSubscriptionType());
        $course->setTimingMode($courseDTO->getTimingMode());
        $course->setMailToMembersType($courseDTO->getMailToMembersType());
        $course->setCoursePeriod(
            (new NullableLegacyDateTimeAdapter($courseDTO->getPeriodFrom()))->toIliasDateOrDateTime(),
            (new NullableLegacyDateTimeAdapter($courseDTO->getPeriodUntil()))->toIliasDateOrDateTime(),
        );

        $booleanContainerSettings = [
            ilObjectServiceSettingsGUI::INFO_TAB_VISIBILITY => $courseDTO->getUseInfoTabVisibility(),
            ilObjectServiceSettingsGUI::USE_NEWS => $courseDTO->getUseNews(),
            ilObjectServiceSettingsGUI::CUSTOM_METADATA => $courseDTO->getWithCustomMetadata(),
            ilObjectServiceSettingsGUI::AUTO_RATING_NEW_OBJECTS => $courseDTO->getWithAutoRatingNewObjects(),
            ilObjectServiceSettingsGUI::TAG_CLOUD => $courseDTO->getUseTagCloud(),
            ilObjectServiceSettingsGUI::NEWS_VISIBILITY => $courseDTO->getUseNewsVisibility(),
            ilObjectServiceSettingsGUI::BADGES => $courseDTO->getUseBadges(),
            ilObjectServiceSettingsGUI::BOOKING => $courseDTO->getUseBooking(),
            ilObjectServiceSettingsGUI::CALENDAR_ACTIVATION => $courseDTO->getUseCalendarActivation(),
            ilObjectServiceSettingsGUI::CALENDAR_VISIBILITY => $courseDTO->getUseCalendarVisibility(),
        ];

        foreach ($booleanContainerSettings as $key => $value) {
            $this->containerSettingService->setBooleanSetting($course->getId(), $key, $value);
        }

        $this->containerSettingService->setStringSetting(
            $course->getId(),
            ilObjectServiceSettingsGUI::EXTERNAL_MAIL_PREFIX,
            $courseDTO->getExternalMailPrefix()
        );

        $course->setViewMode($courseDTO->getViewMode());

        $course->update();

        return $course->getId();
    }

    /**
     * @param TCommand $command
     */
    protected abstract function getCourse(CourseCommand $command): ilObjCourse;
}
