<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Course\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Course\CourseCommand;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Course\UpdateCourse;
use DRVBund\Plugins\CGAutomation\Ilias\Repository\ObjectRepository;
use DRVBund\Plugins\CGAutomation\Ilias\Service\ContainerSettingService;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;
use Exception;
use ilObjCourse;

/**
 * @psalm-api
 * @template-extends StoreCourseHandler<UpdateCourse>
 */
class UpdateCourseHandler extends StoreCourseHandler
{
    private ObjectRepository $objectRepository;

    public function __construct(ObjectRepository $objectRepository, ContainerSettingService $containerSettingService)
    {
        parent::__construct($containerSettingService);

        $this->objectRepository = $objectRepository;
    }

    public function handles(): string
    {
        return UpdateCourse::getName();
    }

    protected function getCourse(CourseCommand $command): ilObjCourse
    {
        $courseId = $command->getCourseId();

        /** @var ilObjCourse|null $course */
        $course = $this->objectRepository->loadByObjId($courseId, (string)ObjectType::COURSE());

        if (null === $course) {
            throw new Exception(sprintf('Course with id %s not found', $courseId));
        }

        return $course;
    }
}
