<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Course\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\CopyObject\CopyObject;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Course\CourseCommand;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Course\CreateCourseFromCourse;
use DRVBund\Plugins\CGAutomation\Ilias\Dispatcher;
use DRVBund\Plugins\CGAutomation\Ilias\Repository\ObjectRepository;
use DRVBund\Plugins\CGAutomation\Ilias\Service\ContainerSettingService;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;
use Exception;
use ilObjCourse;

/**
 * @psalm-api
 * @template-extends StoreCourseHandler<CreateCourseFromCourse>
 */
class CreateCourseFromCourseHandler extends StoreCourseHandler
{
    private ObjectRepository $objectRepository;
    private Dispatcher $dispatcher;

    public function __construct(
        ObjectRepository        $objectRepository,
        ContainerSettingService $containerSettingService,
        Dispatcher              $dispatcher
    )
    {
        parent::__construct($containerSettingService);

        $this->objectRepository = $objectRepository;
        $this->dispatcher = $dispatcher;
    }

    public function handles(): string
    {
        return CreateCourseFromCourse::getName();
    }

    public function getCourse(CourseCommand $command): ilObjCourse
    {
        $title = $command->getCourseDTO()->getTitle();
        $parentTreeItemRefId = $command->getParentTreeItemRefId();
        $copyCourseRefId = $command->getCopyCourseRefId();

        $courseToCopy = $this->objectRepository->loadByRefId($copyCourseRefId);
        if (null === $courseToCopy || $courseToCopy->getType() !== (string)ObjectType::COURSE()) {
            throw new Exception('Can only copy another course');
        }

        /**
         * @var array{copy_id: int, ref_id: int} $result
         */
        $result = $this->dispatcher->dispatchCommandWithResult(
            new CopyObject($copyCourseRefId, $parentTreeItemRefId, $command->getExcludeTypes())
        );
        /** @var ilObjCourse $course */
        $course = $this->objectRepository->loadByRefId($result['ref_id']);
        $course->setTitle($title);
        $course->update();
        return $course;
    }
}
