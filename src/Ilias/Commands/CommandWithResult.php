<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands;

/**
 * @psalm-immutable
 */
interface CommandWithResult
{
    public static function getName(): string;
}
