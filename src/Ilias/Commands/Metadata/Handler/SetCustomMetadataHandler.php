<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Metadata\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Metadata\SetCustomMetadata;
use DRVBund\Plugins\CGAutomation\Ilias\CustomMetadataCollection;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;
use Exception;
use ilADT;
use ilADTLocalizedText;
use ilADTLocalizedTextDefinition;
use ilAdvancedMDFieldDefinition;
use ilAdvancedMDFieldDefinitionText;
use ilAdvancedMDFieldTranslation;
use ilAdvancedMDFieldTranslations;
use ilAdvancedMDRecord;
use ilAdvancedMDRecordTranslations;
use ilAdvancedMDValues;
use ilContainer;
use ilECSCourseSettings;
use ilObject;
use ilObjectFactory;
use ilObjectServiceSettingsGUI;

/**
 * @psalm-api
 * @template-implements CommandHandler<SetCustomMetadata>
 */
class SetCustomMetadataHandler implements CommandHandler
{
    public function handles(): string
    {
        return SetCustomMetadata::getName();
    }

    /**
     * @psalm-suppress UnusedVariable
     */
    public function handle(Command $command): void
    {
        $objRefId = $command->getObjRefId();
        $title = $command->getTitle();
        $importId = $title;
        $customMetadataCollection = $command->getCustomMetadataCollection();

        $object = ilObjectFactory::getInstanceByRefId($objRefId, true);

        if (null === $object) {
            throw new Exception('No object found');
        }

        ilContainer::_writeContainerSetting($object->getId(), ilObjectServiceSettingsGUI::CUSTOM_METADATA, '1');

        $record = $this->getAdvancedMDRecordWithCustomMetadata($object->getId(), $object->getType(), $title, $importId);

        if (null === $record) {
            $record = $this->createAdvancedMDRecord($object->getId(), $object->getType(), $title, $importId);
        }

        $this->updateCustomMetadata($object, $record, $customMetadataCollection);
    }

    private function getAdvancedMDRecordWithCustomMetadata(int $objectId, string $objectType, string $title, string $importId): ?ilAdvancedMDRecord
    {
        $records = ilAdvancedMDRecord::_getSelectedRecordsByObject(
            $objectType,
            $objectId,
            '',
            false
        );

        foreach ($records as $record) {
            if ($record->getTitle() === $title && $record->getImportId() === $importId) {
                return $record;
            }
        }

        return null;
    }

    private function createAdvancedMDRecord(int $objId, string $objectType, string $title, ?string $importId = null): ilAdvancedMDRecord
    {
        $record = new ilAdvancedMDRecord();
        $record->setActive(true);
        $record->setTitle($title);
        $record->setImportId($importId ?? '');
        $record->setParentObject($objId);
        $record->setDefaultLanguage('en');
        $record->appendAssignedObjectType($objectType, '-', false);
        $record->save();

        $translations = ilAdvancedMDRecordTranslations::getInstanceByRecordId($record->getRecordId());
        $translations->addTranslationEntry($record->getDefaultLanguage(), true);
        $translations->updateTranslations(
            $record->getDefaultLanguage(),
            $record->getTitle(),
            $record->getDescription()
        );

        return $record;
    }

    private function updateCustomMetadata(ilObject $object, ilAdvancedMDRecord $record, CustomMetadataCollection $customMetadataCollection): void
    {
        $advancedMDValues = $this->loadAdvancedMDValues($record);
        $fieldDefinitions = $this->loadFieldDefinitions($advancedMDValues);

        foreach (array_keys($customMetadataCollection->getItems()) as $key) {
            if (!array_key_exists($key, $fieldDefinitions)) {
                $fieldDefinitions[$key] = $this->createTextFieldDefinition(
                    $record->getRecordId(),
                    $key
                );
            }
        }

        $advancedMDValues = $this->loadAdvancedMDValues($record);
        $fieldDefinitions = $this->loadFieldDefinitions($advancedMDValues);

        foreach ($customMetadataCollection->getItems() as $key => $item) {
            if (!array_key_exists($key, $fieldDefinitions)) {
                continue;
            }

            $this->setFieldValue($fieldDefinitions[$key]->getADT(), $item->getValue());
        }

        if ((string)ObjectType::COURSE() === $object->getType()) {
            (new ilECSCourseSettings($object))->handleContentUpdate();
        }

        $advancedMDValues->write();
    }

    private function loadAdvancedMDValues(ilAdvancedMDRecord $record): ilAdvancedMDValues
    {
        $advancedMDValues = new ilAdvancedMDValues($record->getRecordId(), $record->getParentObject(), '-', 0);
        $advancedMDValues->read();

        return $advancedMDValues;
    }

    /**
     * @param ilAdvancedMDValues $advancedMDValues
     * @return array<string, ilAdvancedMDFieldDefinition>
     */
    private function loadFieldDefinitions(ilAdvancedMDValues $advancedMDValues): array
    {
        $fieldDefinitions = [];

        foreach ($advancedMDValues->getDefinitions() as $fieldDefinition) {
            $fieldId = $fieldDefinition->getFieldId();
            if (null === $fieldId) {
                continue;
            }

            $fieldDefinitions[$fieldDefinition->getTitle()] = $fieldDefinition;
        }

        return $fieldDefinitions;
    }

    private function createTextFieldDefinition(int $recordId, string $key): ilAdvancedMDFieldDefinitionText
    {
        $fieldDefinition = new ilAdvancedMDFieldDefinitionText();
        $fieldDefinition->setTitle($key);
        $fieldDefinition->setRecordId($recordId);
        $fieldDefinition->setSearchable(false);
        $fieldDefinition->setRequired(true);
        $fieldDefinition->save();

        $translations = ilAdvancedMDFieldTranslations::getInstanceByRecordId($recordId);
        $translations->read();

        $fieldId = $fieldDefinition->getFieldId();
        if (null !== $fieldId) {
            $translation = $translations->getTranslation($fieldId, $translations->getDefaultLanguage());
            if ($translation instanceof ilAdvancedMDFieldTranslation) {
                $translation->setTitle($key);
                $translation->update();
            }
        }

        return $fieldDefinition;
    }

    /**
     * @param mixed $value
     */
    private function setFieldValue(ilADT $adt, $value): void
    {
        switch (true) {
            case $adt instanceof ilADTLocalizedText:
                $adt->setText($value);
                /** @var ilADTLocalizedTextDefinition $definition */
                $definition = $adt->getCopyOfDefinition();
                foreach ($definition->getActiveLanguages() as $language) {
                    $adt->setTranslation($language, (string)$value);
                }
                break;

            default:
                throw new Exception('Unknown ADT type');
        }
    }
}
