<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Metadata;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\CustomMetadataCollection;

/**
 * @psalm-immutable
 */
class SetCustomMetadata implements Command
{
    private int $objRefId;
    private string $title;
    private CustomMetadataCollection $customMetadataCollection;

    public function __construct(int $objRefId, string $title, CustomMetadataCollection $customMetadataCollection)
    {
        $this->objRefId = $objRefId;
        $this->title = $title;
        $this->customMetadataCollection = $customMetadataCollection;
    }

    public static function getName(): string
    {
        return 'SetCustomMetadata';
    }

    public function getObjRefId(): int
    {
        return $this->objRefId;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getCustomMetadataCollection(): CustomMetadataCollection
    {
        return $this->customMetadataCollection;
    }
}
