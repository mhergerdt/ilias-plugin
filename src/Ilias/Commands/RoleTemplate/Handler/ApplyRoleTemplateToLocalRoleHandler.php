<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\RoleTemplate\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\RoleTemplate\ApplyRoleTemplate;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\RoleTemplate\ApplyRoleTemplateToLocalRole;
use DRVBund\Plugins\CGAutomation\Ilias\Dispatcher;
use DRVBund\Plugins\CGAutomation\Ilias\Query\GetLocalRoleByParticipantRole;
use Exception;
use ilObjRole;

/**
 * @psalm-api
 * @template-implements CommandHandler<ApplyRoleTemplateToLocalRole>
 */
class ApplyRoleTemplateToLocalRoleHandler implements CommandHandler
{
    private Dispatcher $dispatcher;

    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function handles(): string
    {
        return ApplyRoleTemplateToLocalRole::getName();
    }

    public function handle(Command $command): void
    {
        $objRefId = $command->getObjRefId();
        $objType = $command->getObjType();
        $roleTemplateId = $command->getRoleTemplateId();
        $participantRole = $command->getParticipantRole();

        /** @var ?ilObjRole $localRole */
        $localRole = $this->dispatcher->dispatchQuery(new GetLocalRoleByParticipantRole($objRefId, $participantRole));

        if (!$localRole) {
            throw new Exception('Local role not found');
        }

        $this->dispatcher->dispatchCommand(
            new ApplyRoleTemplate($roleTemplateId, $localRole->getId(), $objRefId, $objType)
        );
    }
}
