<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\RoleTemplate\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandHandler;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\RoleTemplate\ApplyRoleTemplate;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;
use Exception;
use ilObject;
use ilObjectFactory;
use ilObjRole;
use ilRbacAdmin;
use ilRbacLog;
use ilRbacReview;

/**
 * @psalm-api
 * @template-implements CommandHandler<ApplyRoleTemplate>
 */
class ApplyRoleTemplateHandler implements CommandHandler
{
    private ilRbacReview $rbacReview;
    private ilRbacAdmin $rbacAdmin;
    private array $supportedObjTypes;
    private bool $isRbacLogActive;
    private array $rbacLogOld = [];

    /**
     * @param ilRbacReview $rbacReview
     * @param ilRbacAdmin $rbacAdmin
     */
    public function __construct(ilRbacReview $rbacReview, ilRbacAdmin $rbacAdmin)
    {
        $this->rbacReview = $rbacReview;
        $this->rbacAdmin = $rbacAdmin;

        $this->isRbacLogActive = ilRbacLog::isActive();

        $this->supportedObjTypes = [
            ObjectType::COURSE(),
            ObjectType::GROUP(),
        ];
    }

    public function handles(): string
    {
        return ApplyRoleTemplate::getName();
    }

    public function handle(Command $command): void
    {
        $objRefId = $command->getObjRefId();
        $objType = $command->getObjType();
        $roleTemplateId = $command->getRoleTemplateId();
        $localRoleId = $command->getLocalRoleId();

        $this->ensureObjRefIdIsSet($objRefId);
        $this->ensureSupportedObjType($objType);
        $this->ensureRoleTemplateExists($roleTemplateId);
        $localRole = $this->ensureLocalRole($localRoleId);

        $opsDiff = ilRbacLog::diffTemplate(
            ilRbacLog::gatherTemplate($objRefId, $localRoleId),
            $this->rbacReview->getAllOperationsOfRole($roleTemplateId)
        );
        if ([] === $opsDiff) {
            return;
        }

        $this->startLogRbac($objRefId, $localRoleId);

        $this->rbacAdmin->deleteRolePermission($localRoleId, $objRefId);
        $this->rbacAdmin->copyRoleTemplatePermissions(
            $roleTemplateId,
            ROLE_FOLDER_ID,
            $objRefId,
            $localRoleId
        );
        $operations = $this->rbacReview->getOperationsOfRole(
            $localRoleId,
            (string)$objType,
            $objRefId
        );
        $this->rbacAdmin->grantPermission($localRoleId, $operations, $objRefId);
        $localRole->update(); // Needed?

        $this->stopLogRbac($objRefId, $localRoleId);

        $isProtected = $this->rbacReview->isProtected($objRefId, $localRoleId);
        $this->rbacAdmin->setProtected(
            $objRefId,
            $localRoleId,
            $isProtected ? 'y' : 'n'
        );

        $localRole->changeExistingObjects(
            $objRefId,
            $isProtected ? ilObjRole::MODE_PROTECTED_KEEP_LOCAL_POLICIES : ilObjRole::MODE_UNPROTECTED_KEEP_LOCAL_POLICIES,
            ['all'],
            []
        );
    }

    private function ensureObjRefIdIsSet(int $objRefId): void
    {
        if (0 === $objRefId) {
            throw new Exception('Object refId missing');
        }
    }

    private function ensureSupportedObjType(ObjectType $objType): void
    {
        if (!in_array((string)$objType, array_map(fn(ObjectType $type) => (string) $type, $this->supportedObjTypes))) {
            throw new Exception('Unsupported object type');
        }
    }

    private function ensureRoleTemplateExists(?int $roleTemplateId): void
    {
        if (!ilObject::_exists($roleTemplateId, false, (string)ObjectType::ROLE_TEMPLATE())) {
            throw new Exception('Role template does not exist');
        }
    }

    private function ensureLocalRole(int $localRoleId): ilObjRole
    {
        $obj = ilObjectFactory::getInstanceByObjId($localRoleId);

        if ($obj instanceof ilObjRole) {
            return $obj;
        }

        throw new Exception('Local role does not exist');
    }

    private function startLogRbac(int $objRefId, int $localRoleId): void
    {
        if ($this->isRbacLogActive) {
            $this->rbacLogOld = ilRbacLog::gatherTemplate($objRefId, $localRoleId);
        }
    }

    private function stopLogRbac(int $objRefId, int $localRoleId): void
    {
        if ($this->isRbacLogActive) {
            ilRbacLog::add(
                ilRbacLog::EDIT_TEMPLATE,
                $objRefId,
                ilRbacLog::diffTemplate(
                    $this->rbacLogOld,
                    ilRbacLog::gatherTemplate($objRefId, $localRoleId)
                )
            );
        }
    }
}
