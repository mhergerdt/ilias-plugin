<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\RoleTemplate;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;

/**
 * @psalm-immutable
 */
class ApplyRoleTemplate implements Command
{
    private int $roleTemplateId;
    private int $localRoleId;
    private int $objRefId;
    private ObjectType $objType;

    public function __construct(
        int $roleTemplateId,
        int $localRoleId,
        int $objRefId,
        ObjectType $objType
    ) {
        $this->roleTemplateId = $roleTemplateId;
        $this->localRoleId = $localRoleId;
        $this->objRefId = $objRefId;
        $this->objType = $objType;
    }

    public static function getName(): string
    {
        return 'ApplyRoleTemplate';
    }

    public function getRoleTemplateId(): int
    {
        return $this->roleTemplateId;
    }

    public function getLocalRoleId(): int
    {
        return $this->localRoleId;
    }

    public function getObjRefId(): int
    {
        return $this->objRefId;
    }

    public function getObjType(): ObjectType
    {
        return $this->objType;
    }
}
