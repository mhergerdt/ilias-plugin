<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\RoleTemplate;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Command;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ParticipantRole;

/**
 * @psalm-immutable
 */
class ApplyRoleTemplateToLocalRole implements Command
{
    private int $roleTemplateId;
    private int $objRefId;
    private ObjectType $objType;
    private ParticipantRole $participantRole;

    public function __construct(int $roleTemplateId, int $objRefId, ObjectType $objType, ParticipantRole $participantRole)
    {
        $this->roleTemplateId = $roleTemplateId;
        $this->objRefId = $objRefId;
        $this->objType = $objType;
        $this->participantRole = $participantRole;
    }

    public static function getName(): string
    {
        return 'ApplyRoleTemplateToLocalRole';
    }

    public function getRoleTemplateId(): int
    {
        return $this->roleTemplateId;
    }

    public function getObjRefId(): int
    {
        return $this->objRefId;
    }

    public function getObjType(): ObjectType
    {
        return $this->objType;
    }

    public function getParticipantRole(): ParticipantRole
    {
        return $this->participantRole;
    }
}
