<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Category;

use DRVBund\Plugins\CGAutomation\Ilias\DTO\CategoryDTO;

/**
 * @psalm-immutable
 */
class CreateCategory implements CategoryCommand
{
    private int $parentTreeItemRefId;
    private CategoryDTO $categoryDTO;

    public function __construct(int $parentTreeItemRefId, CategoryDTO $categoryDTO)
    {
        $this->parentTreeItemRefId = $parentTreeItemRefId;
        $this->categoryDTO = $categoryDTO;
    }

    public static function getName(): string
    {
        return 'CreateCategory';
    }

    public function getParentTreeItemRefId(): int
    {
        return $this->parentTreeItemRefId;
    }

    public function getCategoryDTO(): CategoryDTO
    {
        return $this->categoryDTO;
    }
}
