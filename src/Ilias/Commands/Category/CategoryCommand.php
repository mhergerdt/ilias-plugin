<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Category;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;
use DRVBund\Plugins\CGAutomation\Ilias\DTO\CategoryDTO;

/**
 * @psalm-immutable
 */
interface CategoryCommand extends CommandWithResult
{
    public function getCategoryDTO(): CategoryDTO;
}
