<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Category\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Category\CategoryCommand;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Category\CreateCategory;
use ilObjCategory;

/**
 * @psalm-api
 * @template-extends StoreCategoryHandler<CreateCategory>
 */
class CreateCategoryHandler extends StoreCategoryHandler
{
    public function handles(): string
    {
        return CreateCategory::getName();
    }

    protected function getCategory(CategoryCommand $command): ilObjCategory
    {
        $parentTreeItemRefId = $command->getParentTreeItemRefId();

        $category = new ilObjCategory();
        $category->createReference();
        $category->putInTree($parentTreeItemRefId);
        $category->setPermissions($parentTreeItemRefId);

        return $category;
    }
}
