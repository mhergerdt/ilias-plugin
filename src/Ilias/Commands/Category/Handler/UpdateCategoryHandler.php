<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Category\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Category\CategoryCommand;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Category\UpdateCategory;
use DRVBund\Plugins\CGAutomation\Ilias\Repository\ObjectRepository;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;
use Exception;
use ilObjCategory;

/**
 * @psalm-api
 * @template-extends StoreCategoryHandler<UpdateCategory>
 */
class UpdateCategoryHandler extends StoreCategoryHandler
{
    private ObjectRepository $objectRepository;

    public function __construct(ObjectRepository $objectRepository)
    {
        $this->objectRepository = $objectRepository;
    }

    public function handles(): string
    {
        return UpdateCategory::getName();
    }

    protected function getCategory(CategoryCommand $command): ilObjCategory
    {
        /** @var ?ilObjCategory $category */
        $category = $this->objectRepository->loadByObjId($command->getCategoryId(), (string)ObjectType::CATEGORY());

        if ($category === null) {
            // exception no category found for id
            throw new Exception("No category found for id '{$command->getCategoryId()}'");
        }

        return $category;
    }
}
