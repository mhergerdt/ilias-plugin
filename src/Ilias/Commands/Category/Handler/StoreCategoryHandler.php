<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Category\Handler;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Category\CategoryCommand;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\CommandWithResult;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Handler\CommandWithResultHandler;
use ilObjCategory;

/**
 * @psalm-api
 * @template TCommand of CategoryCommand
 * @template-implements CommandWithResultHandler<TCommand, int>
 */
abstract class StoreCategoryHandler implements CommandWithResultHandler
{
    public function handle(CommandWithResult $command)
    {
        $category = $this->getCategory($command);

        $category->setTitle(mb_substr($command->getCategoryDTO()->getTitle(), 0, 128));
        $category->update();

        return $category->getId();
    }

    /**
     * @param TCommand $command
     */
    protected abstract function getCategory(CategoryCommand $command): ilObjCategory;
}
