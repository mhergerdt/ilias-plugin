<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Commands\Category;

use DRVBund\Plugins\CGAutomation\Ilias\DTO\CategoryDTO;

/**
 * @psalm-immutable
 */
class UpdateCategory implements CategoryCommand
{
    private int $categoryId;
    private CategoryDTO $categoryDTO;

    public function __construct(int $categoryId, CategoryDTO $categoryDTO)
    {
        $this->categoryId = $categoryId;
        $this->categoryDTO = $categoryDTO;
    }

    public static function getName(): string
    {
        return 'UpdateCategory';
    }

    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    public function getCategoryDTO(): CategoryDTO
    {
        return $this->categoryDTO;
    }
}
