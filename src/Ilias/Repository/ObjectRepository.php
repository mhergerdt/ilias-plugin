<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Repository;

use ilDBConstants;
use ilDBInterface;
use ilObject;
use ilObjectFactory;

class ObjectRepository
{
    private ilDBInterface $database;

    public function __construct(ilDBInterface $database)
    {
        $this->database = $database;
    }

    /**
     * @template TObject of ilObject
     * @param int $objId
     * @param string $objType
     * @return ilObject|null
     * @throws \ilDatabaseException
     * @throws \ilObjectNotFoundException
     */
    public function loadByObjId(int $objId, string $objType): ?ilObject
    {
        if (! ilObject::_exists($objId, false, $objType)) {
            return null;
        }

        return ilObjectFactory::getInstanceByObjId($objId);
    }

    /**
     * @template TObject of ilObject
     * @param string $importId
     * @param string $objType
     * @return ilObject|null
     * @throws \ilDatabaseException
     * @throws \ilObjectNotFoundException
     */
    public function loadByImportId(string $importId, string $objType): ?ilObject
    {
        $query = sprintf(
            'SELECT obj_id FROM object_data WHERE import_id = %s',
            $this->database->quote($importId, 'string')
        );

        $res = $this->database->query($query);
        $objId = null;
        while ($row = $res->fetchRow(ilDBConstants::FETCHMODE_ASSOC)) {
            $objId = (int)$row['obj_id'];
        }

        if ($objId === null) {
            return null;
        }

        return $this->loadByObjId($objId, $objType);
    }

    /**
     * @param int $objId
     * @return int[]
     */
    public function loadRefIdsForObjectId(int $objId): array
    {
        /**
         * Weird fix for psalm as value-of is not working
         * @var int[] $refIds
         */
        $refIds = array_values(ilObject::_getAllReferences($objId));
        return $refIds;
    }

    /**
     * @template TObject of ilObject
     * @return ilObject|null
     */
    public function loadByRefId(int $refId): ?ilObject
    {
        return ilObjectFactory::getInstanceByRefId($refId);
    }
}
