<?php

namespace DRVBund\Plugins\CGAutomation\Ilias\Facade;

use DRVBund\Plugins\CGAutomation\Ilias\Commands\Participant\AddCourseParticipant;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\Participant\AddGroupParticipant;
use DRVBund\Plugins\CGAutomation\Ilias\Dispatcher;
use DRVBund\Plugins\CGAutomation\Ilias\Query\GetUserByEmail;
use DRVBund\Plugins\CGAutomation\Ilias\Query\GetUserByEmailShaHash;
use DRVBund\Plugins\CGAutomation\Ilias\Query\Query;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\CourseParticipantRole;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\GroupParticipantRole;
use ilObjUser;

class ParticipationFacade
{
    private Dispatcher $dispatcher;

    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function addCourseParticipantByEmail(int $courseId, string $email, CourseParticipantRole $participantRole): void
    {
        $this->addCourseParticipant(
            $courseId,
            new GetUserByEmail($email),
            $participantRole
        );
    }

    public function addCourseParticipantByEmailHash(int $courseId, string $emailHash, CourseParticipantRole $participantRole): void
    {
        $this->addCourseParticipant(
            $courseId,
            GetUserByEmailShaHash::sha256($emailHash),
            $participantRole
        );
    }

    public function addGroupParticipantByEmail(int $groupId, string $email, GroupParticipantRole $participantRole): void
    {
        $this->addGroupParticipant(
            $groupId,
            new GetUserByEmail($email),
            $participantRole
        );
    }

    public function addGroupParticipantByEmailHash(int $groupId, string $emailHash, GroupParticipantRole $participantRole): void
    {
        $this->addGroupParticipant(
            $groupId,
            GetUserByEmailShaHash::sha256($emailHash),
            $participantRole
        );
    }

    private function addCourseParticipant(int $courseId, Query $getUserQuery, CourseParticipantRole $participantRole): void
    {
        $user = $this->dispatcher->dispatchQuery($getUserQuery);

        if (null === $user) {
            return;
        }

        $this->dispatcher->dispatchCommand(new AddCourseParticipant(
            $user->getId(),
            $courseId,
            $participantRole
        ));
    }

    private function addGroupParticipant(int $groupId, Query $getUserQuery, GroupParticipantRole $participantRole): void
    {
        $user = $this->dispatcher->dispatchQuery($getUserQuery);

        if (null === $user) {
            return;
        }

        $this->dispatcher->dispatchCommand(new AddGroupParticipant(
            $user->getId(),
            $groupId,
            $participantRole
        ));
    }
}
