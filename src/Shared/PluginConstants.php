<?php

namespace DRVBund\Plugins\CGAutomation\Shared;

final class PluginConstants
{
    public const PLUGIN_ID = 'cgauto';
    public const PLUGIN_NAME = 'CGAutomation';
}
