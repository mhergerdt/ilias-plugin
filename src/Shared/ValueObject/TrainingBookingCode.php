<?php

namespace DRVBund\Plugins\CGAutomation\Shared\ValueObject;

/**
 * @psalm-immutable
 * @template-extends ValueObject<TrainingBookingCode>
 */
final class TrainingBookingCode extends ValueObject
{
    public const LENGTH = 7;
    public const CHARSET = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';

    private string $value;

    public static function fromJson($data): self
    {
        if (!is_string($data)) {
            throw new \InvalidArgumentException('Invalid json data');
        }

        return new self($data);
    }

    public function __construct(string $value)
    {
        if (!ctype_alnum($value) || strlen($value) !== self::LENGTH) {
            throw new \InvalidArgumentException('Invalid code!');
        }

        $value = strtoupper($value);

        if (strspn($value, self::CHARSET) !== self::LENGTH) {
            throw new \InvalidArgumentException('Invalid code!');
        }

        if (self::calculateChecksumChar(substr($value, 0, self::LENGTH - 1)) !== $value[self::LENGTH - 1]) {
            throw new \InvalidArgumentException('Invalid code!');
        }

        $this->value = $value;
    }

    public static function calculateChecksumChar(string $value): string
    {
        $sum = array_reduce(str_split($value), function (int $sum, string $item): int {
            return $sum + ((int)strpos(self::CHARSET, $item));
        }, 0);

        return self::CHARSET[$sum % strlen(self::CHARSET)];
    }

    public function getValue(): string
    {
        return $this->value;
    }

    protected function isEqual($other): bool
    {
        return $this->value === $other->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function jsonSerialize(): string
    {
        return (string)$this;
    }
}
