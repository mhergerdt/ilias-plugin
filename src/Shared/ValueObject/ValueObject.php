<?php

namespace DRVBund\Plugins\CGAutomation\Shared\ValueObject;

use JsonSerializable;

/**
 * @psalm-immutable
 * @template TValueObject
 */
abstract class ValueObject implements JsonSerializable
{
    /**
     * @param string|int|float|array|bool $data
     * @return TValueObject
     */
    public abstract static function fromJson($data);

    public function equals(ValueObject $other): bool
    {
        if (!$this->isSameClass($other)) {
            return false;
        }

        return $this->isEqual($other);
    }

    /**
     * @psalm-mutation-free
     * @param TValueObject $other
     * @return bool
     */
    protected abstract function isEqual($other): bool;

    /**
     * @psalm-assert TValueObject $other
     * @param mixed $other
     */
    private function isSameClass($other): bool
    {
        return get_class($this) === get_class($other);
    }
}
