<?php

namespace DRVBund\Plugins\CGAutomation\Shared\ValueObject;

/**
 * @psalm-immutable
 * @template-extends ValueObject<FilePath>
 */
class FilePath extends ValueObject
{
    private DirectoryPath $path;
    private string $fileName;

    public static function fromJson($data): self
    {
        if (!is_array($data) || !isset($data['path']) || !isset($data['fileName'])) {
            throw new \InvalidArgumentException('Invalid json data');
        }

        return new self(DirectoryPath::fromJson($data['path']), $data['fileName']);
    }

    public function __construct(DirectoryPath $path, string $fileName)
    {
        $this->path = $path;
        $this->fileName = $fileName;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function getPath(): DirectoryPath
    {
        return $this->path;
    }

    /**
     * @param non-empty-string $filePath
     * @param non-empty-string $directorySeparator
     * @return self
     */
    public static function fromPath(string $filePath, string $directorySeparator = DIRECTORY_SEPARATOR): self
    {
        $fileParts = explode($directorySeparator, $filePath);
        $fileName = $filePath;
        $directoryPath = '';

        if (count($fileParts) > 1) {
            $fileName = array_pop($fileParts);
            $directoryPath = join($directorySeparator, $fileParts);
        }

        return new self(new DirectoryPath($directoryPath, $directorySeparator), $fileName);
    }

    public function __toString(): string
    {
        return (string)$this->path->join($this->fileName);
    }

    public function jsonSerialize(): array
    {
        return [
            'path' => $this->path->jsonSerialize(),
            'fileName' => $this->fileName,
        ];
    }

    protected function isEqual($other): bool
    {
        return $this->path->equals($other->path) && $this->fileName === $other->fileName;
    }
}
