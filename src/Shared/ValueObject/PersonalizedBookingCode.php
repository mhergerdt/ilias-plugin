<?php

namespace DRVBund\Plugins\CGAutomation\Shared\ValueObject;

/**
 * @psalm-immutable
 * @template-extends ValueObject<PersonalizedBookingCode>
 */
final class PersonalizedBookingCode extends ValueObject implements \JsonSerializable
{
    public const PERSONALIZED_PART_LENGTH = 4;
    public const LENGTH = 11;
    public const CHARSET = '23456789ABCDEFGH';
    private string $personalizedPart;
    private TrainingBookingCode $trainingBookingCode;

    public static function fromJson($data): self
    {
        if (!is_string($data)) {
            throw new \InvalidArgumentException('Invalid json data');
        }

        return self::fromString($data);
    }

    public static function fromTrainingBookingCodeAndUserEmail(TrainingBookingCode $trainingBookingCode, string $userEmail): self
    {
        return new PersonalizedBookingCode(self::extractCharsOfEmailHash($userEmail), $trainingBookingCode);
    }

    public static function fromString(string $value): self
    {
        if (strlen($value) !== self::LENGTH) {
            throw new \InvalidArgumentException('Invalid code!');
        }

        $personalizedPart = self::extractPartOfBookingCode($value, [
            ['index' => 0, 'length' => 1],
            ['index' => 3, 'length' => 1],
            ['index' => 6, 'length' => 1],
            ['index' => 9, 'length' => 1],
        ]);

        return new PersonalizedBookingCode(
            $personalizedPart,
            new TrainingBookingCode(self::extractPartOfBookingCode($value, [
                ['index' => 1, 'length' => 2],
                ['index' => 4, 'length' => 2],
                ['index' => 7, 'length' => 2],
                ['index' => 10, 'length' => 1],
            ]))
        );
    }

    private function __construct(string $personalizedPart, TrainingBookingCode $trainingBookingCode)
    {
        if (!ctype_alnum($personalizedPart) || strlen($personalizedPart) !== self::PERSONALIZED_PART_LENGTH) {
            throw new \InvalidArgumentException('Invalid code!');
        }

        $personalizedPart = strtoupper($personalizedPart);

        if (strspn($personalizedPart, self::CHARSET) !== self::PERSONALIZED_PART_LENGTH) {
            throw new \InvalidArgumentException('Invalid code!');
        }

        $this->personalizedPart = $personalizedPart;
        $this->trainingBookingCode = $trainingBookingCode;
    }

    public function isValidForEmail(string $email): bool
    {
        return $this->personalizedPart === self::extractCharsOfEmailHash($email);
    }

    public function getPersonalizedPart(): string
    {
        return $this->personalizedPart;
    }

    public function getTrainingBookingCode(): TrainingBookingCode
    {
        return $this->trainingBookingCode;
    }

    public function __toString(): string
    {
        $trainingCode = (string)$this->trainingBookingCode;

        return substr($this->personalizedPart, 0, 1) .
            substr($trainingCode, 0, 2) .
            substr($this->personalizedPart, 1, 1) .
            substr($trainingCode, 2, 2) .
            substr($this->personalizedPart, 2, 1) .
            substr($trainingCode, 4, 2) .
            substr($this->personalizedPart, 3, 1) .
            substr($trainingCode, 6, 1);
    }

    public function jsonSerialize(): string
    {
        return (string)$this;
    }

    protected function isEqual($other): bool
    {
        return $this->personalizedPart === $other->personalizedPart && $this->trainingBookingCode->equals($other->trainingBookingCode);
    }

    /**
     * @psalm-pure
     */
    private static function extractCharsOfEmailHash(string $userEmail): string
    {
        $chars = [];
        $emailHash = hash('sha256', $userEmail);
        for ($i = 0; $i < strlen($emailHash); $i++) {
            if (($i + 5) % 16 === 0) {
                $chars[] = strtoupper($emailHash[$i]);
            }
        }

        if (count($chars) !== self::PERSONALIZED_PART_LENGTH) {
            throw new \InvalidArgumentException('Unable to extract chars from email hash!');
        }

        return str_replace(['0', '1'], ['G', 'H'], join('', $chars));
    }

    private static function extractPartOfBookingCode(string $bookingCode, array $config): string
    {
        return join('', array_map(function ($config) use ($bookingCode) {
            return substr($bookingCode, $config['index'], $config['length']);
        }, $config));
    }
}
