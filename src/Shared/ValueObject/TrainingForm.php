<?php

namespace DRVBund\Plugins\CGAutomation\Shared\ValueObject;

use InvalidArgumentException;

/**
 * @psalm-immutable
 * @template-extends ValueObject<TrainingForm>
 */
class TrainingForm extends ValueObject
{
    private const PRESENCE = 'Präsenztraining';
    private const WBT = 'Statisches Web Based Training';
    public const ALLOWED_VALUES = [
        self::PRESENCE,
        self::WBT,
    ];
    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public static function Presence(): self
    {
        return new self(self::PRESENCE);
    }

    public static function Wbt(): self
    {
        return new self(self::WBT);
    }

    public static function fromJson($data): self
    {
        if (!is_string($data)) {
            throw new InvalidArgumentException('Invalid json data');
        }

        return new self($data);
    }

    public function isKnownValue(): bool
    {
        return in_array($this->value, self::ALLOWED_VALUES);
    }

    public function jsonSerialize(): string
    {
        return (string)$this;
    }

    protected function isEqual($other): bool
    {
        return $this->value === $other->value;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
