<?php

namespace DRVBund\Plugins\CGAutomation\Shared\ValueObject;

/**
 * @psalm-immutable
 * @template-extends ValueObject<DirectoryPath>
 */
class DirectoryPath extends ValueObject
{
    private string $directorySeparator;
    private string $path;

    public static function fromJson($data): self
    {
        if (!is_array($data) || !isset($data['path'], $data['directorySeparator'])) {
            throw new \InvalidArgumentException('Invalid json data');
        }

        return new self($data['path'], $data['directorySeparator']);
    }

    public function __construct(string $path, string $directorySeparator = DIRECTORY_SEPARATOR)
    {
        $this->path = $path;
        $this->directorySeparator = $directorySeparator;
    }

    public static function joinPartsWithDefaultSeparator(string ...$parts): DirectoryPath
    {
        return self::joinParts(DIRECTORY_SEPARATOR, ...$parts);
    }

    /**
     * @psalm-mutation-free
     */
    public static function joinParts(string $directorySeparator, string ...$parts): DirectoryPath
    {
        return new DirectoryPath(
            str_replace(
                $directorySeparator . $directorySeparator,
                $directorySeparator,
                join($directorySeparator, $parts),
            ),
            $directorySeparator,
        );
    }

    public function getDirectorySeparator(): string
    {
        return $this->directorySeparator;
    }

    public function join(string ...$parts): DirectoryPath
    {
        return self::joinParts($this->directorySeparator, $this->path, ...$parts);
    }

    public function appendFileName(string $fileName): FilePath
    {
        return new FilePath($this, $fileName);
    }

    public function __toString(): string
    {
        return $this->path;
    }

    protected function isEqual($other): bool
    {
        return $this->path === $other->path;
    }

    public function jsonSerialize(): array
    {
        return [
            'path' => $this->path,
            'directorySeparator' => $this->directorySeparator,
        ];
    }
}
