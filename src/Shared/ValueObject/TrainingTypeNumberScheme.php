<?php

namespace DRVBund\Plugins\CGAutomation\Shared\ValueObject;

use InvalidArgumentException;

/**
 * @psalm-immutable
 * @template-extends ValueObject<TrainingTypeNumberScheme>
 */
final class TrainingTypeNumberScheme extends ValueObject
{
    private string $scheme;
    /**
     * @psalm-var non-empty-string
     */
    private string $regex;

    public static function fromJson($data)
    {
        if (!is_string($data)) {
            throw new InvalidArgumentException('Invalid json data');
        }

        return new self($data);
    }

    /**
     * @param string ...$schemes
     * @return TrainingTypeNumberScheme[]
     */
    public static function createMany(string ...$schemes): array
    {
        return array_map(fn (string $scheme) => new self($scheme), $schemes);
    }

    public function __construct(string $scheme)
    {
        if (!preg_match('/^[\dx]{4}\.[\dx]{4}$/i', $scheme)) {
            throw new \Exception('Invalid scheme. The scheme must contain only digits from 0 to 9 and or the character x (as placeholder)');
        }

        $this->scheme = $scheme;
        $this->regex = '/' . str_replace('x', '\d', str_replace('.', '\.', strtolower($scheme))) . '/';
    }

    public function matches(string $trainingTypeNumber): bool
    {
        return !!preg_match($this->regex, $trainingTypeNumber);
    }

    public function jsonSerialize()
    {
        return $this->scheme;
    }

    public function __toString()
    {
        return $this->scheme;
    }

    protected function isEqual($other): bool
    {
        return $this->scheme === $other->scheme;
    }
}
