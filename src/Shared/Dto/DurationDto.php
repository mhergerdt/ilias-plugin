<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

class DurationDto
{
    public float $hours;
    public float $days;

    public static function fromValues(float $hours, float $days): self
    {
        $self = new self();
        $self->hours = $hours;
        $self->days = $days;

        return $self;
    }
}
