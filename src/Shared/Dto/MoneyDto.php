<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

class MoneyDto {
    public string $amount;
    public string $currency;

    public static function fromValues(string $amount, string $currency): self
    {
        $self = new self();
        $self->amount = $amount;
        $self->currency = $currency;

        return $self;
    }
}
