<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

class TrainingScheduleEntryDto
{
    public \DateTime $date;
    public string $startTime;
    public string $endTime;
}
