<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

class TrainingGroupDto
{
    public string $id;
    public string $short;
    public string $title;
    public string $parent;
    /**
     * @var TrainingGroupDto[]
     */
    public array $children = [];

    public static function fromValues(
        string $id,
        string $short,
        string $title,
        string $parent
    ): self
    {
        $dto = new self();

        $dto->id = $id;
        $dto->short = $short;
        $dto->title = $title;
        $dto->parent = $parent;

        return $dto;
    }
}
