<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

class ObjectLinkDto {
    public string $id;
    public string $type;
    public string $short;
    public string $label;

    public static function fromValues(string $id, string $type, string $short, string $label): self
    {
        $self = new self();
        $self->id = $id;
        $self->type = $type;
        $self->short = $short;
        $self->label = $label;

        return $self;
    }
}
