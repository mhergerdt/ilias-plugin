<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

class LocationDto {
    public string $id;
    public string $title;
    public string $streetLine1;
    public string $streetLine2;
    public string $streetNumber;
    public string $zipCode;
    public string $city;

    public static function fromValues(string $id, string $title, string $streetLine1, string $streetLine2, string $streetNumber, string $zipCode, string $city): self
    {
        $self = new self();
        $self->id = $id;
        $self->title = $title;
        $self->streetLine1 = $streetLine1;
        $self->streetLine2 = $streetLine2;
        $self->streetNumber = $streetNumber;
        $self->zipCode = $zipCode;
        $self->city = $city;

        return $self;
    }
}
