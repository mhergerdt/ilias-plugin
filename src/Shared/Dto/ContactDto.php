<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

class ContactDto {
    public string $employeeNumber;
    public string $fullName;
    public ?string $email;
    public ?string $iliasReference;

    public static function fromValues(string $employeeNumber, string $fullName, ?string $email, ?string $iliasReference): self
    {
        $self = new self();
        $self->employeeNumber = $employeeNumber;
        $self->fullName = $fullName;
        $self->email = $email;
        $self->iliasReference = $iliasReference;

        return $self;
    }

    public static function fromNameAndIliasRef(string $fullName,?string $iliasReference): self
    {
        return self::fromValues('', $fullName, null, $iliasReference);
    }
}
