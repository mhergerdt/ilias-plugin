<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

class OccupancyDto
{
    public int $free;
    public int $waiting;

    public static function fromValues(int $free, int $waiting): self
    {
        $self = new self();
        $self->free = $free;
        $self->waiting = $waiting;

        return $self;
    }
}
