<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

use Exception;
use ReflectionClass;

class VerbalDescriptionsDto {
    /**
     * @var string[]
     */
    private static array $DTO_PROPERTIES = [];
    /**
     * @var string[]
     */
    public array $contentShort = [];
    /**
     * @var string[]
     */
    public array $contentLong = [];
    /**
     * @var string[]
     */
    public array $notes = [];
    /**
     * @var string[]
     */
    public array $targetGroup = [];
    /**
     * @var string[]
     */
    public array $goals = [];
    /**
     * @var string[]
     */
    public array $preconditions = [];
    /**
     * @var string[]
     */
    public array $methods = [];
    /**
     * @var string[]|null
     */
    public ?array $title = null;

    /**
     * @return string[]
     */
    public static function getFields(): array {
        if (empty(self::$DTO_PROPERTIES)) {
            $reflection = new ReflectionClass(self::class);
            self::$DTO_PROPERTIES = array_map(
                function($property) { return $property->getName(); },
                array_filter(
                    $reflection->getProperties(),
                    function($property) { return $property->isPublic(); }
                )
            );
        }

        return self::$DTO_PROPERTIES;
    }

    /**
     * @param string $field
     * @param string $value
     * @return $this
     * @throws Exception
     */
    public function map(string $field, string $value): self {
        if (!in_array($field, self::getFields())) {
            throw new Exception("Unknown verbal description field: " . $field);
        }
        if (!is_array($this->$field)) {
            $this->$field = [];
        }
        $this->$field[] = $value;

        return $this;
    }
}
