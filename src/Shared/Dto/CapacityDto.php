<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

class CapacityDto
{
    public int $minimum;
    public int $optimum;
    public int $maximum;

    public static function fromValues(int $minimum, int $optimum, int $maximum): self
    {
        $self = new self();
        $self->minimum = $minimum;
        $self->optimum = $optimum;
        $self->maximum = $maximum;

        return $self;
    }
}
