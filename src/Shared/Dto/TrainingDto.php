<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

use DateTime;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingForm;

class TrainingDto
{
    public string $id;
    public string $trainingTypeId;
    public string $short;
    public string $planVariant;
    public string $objectType;
    public TrainingForm $trainingForm;
    public string $title;
    public DateTime $validFrom;
    public DateTime $validUntil;
    public string $status;
    public string $iliasBookingCode;
    public LocationDto $location;
    public ?DateTime $registrationDeadline;
    public CapacityDto $capacity;
    public OccupancyDto $occupancy;
    public DurationDto $duration;
    public MoneyDto $priceInternal;
    public MoneyDto $priceExternal;
    /**
     * @var ContactDto[]
     */
    public array $contentResponsible = [];
    /**
     * @var ContactDto[]
     */
    public array $eventResponsible = [];
    /**
     * @var ContactDto[]
     */
    public array $speaker = [];
    /**
     * @var ObjectLinkDto[]
     */
    public array $rooms = [];
    /**
     * @var TrainingScheduleEntryDto[]
     */
    public array $schedule = [];
    /**
     * @var string[]
     */
    public array $allowedParticipantMailHashes = [];
}
