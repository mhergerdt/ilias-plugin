<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

use DateTimeInterface;

class BilbaoDataDto
{
    /**
     * @var array<TrainingGroupDto>
     */
    public array $trainingGroups = [];
    /**
     * @var array<TrainingTypeDto>
     */
    public array $trainingTypes = [];
    /**
     * @var array<string, IliasContactDto>
     */
    public array $iliasContacts = [];
    public DateTimeInterface $exportedAt;
}
