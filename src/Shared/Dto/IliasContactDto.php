<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

class IliasContactDto
{
    public string $employeeNumber;
    public string $title;
    public string $firstName;
    public string $lastName;
    public string $fullName;
    public ?string $phoneNumber;
    public ?string $email;
    public ?string $image;
    public string $iliasContactReference;

    public static function fromValues(
        string $employeeNumber,
        string $title,
        string $firstName,
        string $lastName,
        string $fullName,
        ?string $phoneNumber,
        ?string $email,
        ?string $image,
        string $iliasContactReference
    ): self
    {
        $self = new self();
        $self->employeeNumber = $employeeNumber;
        $self->title = $title;
        $self->firstName = $firstName;
        $self->lastName = $lastName;
        $self->fullName = $fullName;
        $self->phoneNumber = $phoneNumber;
        $self->email = $email;
        $self->image = $image;
        $self->iliasContactReference = $iliasContactReference;

        return $self;
    }
}
