<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

use DateTime;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingForm;

class TrainingTypeDto
{
    public string $id;
    public string $short;
    public string $planVariant;
    public string $objectType;
    public string $title;
    public DateTime $validFrom;
    public DateTime $validUntil;
    public string $trainingGroupId;
    public TrainingForm $trainingForm;
    public VerbalDescriptionsDto $verbalDescriptions;
    public bool $priceVatExempt;
    public MoneyDto $priceInternal;
    public MoneyDto $priceExternal;
    public CapacityDto $capacity;
    public bool $showInBrochure;
    public bool $showInIntranet;
    public bool $showInInternet;
    public bool $determinationOfDemand;
    public string $targetGroup;
    public string $accountingChar;
    public string $accountingCategory;
    public DurationDto $duration;
    /**
     * @var ContactDto[]
     */
    public array $contentResponsible = [];
    /**
     * @var ContactDto[]
     */
    public array $eventResponsible = [];
    /**
     * @var ContactDto[]
     */
    public array $speaker = [];
    /**
     * @var ObjectLinkDto[]
     */
    public array $belongsTo = [];
    /**
     * @var ObjectLinkDto[]
     */
    public array $requires = [];
    /**
     * @var ObjectLinkDto[]
     */
    public array $intendedFor = [];
    /**
     * @var ObjectLinkDto[]
     */
    public array $hostedBy = [];
    /**
     * @var ObjectLinkDto[]
     */
    public array $mandatoryFor = [];
    /**
     * @var ObjectLinkDto[]
     */
    public array $needs = [];
    /**
     * @var ObjectLinkDto[]
     */
    public array $mediates = [];
    /**
     * @var TrainingDto[]
     */
    public array $trainings = [];
}
