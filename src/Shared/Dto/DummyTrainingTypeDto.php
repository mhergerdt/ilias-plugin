<?php

namespace DRVBund\Plugins\CGAutomation\Shared\Dto;

class DummyTrainingTypeDto
{
    public string $id;
    public TrainingTypeDto $trainingType;
    /**
     * @var array<string>
     */
    public array $containsTrainingIds;
    /**
     * @var array<string>
     */
    public array $replacesTrainingTypes;
    public bool $includeReplacedTrainingTypeTrainings;

    public static function fromValues(
        string $id,
        TrainingTypeDto $trainingTypeDto,
        array $containsTrainingIds,
        array $replacesTrainingTypes,
        bool $includeReplacedTrainingTypeTrainings
    ): self
    {
        $dto = new DummyTrainingTypeDto();
        $dto->id = $id;
        $dto->trainingType = $trainingTypeDto;
        $dto->containsTrainingIds = $containsTrainingIds;
        $dto->replacesTrainingTypes = $replacesTrainingTypes;
        $dto->includeReplacedTrainingTypeTrainings = $includeReplacedTrainingTypeTrainings;
        return $dto;
    }
}
