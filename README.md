
# Deployment
@TODO: To be documented

# Development

## Setup

Adjust [client.ini.php](..%2F..%2F..%2F..%2F..%2F..%2F..%2Fdata%2Fmyilias%2Fclient.ini.php)
```ini
[session]
expire = "31536000"

[system]
DEBUG = "0"
DEVMODE = "1"
```

Adjust [ilias.ini.php](..%2F..%2F..%2F..%2F..%2F..%2F..%2Filias.ini.php)
```ini
; <?php exit; ?>
[server]
http_path = "http://ilias.localhost"
absolute_path = "/var/www/project"
presetting = ""
timezone = "Europe/Berlin"

[clients]
path = "data"
inifile = "client.ini.php"
datadir = "/var/lib/ilias"
default = "myilias"
list = "0"

[setup]
pass = ""

[tools]
convert = "/usr/bin/convert"
zip = "/usr/bin/zip"
unzip = "/usr/bin/unzip"
java = ""
ffmpeg = ""
ghostscript = ""
lessc = "/usr/local/bin/lessc"
enable_system_styles_management = "1"
vscantype = "none"
scancommand = ""
cleancommand = ""
icap_host = ""
icap_port = ""
icap_service_name = ""
icap_client_path = ""

[log]
path = "/var/log/ilias"
file = "ilias_test7.log"
enabled = "1"
level = "WARNING"
error_path = "/var/log/ilias/ilias_errorlogs"

[debian]
data_dir = "/var/opt/ilias"
log = "/var/log/ilias/ilias.log"
convert = "/usr/bin/convert"
zip = "/usr/bin/zip"
unzip = "/usr/bin/unzip"
java = ""
ffmpeg = "/usr/bin/ffmpeg"

[redhat]
data_dir = ""
log = ""
convert = ""
zip = ""
unzip = ""
java = ""

[suse]
data_dir = ""
log = ""
convert = ""
zip = ""
unzip = ""
java = ""

[https]
auto_https_detect_enabled = "0"
auto_https_detect_header_name = ""
auto_https_detect_header_value = ""
forced = "0"

[background_tasks]
concurrency = "sync"
number_of_concurrent_tasks = "1"
```

### Activate DEVMODE
https://docu.ilias.de/ilias.php?baseClass=illmpresentationgui&cmd=layout&ref_id=42&obj_id=1082
https://github.com/ILIAS-eLearning/ILIAS/blob/release_8/docs/configuration/settings/ini-file.md

## Refresh ILIAS Plugin & Artifacts
Run [refreshAutoloadAndArtifacts.sh](..%2F..%2F..%2F..%2F..%2F..%2F..%2FrefreshAutoloadAndArtifacts.sh)

```bash
./refreshAutoloadAndArtifacts.sh
```

## Show error log
Run [showLog.sh](..%2F..%2F..%2F..%2F..%2F..%2F..%2FshowLog.sh)

```bash
./showLog.sh [ID]
```

## Run cron jobs
Run [runCrons.sh](..%2F..%2F..%2F..%2F..%2F..%2F..%2FrunCrons.sh)

```bash
./runCrons.sh
```
