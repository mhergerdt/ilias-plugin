<?php

use DRVBund\Plugins\CGAutomation\BilbaoImport\Import\BilbaoImportOrchestrator;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Synchronization\BilbaoSynchronizationOrchestrator;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Generate\DefaultEducationCatalogueGenerator;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Generate\EducationCatalogueGenerator;
use DRVBund\Plugins\CGAutomation\Ilias\PluginSettings;
use DRVBund\Plugins\CGAutomation\Shared\PluginConstants;

/**
 * @psalm-api
 */
class ilCGAutomationImportCronJob extends ilCronJob
{
    public const CRON_JOB_ID = PluginConstants::PLUGIN_ID . '_import_bilbao_data_cron_job';

    private ilPluginLanguage $language;
    private PluginSettings $pluginSettings;
    private BilbaoImportOrchestrator $bilbaoImportOrchestrator;
    private BilbaoSynchronizationOrchestrator $bilbaoSynchronizationOrchestrator;
    private EducationCatalogueGenerator $educationCatalogueGenerator;

    public function __construct(
        ilPluginLanguage                  $language,
        PluginSettings                    $pluginSettings,
        BilbaoImportOrchestrator          $bilbaoImportOrchestrator,
        BilbaoSynchronizationOrchestrator $bilbaoSynchronizationOrchestrator,
        EducationCatalogueGenerator       $educationCatalogueGenerator
    )
    {
        $this->language = $language;
        $this->pluginSettings = $pluginSettings;
        $this->bilbaoImportOrchestrator = $bilbaoImportOrchestrator;
        $this->bilbaoSynchronizationOrchestrator = $bilbaoSynchronizationOrchestrator;
        $this->educationCatalogueGenerator = $educationCatalogueGenerator;
    }

    public function getId(): string
    {
        return self::CRON_JOB_ID;
    }

    public function getTitle(): string
    {
        return PluginConstants::PLUGIN_NAME . ': ' . $this->language->txt('import_bilbao_data_cron_job_title');
    }

    public function getDescription(): string
    {
        return $this->language->txt('import_bilbao_data_cron_job_description');
    }

    public function hasAutoActivation(): bool
    {
        return false;
    }

    public function hasFlexibleSchedule(): bool
    {
        return true;
    }

    public function getDefaultScheduleType(): int
    {
        return self::SCHEDULE_TYPE_DAILY;
    }

    public function getDefaultScheduleValue(): ?int
    {
        return null;
    }

    public function run(): ilCronJobResult
    {
        $result = new ilCronJobResult();
        $time = microtime(true);

        try {
            $this->bilbaoImportOrchestrator->import($this->pluginSettings->getDataArchivePath());

            $this->bilbaoSynchronizationOrchestrator->sync($this->pluginSettings->refIdTargetCategory()->getValueAsInt());

            $this->educationCatalogueGenerator->generate();

            $result->setStatus(ilCronJobResult::STATUS_OK);
        } catch (Throwable $e) {
            $result->setStatus(ilCronJobResult::STATUS_FAIL);
            $result->setMessage($e->getMessage());
        }

        $result->setDuration(microtime(true) - $time);

        return $result;
    }
}
