<?php

require_once __DIR__ . "/../vendor/autoload.php";
/** @psalm-suppress MissingFile */
include_once("./Services/UIComponent/classes/class.ilUserInterfaceHookPlugin.php");

use DRVBund\Plugins\CGAutomation\DependencyInjection\DIC;
use DRVBund\Plugins\CGAutomation\Shared\PluginConstants;

/**
 * @psalm-api
 */
class ilCGAutomationPlugin extends ilUserInterfaceHookPlugin implements ilCronJobProvider
{
    /** @psalm-suppress UnusedProperty */
    private DIC $container;

    public function __construct(ilDBInterface $db, ilComponentRepositoryWrite $component_repository, string $id)
    {
        parent::__construct($db, $component_repository, $id);

        $this->container = new DIC();
    }


    public function getPluginName(): string
    {
        return PluginConstants::PLUGIN_NAME;
    }

    public function uninstall(): bool
    {
        try {
            $settings = new ilSetting(PluginConstants::PLUGIN_ID);
            $settings->deleteAll();

            return parent::uninstall();
        } catch (Exception $e) {
            ilLoggerFactory::getLogger(PluginConstants::PLUGIN_ID)->log($e->getMessage());
        }

        return false;
    }

    /**
     * @return ilCronJob[]
     */
    public function getCronJobInstances(): array
    {
        return [
            $this->getCronJobInstance(ilCGAutomationImportCronJob::CRON_JOB_ID)
        ];
    }

    public function getCronJobInstance(string $jobId): ilCronJob
    {
        switch ($jobId) {
            case ilCGAutomationImportCronJob::CRON_JOB_ID:
            default:
                return $this->container->get(ilCGAutomationImportCronJob::class);
        }
    }
}
