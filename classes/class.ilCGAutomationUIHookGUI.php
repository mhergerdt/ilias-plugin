<?php

use DRVBund\Plugins\CGAutomation\BilbaoImport\Import\BilbaoImportOrchestrator;
use DRVBund\Plugins\CGAutomation\BilbaoImport\Synchronization\BilbaoSynchronizationOrchestrator;
use DRVBund\Plugins\CGAutomation\Booking\BookingService;
use DRVBund\Plugins\CGAutomation\DependencyInjection\DIC;
use DRVBund\Plugins\CGAutomation\EducationCatalogue\Generate\EducationCatalogueGenerator;
use DRVBund\Plugins\CGAutomation\Ilias\PluginSettings;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\PersonalizedBookingCode;
use DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingBookingCode;
use ILIAS\HTTP\Services;
use ILIAS\Refinery\Factory;

/** @psalm-suppress MissingFile */
include_once("./Services/UIComponent/classes/class.ilUIHookPluginGUI.php");

/**
 * @psalm-api
 * @psalm-suppress UnusedProperty
 * @ilCtrl_isCalledBy ilCGAutomationUIHookGUI: ilUIPluginRouterGUI
 */
class ilCGAutomationUIHookGUI extends ilUIHookPluginGUI
{
    private DIC $container;
    private Services $http;
    private Factory $refinery;
    private ilObjUser $user;

    public function __construct()
    {
        $this->container = new DIC();

        $this->http = $this->container->get(Services::class);
        $this->refinery = $this->container->get(Factory::class);
        $this->user = $this->container->get(ilObjUser::class);
    }

//    public function executeCommand(): void
//    {
//        $next_class = $this->ctrl->getNextClass();
//        switch ($next_class) {
//            default:
//                $cmd = $this->ctrl->getCmd();
//                $this->$cmd();
//                break;
//        }
//    }

//    public function testLinkAjaxResponse() {
//        var_dump($this->http->wrapper()->query()->retrieve('bookingCode', $this->refinery->to()->string()));
//        $response = 'Hello';
//        echo $response;
//        exit();
//    }

    public function gotoHook(): void
    {
        $pluginSettings = $this->container->get(PluginSettings::class);
        $bookingCodeService = $this->container->get(BookingService::class);
        $bilbaoImportOrchestrator = $this->container->get(BilbaoImportOrchestrator::class);
        $bilbaoSynchronizationOrchestrator = $this->container->get(BilbaoSynchronizationOrchestrator::class);
        $educationCatalogueGenerator = $this->container->get(EducationCatalogueGenerator::class);

        $pluginSettings->ensureRequiredConfig();

        if ($this->http->wrapper()->query()->has('generateBookingCode')) {
            $this->sendResponse(200, [
                'bookingCode' => PersonalizedBookingCode::fromTrainingBookingCodeAndUserEmail(
                    new TrainingBookingCode(
                        $this->http->wrapper()->query()->retrieve('trainingCode', $this->refinery->to()->string())
                    ),
                    $this->user->getEmail()
                )
            ]);
        }

        if ($this->http->wrapper()->query()->has('bookingCode')) {
            $bookingCode = $this->http->wrapper()->query()->retrieve('bookingCode', $this->refinery->to()->string());

            try {
                $result = $bookingCodeService->redeem($bookingCode);
                $this->sendResponse(200, $result);
            } catch (InvalidArgumentException $exception) {
                $this->sendResponse(400, ['error' => $exception->getMessage()]);
            } catch (Exception $exception) {
                if ($exception->getMessage() === 'Unknown booking code!') {
                    $this->sendResponse(404, ['error' => $exception->getMessage()]);
                }

                if ($exception->getMessage() === 'Booking code invalid for current user!') {
                    $this->sendResponse(403, ['error' => $exception->getMessage()]);
                }

                $this->sendResponse(400, ['error' => $exception->getMessage()]);
            }
        }

        if ($this->http->wrapper()->query()->has('bilbao')) {
            $bilbaoImportOrchestrator->import($pluginSettings->getDataArchivePath());
            $this->sendResponse(200, []);
        }

        if ($this->http->wrapper()->query()->has('sync')) {
            $bilbaoSynchronizationOrchestrator->sync($pluginSettings->refIdTargetCategory()->getValueAsInt());
        }

        if ($this->http->wrapper()->query()->has('educationCatalogue')) {
            $educationCatalogueGenerator->generate();
            $this->sendResponse(200, []);
        }

        if ($this->http->wrapper()->query()->has('copy')) {
//            $result = (new \DRVBund\Plugins\CGAutomation\Ilias\Commands\CopyTemplateObject(
//                $this->container->get(ilTree::class),
//                6167,
//                913,
//                []
//            ))->execute();
            $trainingType = new \DRVBund\Plugins\CGAutomation\Shared\Dto\TrainingTypeDto();
            $trainingType->id = 'test-id';
            $trainingType->trainingForm = \DRVBund\Plugins\CGAutomation\Shared\ValueObject\TrainingForm::Presence();
            $trainingType->short = '0000.1111';
            $trainingType->title = 'Test training type';
            $trainingType->verbalDescriptions = new \DRVBund\Plugins\CGAutomation\Shared\Dto\VerbalDescriptionsDto();
            $trainingType->validFrom = new DateTime();
            $trainingType->validUntil = new DateTime();
            $trainingType->trainings = [];

            $courseId = $this
                ->container
                ->get(\DRVBund\Plugins\CGAutomation\BilbaoImport\Synchronization\CourseSynchronizer::class)
                ->sync(913, $trainingType);
            $courseRefId = $this->container->get(\DRVBund\Plugins\CGAutomation\Ilias\Service\TreeService::class)->resolveObjRefIdUnderParentTreeItem(
                $courseId,
                $this->container->get(PluginSettings::class)->refIdTargetCategory()->getValueAsInt()
            );
            $this->sendResponse(200, ['ref_id' => $courseRefId]);
        }

        parent::gotoHook();
    }

    private function sendResponse(int $statusCode, array $jsonContent): void
    {
        $body = $this->http->response()->getBody();

        $this->http->saveResponse(
            $this
                ->http
                ->response()
                ->withStatus($statusCode)
                ->withAddedHeader('Content-Type', 'application/json')
        );
        $body->write(json_encode($jsonContent));
        $this->http->sendResponse();
        exit();
    }
}
