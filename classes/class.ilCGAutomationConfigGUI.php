<?php

use DRVBund\Plugins\CGAutomation\DependencyInjection\DIC;
use DRVBund\Plugins\CGAutomation\Ilias\Adapter\FilesystemAdapter;
use DRVBund\Plugins\CGAutomation\Ilias\Commands\TreeItem\RelocateTreeItemChildren;
use DRVBund\Plugins\CGAutomation\Ilias\Dispatcher;
use DRVBund\Plugins\CGAutomation\Ilias\PluginSettingItem;
use DRVBund\Plugins\CGAutomation\Ilias\PluginSettings;
use DRVBund\Plugins\CGAutomation\Ilias\ValueObjects\ObjectType;
use ILIAS\DI\UIServices;
use ILIAS\HTTP\Services;
use ILIAS\Refinery\Constraint;
use ILIAS\Refinery\Factory;
use ILIAS\UI\Component\Input\Container\Form\Standard;
use ILIAS\UI\Component\Input\Field\FormInput;

/**
 * @psalm-api
 * @ilCtrl_IsCalledBy ilCGAutomationConfigGUI: ilObjComponentSettingsGUI
 *
 * @psalm-type PostUpdateCallback = Closure(string, string): void
 * @psalm-type FormFieldConfig = array{settingItem: PluginSettingItem, field: FormInput, postUpdate?: PostUpdateCallback}
 */
class ilCGAutomationConfigGUI extends ilPluginConfigGUI
{
    private const TAB_CONFIGURE = 'configure';
    private const CMD_CONFIGURE = 'configure';
    private const CMD_SAVE_CONFIG = 'save_config';

    private ilTabsGUI $tabs;
    private ilCtrlInterface $ctrl;
    private UIServices $ui;
    private Services $http;
    private PluginSettings $pluginSettings;
    private Factory $refinery;
    private FilesystemAdapter $filesystem;
    private Dispatcher $dispatcher;

    public function __construct()
    {
        $container = new DIC();

        $this->tabs = $container->get(ilTabsGUI::class);
        $this->ctrl = $container->get(ilCtrlInterface::class);
        $this->ui = $container->get(UIServices::class);
        $this->http = $container->get(Services::class);
        $this->pluginSettings = $container->get(PluginSettings::class);
        $this->refinery = $container->get(Factory::class);
        $this->filesystem = $container->get(FilesystemAdapter::class);
        $this->dispatcher = $container->get(Dispatcher::class);
    }

    public function performCommand(string $cmd): void
    {
        $this->setTabs();

        switch ($cmd) {
            case self::CMD_SAVE_CONFIG:
                $this->saveConfig();
                break;

            case self::CMD_CONFIGURE:
            default:
                $this->configure();
        }
    }

    private function setTabs(): void
    {
        $tabs = [
            self::TAB_CONFIGURE => [
                'lng' => 'config',
                'cmd' => self::CMD_CONFIGURE,
            ],
        ];

        foreach ($tabs as $key => $tab) {
            $this->tabs->addTab(
                $key,
                $this->getPluginObjectOrFail()->txt($tab['lng']),
                $this->ctrl->getLinkTarget($this, $tab['cmd'])
            );
        }
    }

    private function configure(): void
    {
        $this->tabs->activateTab(self::TAB_CONFIGURE);

        $form = $this->getConfigForm();

        $this->ui->mainTemplate()->setContent($this->ui->renderer()->render($form));
    }

    private function getConfigForm(): Standard
    {
        $this->ctrl->setParameterByClass(self::class, 'cmd', 'save_config');

        $fields = [];
        foreach ($this->getFormFieldConfig() as $config) {
            $fields[$config['settingItem']->getKey()] = $config['field'];
        }

        return $this->ui->factory()->input()->container()->form()->standard(
            $this->ctrl->getFormActionByClass(self::class),
            $fields
        );
    }

    private function saveConfig(): void
    {
        $form = $this->getConfigForm()->withRequest($this->http->request());
        $data = $form->getData() ?? [];

        foreach ($this->getFormFieldConfig() as $config) {
            $key = $config['settingItem']->getKey();

            if (!key_exists($key, $data)) {
                continue;
            }

            $previousValue = $form->getInputs()[$key]->getValue();
            $currentValue = $data[$key];
            $config['settingItem']->setValue($currentValue);

            if (array_key_exists('postUpdate', $config)) {
                $config['postUpdate']($previousValue, $currentValue);
            }
        }

        $successMessage = '';
        if ($form->getError() === null) {
            $successMessage = $this->ui->renderer()->render($this->ui->factory()->messageBox()->success('Saved!'));
        }

        $this->ui->mainTemplate()->setContent(
            $successMessage .
            $this->ui->renderer()->render($form)
        );
    }

    /**
     * @psalm-return FormFieldConfig[]
     */
    private function getFormFieldConfig(): array
    {
        $ui = $this->ui->factory();
        $uiInputField = $ui->input()->field();

        $htmlModuleExistsConstraint = $this->getHtmlModuleExistsConstraint();
        $categoryExistsConstraint = $this->getObjectExistsByRefIdConstraint(ObjectType::CATEGORY(), 'no_category_found');
        $roleTemplateExistsConstraint = $this->getObjectExistsByObjIdConstraint(ObjectType::ROLE_TEMPLATE(), 'no_role_template_found');

        $config = [
            $this->createFormConfigItem(
                $this->pluginSettings->zipFileName(),
                $uiInputField->text('')->withRequired(true),
            ),
            $this->createFormConfigItem(
                $this->pluginSettings->objIdDataSrc(),
                $uiInputField->numeric('')->withRequired(true),
                [$htmlModuleExistsConstraint]
            ),
            $this->createFormConfigItem(
                $this->pluginSettings->refIdTargetCategory(),
                $uiInputField->numeric('')->withRequired(true),
                [$categoryExistsConstraint],
                function (string $previousValue, string $currentValue): void {
                    $previousValue = intval($previousValue);
                    $currentValue = intval($currentValue);

                    if ($previousValue !== $currentValue) {
                        $this->dispatcher->dispatchCommand(new RelocateTreeItemChildren(
                            $previousValue,
                            $currentValue
                        ));
                    }
                }
            ),
            $this->createFormConfigItem(
                $this->pluginSettings->objIdCatalogueProd(),
                $uiInputField->numeric('')->withRequired(true),
                [$htmlModuleExistsConstraint]
            ),
            $this->createFormConfigItem(
                $this->pluginSettings->objIdCatalogueStage(),
                $uiInputField->numeric('')->withRequired(true),
                [$htmlModuleExistsConstraint]
            ),
            $this->createFormConfigItem(
                $this->pluginSettings->objIdCatalogueDataBund(),
                $uiInputField->numeric('')->withRequired(true),
                [$htmlModuleExistsConstraint]
            ),
            $this->createFormConfigItem(
                $this->pluginSettings->objIdCatalogueDataTraeger(),
                $uiInputField->numeric('')->withRequired(true),
                [$htmlModuleExistsConstraint]
            ),
            $this->createFormConfigItem(
                $this->pluginSettings->courseTutorRoleTemplateId(),
                $uiInputField->numeric('')->withRequired(true),
                [$roleTemplateExistsConstraint]
            ),
            $this->createFormConfigItem(
                $this->pluginSettings->courseMemberRoleTemplateId(),
                $uiInputField->numeric('')->withRequired(true),
                [$roleTemplateExistsConstraint]
            ),
            $this->createFormConfigItem(
                $this->pluginSettings->groupAdminRoleTemplateId(),
                $uiInputField->numeric('')->withRequired(true),
                [$roleTemplateExistsConstraint]
            ),
            $this->createFormConfigItem(
                $this->pluginSettings->groupMemberRoleTemplateId(),
                $uiInputField->numeric('')->withRequired(true),
                [$roleTemplateExistsConstraint]
            ),
            $this->createFormConfigItem(
                $this->pluginSettings->refIdDefaultCourseTemplate(),
                $uiInputField->numeric(''),
                [$this->getObjectExistsByRefIdConstraint(ObjectType::COURSE(), 'no_course_found', true)]
            ),
            $this->createFormConfigItem(
                $this->pluginSettings->refIdDefaultGroupTemplate(),
                $uiInputField->numeric(''),
                [$this->getObjectExistsByRefIdConstraint(ObjectType::GROUP(), 'no_group_found', true)]
            ),
        ];

        foreach ($this->pluginSettings->courseTemplateRefIds() as $item) {
            $config[] = $this->createFormConfigItem(
                $item,
                $uiInputField->numeric(''),
                [$this->getObjectExistsByRefIdConstraint(ObjectType::COURSE(), 'no_course_found', true)],
            );
        }

        foreach ($this->pluginSettings->groupTemplateRefIds() as $item) {
            $config[] = $this->createFormConfigItem(
                $item,
                $uiInputField->numeric(''),
                [$this->getObjectExistsByRefIdConstraint(ObjectType::GROUP(), 'no_group_found', true)],
            );
        }

        return $config;
    }

    /**
     * @param Constraint[] $constraints
     * @psalm-param PostUpdateCallback|null $postUpdate
     * @psalm-return FormFieldConfig
     */
    private function createFormConfigItem(
        PluginSettingItem $item,
        FormInput         $field,
        array             $constraints = [],
        callable          $postUpdate = null
    ): array
    {
        $field = $field->withLabel($this->getPluginObjectOrFail()->txt($item->getKey()));

        $value = $item->getValue();
        if ($value !== null) {
            $field = $field->withValue($value);
        }

        foreach ($constraints as $constraint) {
            $field = $field->withAdditionalTransformation($constraint);
        }

        $config = [
            'settingItem' => $item,
            'field' => $field,
        ];

        if ($postUpdate !== null) {
            $config['postUpdate'] = $postUpdate;
        }

        return $config;
    }

    private function getPluginObjectOrFail(): ilPlugin
    {
        $pluginObject = $this->getPluginObject();
        if (null === $pluginObject) {
            throw new Exception('Plugin object not found!');
        }

        return $pluginObject;
    }

    private function getHtmlModuleExistsConstraint(): Constraint
    {
        return $this->refinery->custom()->constraint(function (int $value): bool {
            if (!ilObject::_exists($value, false, (string)ObjectType::HTML_LEARNING_MODULE())) {
                return false;
            }

            return $this->filesystem->hasDir('lm_data/lm_' . $value);
        }, $this->getPluginObjectOrFail()->txt('no_html_module_found'));
    }

    private function getObjectExistsByRefIdConstraint(ObjectType $type, string $translationKey = null, bool $isOptional = false): Constraint
    {
        /** @psalm-suppress UnusedVariable */
        return $this->refinery->custom()->constraint(/** @param mixed $value */ function ($value) use ($type, $isOptional): bool {
            if ($isOptional && !is_int($value)) {
                return true;
            }

            return ilObject::_exists($value, true, (string)$type);
        }, $this->getPluginObjectOrFail()->txt($translationKey ?? 'object_not_found'));
    }

    private function getObjectExistsByObjIdConstraint(ObjectType $type, string $translationKey = null): Constraint
    {
        /** @psalm-suppress UnusedClosureParam, UnusedVariable */
        return $this->refinery->custom()->constraint(function (int $value) use ($type): bool {
            return ilObject::_exists($value, false, (string)$type);
        }, $this->getPluginObjectOrFail()->txt($translationKey ?? 'object_not_found'));
    }
}
