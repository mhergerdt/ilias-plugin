<?php

$id = 'cgauto';
$version = '0.1.1';
$ilias_min_version = '7.21';
$ilias_max_version = '8.999';
$responsible = 'Marcel Hergerdt';
$responsible_mail = 'marcel.hergerdt@drv-bund.de';
$learning_progress = false;
$supports_export = false;
